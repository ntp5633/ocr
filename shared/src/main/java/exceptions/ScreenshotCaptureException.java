package exceptions;

/**
 * Created by npatta001c on 1/8/2015.
 */
public class ScreenshotCaptureException extends RuntimeException {

    public ScreenshotCaptureException(Throwable exception){
        super(exception);
    }

}

