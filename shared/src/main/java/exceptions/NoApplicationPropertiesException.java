package exceptions;

/**
 * Created by npatta001c on 1/8/2015.
 */
public class NoApplicationPropertiesException extends RuntimeException{

    public NoApplicationPropertiesException(String file, Throwable details){


        super(String.format("Error in locating/parsing file %s", file), details);

    }

}
