package exceptions;

/**
 * Created by npatta001c on 1/7/2015.
 */
public class UnknownActionException extends RuntimeException {


    public UnknownActionException(String action){
        super(String.format("Unknown action:%s", action));
    }
}
