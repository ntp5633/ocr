package exceptions;

/**
 * Created by npatta001c on 1/12/2015.
 */
public class OptionNotFoundException extends RuntimeException {

    public OptionNotFoundException(String option){
        super(String.format("Option %s is not found",option));
    }
}
