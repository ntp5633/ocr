package exceptions;

/**
 * Created by npatta001c on 1/7/2015.
 */
public class UnparsableUiActionFileException extends RuntimeException {

    public UnparsableUiActionFileException(String path, Throwable detailed_exception){
        super (String.format("Failed to parse file in path :%s",path), detailed_exception);
    }
}
