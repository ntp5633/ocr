import org.apache.commons.io.IOUtils;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.yaml.snakeyaml.Yaml;
import org.yaml.snakeyaml.constructor.Constructor;
import processing.BookmarkValidation;
import processing.SplunkHandler;

import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.text.ParseException;
import java.util.List;

/**
 * Created by Nidhin on 11/21/2014.
 */
public class SplunkExample {



    public static void main(String []args) throws ParseException, IOException, URISyntaxException {


        SplunkHandler sph = new SplunkHandler();

        String deviceId="6823055674311460572";
        org.joda.time.format.DateTimeFormatter sdf = DateTimeFormat.forPattern("MM/dd/yyyy:HH:mm:ss");


        //DateTime sd = sdf.parse("11/20/2014:21:41:00");

        //DateTime ed = sdf.parse("11/20/2014:21:46:00");


        DateTime sd = sdf.parseDateTime("1/13/2015:17:00:00");
        DateTime ed = sdf.parseDateTime("1/13/2015:17:10:00");

        String date_out=sd.toString();


        URL resource = YamlParserTest.class.getResource("bookmark_validation.yml");


        String content = IOUtils.toString(Main.class.getResourceAsStream("bookmark_validation.yml"),
                "UTF-8");

        Constructor c = new Constructor(BookmarkValidation.class);
        Yaml yaml = new Yaml(c);


        BookmarkValidation bv = (BookmarkValidation) yaml.load(content);



        List<String> logs=sph.getLog(deviceId,sd,ed);


        boolean valid = sph.isValid(logs, bv.getAdding_validation());

        int k=7;

        System.out.println(valid);
    }
}
