package ui;

import config.ConfigConstants;
import exceptions.UnknownActionException;
import exceptions.UnparsableUiActionFileException;
import model.ActionEnum;
import model.UIAction;
import model.ActionArguments;
import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;
import org.yaml.snakeyaml.Yaml;
import org.yaml.snakeyaml.constructor.Constructor;

import java.io.IOException;
import java.io.InputStream;
import java.util.*;

/**
 * Created by npatta001c on 1/7/2015.
 */
public class UIActionList {

    private Map<String, UIAction> actionmap;

    private static Logger logger = Logger.getLogger(UIActionList.class);

    private String uipath = ConfigConstants.uiActionsFile;

    public UIActionList() {
        actionmap = new HashMap<String, UIAction>();

        parseActions();
    }


    private void parseActions() {
        Constructor c = new Constructor(UIAction.class);
        Yaml yaml = new Yaml(c);


        ClassLoader loader = Thread.currentThread().getContextClassLoader();
        InputStream resourceAsStream = loader.getResourceAsStream(uipath);


        String content = null;
        try {
            content = IOUtils.toString(resourceAsStream);
        } catch (IOException e) {
            logger.error("Failed to parse the ui actions");
            throw new UnparsableUiActionFileException(uipath, e);

        }


        Iterable<Object> parsed_objects = yaml.loadAll(content);

        List<UIAction> actions = new LinkedList<UIAction>();


        parsed_objects.forEach(this::addAction);



    }


    public void addAction(Object o) {
        UIAction a = (UIAction) o;


        actionmap.put(a.getName(), a);
    }



    public UIAction getAction(String action){
        return actionmap.get(action);
    }
}
