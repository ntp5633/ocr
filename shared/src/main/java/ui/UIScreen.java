package ui;

import org.hibernate.validator.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by npatta001c on 1/13/2015.
 */
public class UIScreen {



    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<UISegmenters> getParsers() {
        return parsers;
    }

    public void setParsers(ArrayList<UISegmenters> parsers) {
        this.parsers = parsers;
    }

    public List<UIRegion> getRegions() {
        return regions;
    }

    public void setRegions(ArrayList<UIRegion> regions) {
        this.regions = regions;
    }

    public ArrayList<UIValidationRule> getValidation() {
        return validation;
    }

    public void setValidation(ArrayList<UIValidationRule> validation) {
        this.validation = validation;
    }

    private String name;

    private ArrayList<UIRegion> regions;

    private ArrayList<UISegmenters> parsers;

    private ArrayList<UIValidationRule> validation;


    public void initialize(){

    }


}
