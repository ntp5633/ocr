package ui;

/**
 * Created by npatta001c on 1/13/2015.
 */
public class UIValidationRule {


    public String getScreen() {
        return screen;
    }

    public void setScreen(String screen) {
        this.screen = screen;
    }

    public String getExpected_text() {
        return expected_text;
    }

    public void setExpected_text(String expected_text) {
        this.expected_text = expected_text;
    }

    private String screen;
    private String expected_text;

}
