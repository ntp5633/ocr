package ui;

import org.hibernate.validator.constraints.NotBlank;

import java.util.List;

/**
 * Created by npatta001c on 1/13/2015.
 */
public class UIRegion {

    private String name;
    private UILocation uilocation;
    private String location;
    private List<UIRegion> children;






    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public UILocation getUilocation() {
        return uilocation;
    }

    public void setUilocation(UILocation uilocation) {
        this.uilocation = uilocation;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public List<UIRegion> getChildren() {
        return children;
    }

    public void setChildren(List<UIRegion> children) {
        this.children = children;
    }
}
