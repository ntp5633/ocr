package ui;

import java.util.List;

/**
 * Created by npatta001c on 1/13/2015.
 */
public class UISegmenters {

    private String name;
    private String type;
    private String args;
    private String component;
    private String on;

    public String getArgs() {
        return args;
    }

    public void setArgs(String args) {
        this.args = args;
    }

    public List<UISegmenters> getCustom() {
        return custom;
    }

    public void setCustom(List<UISegmenters> custom) {
        this.custom = custom;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    private List<UISegmenters> custom;


    public String getOn() {
        return on;
    }

    public void setOn(String on) {
        this.on = on;
    }

    public String getComponent() {
        return component;
    }

    public void setComponent(String component) {
        this.component = component;
    }
}
