import com.comcast.xre.testframework.IXREReceiver;
import com.comcast.xre.testframework.receivers.StbXREReceiver;
import com.comcast.xre.testframework.services.TestModuleDataClient;
import com.comcast.xre.testframework.testng.XREReceiverDataProvider;

/**
 * Created by npatta001c on 2/27/2015.
 */
public class Sample1 {

    public static void main (String [] args){
        String stbDeviceId;

        stbDeviceId = "8911730791644808378";
        //stbDeviceId = "849322751399247923";
        IXREReceiver receiver = new StbXREReceiver(stbDeviceId);

        receiver.sendKey();
        dataClient = new TestModuleDataClient();

        clearStats(stbDeviceId, false);

        receiverList[0].setupReceiver();
        //waitForTestModeEnabled(stbDeviceId, receiverList[0]);
        //waitForGuideStart(stbDeviceId);


        sleep(4000);

        receiverList[0].sendKey("DOWN");

        receiverList[0].sendKey("EXIT");
        sleep(2000);

        //  log.info("TC: Send GUIDE key");
        receiverList[0].sendKey("GUIDE");
        sleep(3000);

        //log.info("TC: Send MENU key");
        receiverList[0].sendKey("MENU");
        receiverList[1].sendKey("MENU");
        sleep(3000);

        //log.info("TC: Send ENTER key");
        receiverList[0].sendKey("ENTER");
        receiverList[1].sendKey("ENTER");
        sleep(4000);

        receiverList[0].sendKey("EXIT");
        receiverList[1].sendKey("EXIT");

        receiverList[0].exitTest();
        receiverList[1].exitTest();
    }
}
