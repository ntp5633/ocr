package model;

/**
 * Created by npatta001c on 1/8/2015.
 */
public enum ActionEnum {

    consume_argument,
    consume_named_argument,
    key_press,
    navigate_option_horizontally,
    navigate_option_vertically,
    guide_navigate,
    channel_navigate,
    hot_channel_recording,
    multi_key_press


}
