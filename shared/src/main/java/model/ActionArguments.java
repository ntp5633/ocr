package model;

import java.util.HashMap;
import java.util.Map;
import java.util.Stack;

/**
 * Created by npatta001c on 1/7/2015.
 */
public class ActionArguments {

    private Stack<String> sequenceArguments;


    private Map<String,String> namedArguments;



    private String currentArg;


    public ActionArguments(){
        sequenceArguments = new Stack<String>();
        namedArguments = new HashMap<String,String>();
        currentArg =null;
    }

    public void getNamedArgument(String arg){
        currentArg= namedArguments.get(arg);
    }



    public String getCurrentArg(){
        String value=currentArg;
        currentArg = null;
        return value;
    }


    public void getNext(){
        currentArg= sequenceArguments.pop();
    }


    public void addNamedArgument(Arguments key,String value){
        addNamedArgument(key.name(),value);
    }

    public void addNamedArgument(String key,String value){
        namedArguments.put(key,value);
    }

    public void addSequentialArgument(String arg){
        sequenceArguments.push(arg);
    }
}
