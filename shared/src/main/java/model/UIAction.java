package model;

import java.util.List;

/**
 * Created by npatta001c on 1/7/2015.
 */
public class UIAction {


    private String name;
    private String description;
    private List<String> sub_actions;

    public UIAction(){}

    public UIAction(String description, String name) {
        this.description = description;
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        UIAction uiAction = (UIAction) o;

        if (!name.equals(uiAction.name)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return name.hashCode();
    }

    public List<String> getSub_actions() {
        return sub_actions;
    }

    public void setSub_actions(List<String> sub_actions) {
        this.sub_actions = sub_actions;
    }
}
