package screenshot;

import annotation.DeviceId;
import com.google.inject.Inject;
import com.google.inject.name.Named;
import exceptions.ScreenshotCaptureException;
import org.apache.http.HttpEntity;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.CookieStore;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.Logger;

import java.io.*;
import java.util.ArrayList;
import java.util.List;


public class XreScreenshotTaker implements IScreenshotTaker {

    private static Logger logger = Logger.getLogger(XreScreenshotTaker.class);

    private String deviceId;
    private String screenCaptureTool;

    @Inject
    public XreScreenshotTaker(@Named("deviceId") String deviceId, @Named("screenCaptureUrl") String screenCaptureUrl) {
        this.deviceId = deviceId;
        this.screenCaptureTool = screenCaptureUrl;

    }

    @Override
    public void initialize() {

    }

    @Override
    public void takeScreenshot(String filepath) {

        try {
            CloseableHttpClient client = HttpClientBuilder.create().build();


            // Create a local instance of cookie store
            CookieStore cookieStore = new BasicCookieStore();

            // Create local HTTP context
            HttpContext localContext = new BasicHttpContext();
            // Bind custom cookie store to the local context
            localContext.setAttribute(HttpClientContext.COOKIE_STORE, cookieStore);

            HttpGet httpget = new HttpGet(screenCaptureTool + "getScreenshot");


            HttpPost httppost = new HttpPost(screenCaptureTool + "capture");

            List<BasicNameValuePair> nvps = new ArrayList<BasicNameValuePair>();
            nvps.add(new BasicNameValuePair("deviceId", deviceId));

            httppost.setEntity(new UrlEncodedFormEntity(nvps));


            CloseableHttpResponse response = client.execute(httppost, localContext);


            EntityUtils.consumeQuietly(response.getEntity());

            Thread.sleep(5000);

            //localContext = new BasicHttpContext();

            //localContext.setAttribute(HttpClientContext.COOKIE_STORE, cookieStore);


            response = client.execute(httpget, localContext);

            StatusLine statusLine = response.getStatusLine();
            int statusCode = statusLine.getStatusCode();


            byte[] bytes;
            if (statusCode == 200) {
                try {
                    HttpEntity entity = response.getEntity();
                    if (entity != null) {

                        FileOutputStream fos = new FileOutputStream(new File(filepath));
                        entity.writeTo(fos);
                        fos.close();
                    }
                } finally {
                    response.close();
                }
            } else {
                throw new IOException("Download failed, HTTP response code "
                        + statusCode + " - " + statusLine.getReasonPhrase());
            }
        }catch (Exception  e) {
            logger.error("Failed to capture screenshot");
            throw  new ScreenshotCaptureException(e);
        }


    }
}
