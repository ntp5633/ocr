package screenshot;

/**
 * Created by npatta001c on 1/8/2015.
 */
public interface IScreenshotTaker {

    public void initialize();
    public void takeScreenshot(String filepath);
}
