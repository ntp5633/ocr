import java.io.File;
import net.sourceforge.tess4j.*;
/**
 * Created by npatta001c on 10/24/2014.
 */
import java.io.File;

import net.sourceforge.tess4j.Tesseract;
import net.sourceforge.tess4j.TesseractException;

public class TesseractExample
{
    public static void main(String[] args) {
        //File imageFile = new File("samples/eurotext.pdf");

        //File imageFile = new File("samples/eurotext.png");
        File imageFile = new File("final_image.jpg");
        Tesseract instance = Tesseract.getInstance(); // JNA Interface Mapping
        // Tesseract1 instance = new Tesseract1(); // JNA Direct Mapping


        try {
            String result = instance.doOCR(imageFile);
            System.out.println(result);
        } catch (TesseractException e) {
            System.err.println(e.getMessage());
        }
    }
}
