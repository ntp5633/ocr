package config;

/**
 * Created by npatta001c on 1/8/2015.
 */
public class GlobalProperties {

    private String deviceId;
    private String accountId;
    private String userName;
    private String password;
    private String splunkHost;
    private String splunkPort;
    private String deviceIp;

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getSplunkHost() {
        return splunkHost;
    }

    public void setSplunkHost(String splunkHost) {
        this.splunkHost = splunkHost;
    }

    public String getSplunkPort() {
        return splunkPort;
    }

    public void setSplunkPort(String splunkPort) {
        this.splunkPort = splunkPort;
    }

    public String getDeviceIp() {
        return deviceIp;
    }

    public void setDeviceIp(String deviceIp) {
        this.deviceIp = deviceIp;
    }


    /*




    deviceId : 6823055674311460572
    accountId: 8880552999379356157
    userName: npatta001c
    password: Eliwood8
    splunkHost: splunk.ccp.cable.comcast.com
    splunkPort: 8089
    deviceIp: 68.46.241.230
*/

}
