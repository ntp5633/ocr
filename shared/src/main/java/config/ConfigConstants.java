package config;

/**
 * Created by npatta001c on 1/8/2015.
 */
public class ConfigConstants {

    public static String useCaptureCard ="useCaptureCard";
    public static String useCloudAccount="useCloudAccount";


    public static String defaultPropertiesFile = "options.properties";

    public static String uiActionsFile="ui_actions/actions.yml";
}
