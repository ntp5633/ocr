package config;

import com.google.inject.AbstractModule;
import com.google.inject.name.Names;
import driver.BaseDriver;
import driver.StbBaseDriver;
import org.apache.log4j.Logger;
import processing2.ImageProcessor;
import processing2.TextProcessor;
import screenshot.CaptureCardScreenshotTaker;
import screenshot.IScreenshotTaker;
import screenshot.XreScreenshotTaker;
import ui.UIActionList;

import java.io.InputStream;
import java.util.Properties;


/**
 * Created by npatta001c on 1/8/2015.
 */
public class ApplicationModule extends AbstractModule {

    static Logger logger = Logger.getLogger(ApplicationModule.class);

    private String propertiesFile;

    public ApplicationModule(String propertiesFile){
        this.propertiesFile = propertiesFile;

    }

    public ApplicationModule(){
        this.propertiesFile = ConfigConstants.defaultPropertiesFile;

    }

    @Override
    protected void configure()  {

        InputStream resourceAsStream = Thread.currentThread().getContextClassLoader().getResourceAsStream(propertiesFile);

        Properties properties=null;

        try {

            properties = new Properties();
            properties.load(resourceAsStream);

        } catch (Exception e) {
            String msg= String.format("Error in locating/parsing file %s", propertiesFile);
            logger.error(msg);

            System.exit(-1);

        }

        //named bind properties
        Names.bindProperties(binder(), properties);


        if (properties.getProperty(ConfigConstants.useCaptureCard).equals("false")){
            bind(IScreenshotTaker.class).to(XreScreenshotTaker.class).asEagerSingleton();

        }else{
            bind(IScreenshotTaker.class).to(CaptureCardScreenshotTaker.class).asEagerSingleton();
        }



        bind(UIActionList.class).asEagerSingleton();

        if (properties.getProperty(ConfigConstants.useCloudAccount).equals("false")){
            bind(BaseDriver.class).to(StbBaseDriver.class).asEagerSingleton();
        }


        bind(ImageProcessor.class).asEagerSingleton();

        bind(TextProcessor.class).asEagerSingleton();

        /*

        Constructor c = new Constructor(GlobalProperties.class);
        Yaml yaml = new Yaml(c );

        InputStream resourceAsStream = Thread.currentThread().getContextClassLoader().getResourceAsStream(propertiesFile);


        String content =null;

        try {

            content = IOUtils.toString(resourceAsStream);
        } catch (IOException e) {
            String msg= String.format("Error in locating/parsing file %s", propertiesFile);
            logger.error(msg);

            System.exit(-1);

        }



        Iterable<Object> object = yaml.loadAll(content);

        List<Action> actions =new LinkedList<Action>();

        for(Object o : object ){
            actions.add((Action)o);
        }

        int k = 7;

        //parse the properties file

*/


        //bind()
    }
}
