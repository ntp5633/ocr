package config;

import exceptions.UnparsableUiActionFileException;
import model.UIAction;
import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;
import org.yaml.snakeyaml.Yaml;
import org.yaml.snakeyaml.constructor.Constructor;
import ui.UIActionList;

import java.io.IOException;
import java.io.InputStream;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by npatta001c on 1/7/2015.
 */
public class UiActionsConfigParser {

    static Logger logger = Logger.getLogger(UiActionsConfigParser.class);

    private UIActionList actionsList;

    private String file_path;


    public UIActionList parseUiActions(){
        Constructor c = new Constructor(UIAction.class);
        Yaml yaml = new Yaml(c );


        file_path = "ui_actions/actions.yml";
        ClassLoader loader = Thread.currentThread().getContextClassLoader();
        InputStream resourceAsStream = loader.getResourceAsStream(file_path);


        String content=null;
        try{
            content = IOUtils.toString(resourceAsStream);
        }catch (IOException e){
            logger.error("Failed to parse the ui actions" );
            throw new UnparsableUiActionFileException(file_path,e);

        }



        Iterable<Object> object = yaml.loadAll(content);

        List<UIAction> actions =new LinkedList<UIAction>();

        for (Object o : object ){
            actionsList.addAction((UIAction)o);
        }

        return actionsList;
    }


}
