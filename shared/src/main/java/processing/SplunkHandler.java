package processing;

import com.splunk.Args;
import com.splunk.ResultsReaderJson;
import com.splunk.Service;
import org.apache.http.HttpHost;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.fluent.Executor;
import org.apache.http.client.utils.URIBuilder;
import org.joda.time.DateTime;

import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by Nidhin on 11/20/2014.
 */
public class SplunkHandler {


    private String userName;
    private String password;

    private String endpoint;

    private SimpleDateFormat sdf;

    private Executor executor;

    private URIBuilder uri;

    public SplunkHandler(String userName, String password){
        this.userName=userName;
        this.password=password;
        endpoint="http://splunk.ccp.cable.comcast.com:8089/servicesNS/npattaniyil/search/search/jobs/export";

        sdf = new SimpleDateFormat("MM/dd/yyyy:HH:mm:ss");
    }

    public SplunkHandler() throws URISyntaxException {
        this.userName="npattaniyil";
        this.password="DSCsGtDJ`9";
        endpoint="http://splunk.ccp.cable.comcast.com:8089/servicesNS/npattaniyil/search/search/jobs/export/";

        sdf = new SimpleDateFormat("MM/dd/yyyy:HH:mm:ss");

        executor = Executor.newInstance();

        executor.auth(new AuthScope(AuthScope.ANY), new UsernamePasswordCredentials(userName, password));
        executor.authPreemptive(new HttpHost(endpoint));

        uri = new URIBuilder(endpoint);
    }

    /*
    public String getLog(String deviceId,Date starttime, Date endtime) throws IOException, URISyntaxException {

        String start_time_str=sdf.format(starttime);

        String end_time_str=sdf.format(endtime);




        String query_str=String.format("search %s %s %s", deviceId, start_time_str, end_time_str);

        URIBuilder n_uri= uri.addParameter("search",query_str);
        n_uri=n_uri.addParameter("output_mode","json");





        Request post = Request.Post(n_uri.build());

        String userPass = userName + ":" + password;
        String basicAuth = "Basic " + javax.xml.bind.DatatypeConverter.printBase64Binary(userPass.getBytes("UTF-8"));
        post=post.addHeader("Authorization",basicAuth);



        Content raw_content;
        try{

            post=post.addHeader("Authorization",basicAuth);
            raw_content = executor.execute(post).returnContent();
        }catch (Exception e){

        }

         post = Request.Post(n_uri.build());

        userPass = userName + ":" + password;
        basicAuth = "Basic " + javax.xml.bind.DatatypeConverter.printBase64Binary(userPass.getBytes("UTF-8"));
        post=post.addHeader("Authorization",basicAuth);

        raw_content = executor.execute(post).returnContent();

        String content=raw_content.asString();
        return  content;



    }
*/

    public List<String> getLog(String deviceId,DateTime starttime, DateTime endtime) throws IOException, URISyntaxException {

        String start_time_str=starttime.toString();

        String end_time_str=endtime.toString();




        String query_str=String.format("search %s", deviceId);

        URIBuilder n_uri= uri.addParameter("search",query_str);
        n_uri=n_uri.addParameter("output_mode","json");



        Service s = new Service("splunk.ccp.cable.comcast.com",8089,"http");
        s.login(userName,password);

        Args oneshotSearchArgs = new Args();
        oneshotSearchArgs.put("earliest_time", start_time_str);
        oneshotSearchArgs.put("latest_time",   end_time_str);
        oneshotSearchArgs.put("count", 0);
        oneshotSearchArgs.put("output_mode",   "json");

        String oneshotSearchQuery = query_str;


        InputStream results_oneshot =  s.oneshotSearch(oneshotSearchQuery, oneshotSearchArgs);

        ResultsReaderJson rrj = new ResultsReaderJson(results_oneshot);

        List<String> events = new LinkedList<String>();

        HashMap<String, String> event;
        while ((event = rrj.getNextEvent()) != null) {
           // System.out.println("\n********EVENT********");


            for (String key: event.keySet()) {
                //System.out.println("   " + key + ":  " + event.get(key));

                if (key.equals("_raw")){
                    events.add(event.get(key));
                }

            }
        }
        rrj.close();



        return events;


    }




    public boolean isValid(List<String> logs, List<String> expected){

        int s_exp=expected.size()-1;

        int non_matched= expected.size();

        for(String s: logs){
            if (non_matched!=0){
                String current=expected.get(s_exp);
                if(s.contains(current)){
                    System.out.println("Matched :"+ current);
                    s_exp--;
                    non_matched--;
                }
            }

            System.out.println(logs);


        }

        if (non_matched!=0){
            return false;
        }

        return true;
    }

}
