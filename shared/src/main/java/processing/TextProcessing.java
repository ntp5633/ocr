package processing;

import net.sourceforge.tess4j.Tesseract;
import net.sourceforge.tess4j.TesseractException;

import java.io.File;

public class TextProcessing {

    public static String getText(String image_path){
        Tesseract instance = Tesseract.getInstance(); // JNA Interface Mapping
        // Tesseract1 instance = new Tesseract1(); // JNA Direct Mapping


        File imageFile = new File(image_path);

        String result="";
        try {


            result = instance.doOCR(imageFile).trim();
            System.out.println(result);
            return result;
        } catch (TesseractException e) {
            System.err.println(e.getMessage());
        }


        return result;
    }


}
