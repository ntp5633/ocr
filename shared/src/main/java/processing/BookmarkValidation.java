package processing;

import java.util.List;

/**
 * Created by Nidhin on 11/21/2014.
 */
public class BookmarkValidation {

    private List<String> adding_validation;

    private List<String> removing_validation;

    private String name;

    private String description;


    public List<String> getAdding_validation() {
        return adding_validation;
    }

    public void setAdding_validation(List<String> expressions) {
        this.adding_validation = expressions;

    }


    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<String> getRemoving_validation() {
        return removing_validation;
    }

    public void setRemoving_validation(List<String> removing_validation) {
        this.removing_validation = removing_validation;
    }
}
