package processing;

import org.apache.http.HttpEntity;
import org.apache.http.StatusLine;
import org.apache.http.client.CookieStore;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.apache.http.util.EntityUtils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by npatta001c on 10/27/2014.
 */
public class ScreenshotTaker {

    private static String host="http://xre.poc8.xcal.tv:9072/screenCaptureTool/";
    public static void main(String[] args) throws IOException, InterruptedException {


        ScreenshotTaker.getScreenshot("staker.jpg","6823055674311460572");

    }



    public static void getScreenshot(String filepath,String deviceId) throws IOException, InterruptedException {

        CloseableHttpClient client = HttpClientBuilder.create().build();


        // Create a local instance of cookie store
        CookieStore cookieStore = new BasicCookieStore();

        // Create local HTTP context
        HttpContext localContext = new BasicHttpContext();
        // Bind custom cookie store to the local context
        localContext.setAttribute(HttpClientContext.COOKIE_STORE, cookieStore);

        HttpGet httpget = new HttpGet(host+"getScreenshot");


        HttpPost httppost = new HttpPost(host+"capture");

        List<BasicNameValuePair> nvps = new ArrayList<BasicNameValuePair>();
        nvps.add(new BasicNameValuePair("deviceId", deviceId));

        httppost.setEntity(new UrlEncodedFormEntity(nvps));


        CloseableHttpResponse response = client.execute(httppost, localContext);


        EntityUtils.consumeQuietly(response.getEntity());

        Thread.sleep(5000);

        //localContext = new BasicHttpContext();

        //localContext.setAttribute(HttpClientContext.COOKIE_STORE, cookieStore);


        response = client.execute(httpget, localContext);

        StatusLine statusLine = response.getStatusLine();
        int statusCode = statusLine.getStatusCode();


        byte[] bytes;
        if (statusCode == 200) {
            try {
                HttpEntity entity = response.getEntity();
                if (entity != null) {

                    FileOutputStream fos = new FileOutputStream(new File(filepath));
                    entity.writeTo(fos);
                    fos.close();
                }
            } finally {
                response.close();
            }
        } else {
            throw new IOException("Download failed, HTTP response code "
                    + statusCode + " - " + statusLine.getReasonPhrase());
        }



    }
}
