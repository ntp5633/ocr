import org.opencv.core.Mat;
import org.opencv.highgui.Highgui;
import processing.ImageProcessing;
import processing.TextProcessing;

/**
 * Created by Nidhin on 10/26/2014.
 */
public class Combined {

    public static void main(String []args){

        String imgPath="samples/screen/show_recent_off.png";

        Mat filteredImage=ImageProcessing.getFilteredImage(imgPath);


        String procPath="processed_image.jpg";
        Highgui.imwrite(procPath, filteredImage);

        String content=TextProcessing.getText(procPath);

        System.out.println(content);
    }
}
