package driver;

import ch.qos.logback.core.util.FileUtil;
import com.comcast.xre.testframework.receivers.StbXREReceiver;
import com.google.inject.Inject;
import com.google.inject.name.Named;
import exceptions.OptionNotFoundException;
import exceptions.UnknownActionException;
import model.ActionArguments;
import model.ActionEnum;
import model.UIAction;
import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.opencv.core.Mat;
import processing2.ImageProcessor;
import processing2.TextProcessor;
import screenshot.IScreenshotTaker;
import ui.UIActionList;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

/**
 * Created by npatta001c on 1/9/2015.
 */
public class StbBaseDriver implements BaseDriver {
    private static Logger logger = Logger.getLogger(ImageProcessor.class);
    private UIActionList actions;


    private StbXREReceiver client;
    private IScreenshotTaker screenshotTaker;

    private ImageProcessor imageProcessor;
    private TextProcessor textProcessor;

    private int millis = 3000;

    private String screenShotArtifactsDirectory;
    private int action_number;

    private int max_times = 10;

    @Inject
    public StbBaseDriver(UIActionList actions, @Named("deviceId") String deviceId,
                         @Named("screenShotArtifactsDirectory") String screenShotArtifactsDirectory,
                         IScreenshotTaker screenshotTaker, ImageProcessor imageProcessor, TextProcessor textProcessor


    ) {
        this.actions = actions;
        client = new StbXREReceiver(deviceId);
        this.screenshotTaker = screenshotTaker;
        this.screenShotArtifactsDirectory = screenShotArtifactsDirectory;
        this.action_number = 0;

        this.imageProcessor = imageProcessor;
        this.textProcessor = textProcessor;
        File destfile = Paths.get(screenShotArtifactsDirectory).toFile();
        FileUtils.deleteQuietly(destfile);


        Paths.get(screenShotArtifactsDirectory).toFile().mkdirs();

    }


    @Override
    public void start() {

    }

    @Override
    public void stop() {

    }

    @Override
    public void sleep() {
        try {
            Thread.sleep(millis);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }


    @Override
    public void performAction(ActionEnum action, ActionArguments args) {
        this.action_number++;
        Path path = Paths.get(this.screenShotArtifactsDirectory, "" + action_number);
        path.toFile().mkdir();

        logger.info(String.format("Performing action:%s", action));


        switch (action) {
            case consume_argument:
                consume_argument(args);
                break;
            case consume_named_argument:
                consume_named_argument(args);
                break;
            case key_press:
                consume_argument(args);
                keyPress(args);
                break;
            case multi_key_press:
                multiKeyPress(args);
                break;
            case navigate_option_horizontally:
                consume_argument(args);
                navigateAction(args, true);
                break;
            case navigate_option_vertically:
                consume_argument(args);
                navigateAction(args, false);
            default:
                complex_action(action, args);
                break;

        }


        // perform action
        // performActionHelper
    }

    private void navigateAction(ActionArguments args, boolean horizontal_direction) {

        String direction;
        String option_to_search_for;
        if (horizontal_direction) {
            direction = "RIGHT";
        } else {
            direction = "DOWN";
        }

        int times = 0;
        option_to_search_for = args.getCurrentArg();
        logger.info(String.format("Navigate %s until option %s is found", direction,option_to_search_for));

        boolean item_found = false;
        do {


            Path path = Paths.get(this.screenShotArtifactsDirectory, "" + action_number, times + ".jpg");
            this.screenshotTaker.takeScreenshot(path.toString());


            String areaOnFocus = imageProcessor.getAreaOnFocus(path.toString());
            String item_on_focus = textProcessor.extractContent(areaOnFocus);
            logger.debug(String.format("Parsed option: %s", item_on_focus));
            if (item_on_focus.equals(option_to_search_for)) {
                item_found = true;
            } else {
                args.addSequentialArgument(direction);
                args.getNext();
                keyPress(args);
            }
            times++;
        } while (!item_found && times < max_times);


        if (times > max_times && !item_found) {
            throw new OptionNotFoundException(option_to_search_for);
        }

        //get item in focus
        //if equal, stop...else iterate


    }


    private void complex_action(ActionEnum action, ActionArguments args) {
        List<String> subactions = actions.getAction(action.name()).getSub_actions();

        if (subactions != null) {
            subactions.forEach((actionString) -> {
                String[] values = actionString.split(",");
                String actionname = values[0].trim();
                String[] arguments = Arrays.copyOfRange(values, 1, values.length);


                for (String a : arguments) {
                    args.addSequentialArgument(a.trim());
                }

                ActionEnum ae = ActionEnum.valueOf(actionname);

                UIAction a = actions.getAction(ae.name());

                if (a == null) {
                    throw new UnknownActionException(action.name());
                }


                performAction(ae, args);
            });
        }

    }


    private void consume_argument(ActionArguments args) {
        args.getNext();

    }

    private void consume_named_argument(ActionArguments args) {
        String arg = args.getCurrentArg();
        args.getNamedArgument(arg);

        String new_arg = args.getCurrentArg();
        args.addSequentialArgument(new_arg);

    }


    private String preprocessedKey(String key) {
        String processed = key.toUpperCase();

        int num;

        try {
            num = Integer.parseInt(processed);
            processed = "NUMBER_" + num;
        } catch (NumberFormatException nfe) {

        }


        switch (processed) {
            case "XFINITY":
                processed = "MENU";
                break;
            case "OK":
                processed = "ENTER";
                break;

        }

        return processed;
    }

    private void keyPress(ActionArguments args) {
        String arg = args.getCurrentArg();
        arg = preprocessedKey(arg);
        client.sendKey(arg);
        sleep();

    }

    private void multiKeyPress(ActionArguments args) {
        String arg = args.getCurrentArg();


        char[] chars = arg.toCharArray();
        //put characters in reverse order
        for (int i = chars.length - 1; i >= 0; i--) {
            args.addSequentialArgument(String.valueOf(chars[i]));
        }
        //for each character , press key
        for (int i = 0; i < chars.length; i++) {
            args.getNext();
            keyPress(args);
        }


    }
}
