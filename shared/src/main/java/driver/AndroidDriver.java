package driver;

import java.util.List;

/**
 * Created by Nidhin on 11/21/2014.
 */
public class AndroidDriver implements ApplicationInterface {
    @Override
    public void setup() {

    }

    @Override
    public void teardown() {

    }

    @Override
    public void navigateToScreen(String screen_name) {

    }

    @Override
    public void removeAllFavorites() {

    }

    @Override
    public void type(String input) {

    }

    @Override
    public void addFavorite() {

    }

    @Override
    public List<String> getAllBookmarks() {

        return null;

    }

    @Override
    public void takeScreenshot() {

    }

    @Override
    public void bookmark() {

    }
}
