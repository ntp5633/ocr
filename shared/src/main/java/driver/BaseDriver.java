package driver;

import model.ActionArguments;
import model.ActionEnum;

/**
 * Created by npatta001c on 1/9/2015.
 */
public interface  BaseDriver {

    public void start();
    public void stop();
    public void sleep();

    public void performAction(ActionEnum ae, ActionArguments args);
}


