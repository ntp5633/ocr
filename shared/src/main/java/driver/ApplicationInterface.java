package driver;

import java.util.List;

/**
 * Created by Nidhin on 11/21/2014.
 */
public interface ApplicationInterface {


    public void setup();

    public void teardown();

    public void navigateToScreen(String screen_name);

    public void removeAllFavorites();

    public void type(String input);

    public void addFavorite();

    public List<String> getAllBookmarks();

    public void takeScreenshot();

    public void bookmark();

}
