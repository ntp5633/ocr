package driver;

//import com.comcast.xre.testframework.receivers.StbXREReceiver;
import org.apache.log4j.Logger;
import org.opencv.core.Mat;
import org.opencv.highgui.Highgui;
import processing.ImageProcessing;
import processing.ScreenshotTaker;
import processing.TextProcessing;

import java.io.IOException;
import java.util.List;

/**
 * Created by Nidhin on 11/21/2014.
 */
public class StbDriver implements ApplicationInterface {


    public final int MILLIS = 3000;
    private String deviceId = "";

   // private StbXREReceiver sbreceiver;


    public StbDriver() {
     //   sbreceiver = new StbXREReceiver(deviceId);

        Logger.getLogger("org.apache.http").setLevel(org.apache.log4j.Level.OFF);
    }

    private static void sleep(long millis) {
        try {
            Thread.sleep(millis);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void setup() {
    //    sbreceiver.sendKey("EXIT");
    }

    @Override
    public void teardown() {
     //   sbreceiver.sendKey("EXIT");
    }

    @Override
    public void navigateToScreen(String screen) {

        if (screen.equals("Search")) {
            String[] subscreen = screen.split("->");

     //       sbreceiver.sendKey("MENU");
            sleep(MILLIS);

            navigateTo(screen);
            sleep(MILLIS);

  //          sbreceiver.sendKey("ENTER");
//

        }

    }


    private void navigateTo(String option) {
        String path = "nav";
        String content = null;
        try {
            content = getMenuOption(path);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        while (!content.equals(option)) {
       //     sbreceiver.sendKey("RIGHT");
            sleep(MILLIS);
            try {
                content = getMenuOption(path);
            } catch (IOException e) {
                e.printStackTrace();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        }
    }


    private String getMenuOption(String filepath) throws IOException, InterruptedException {
        String path = filepath + ".jpg";
        ScreenshotTaker.getScreenshot(path, deviceId);
        Mat m = ImageProcessing.getFilteredImage(path);

        String n_file = filepath + "_processed.jpg";
        Highgui.imwrite(n_file, m);

       // String content = TextProcessing.getText(n_file);
        //return content;
        return "";
    }


    @Override
    public void removeAllFavorites() {

    }

    @Override
    public void type(String input) {

        input = input.toUpperCase();

        for (char c : input.toCharArray()) {
       //     sbreceiver.sendKey(Character.toString(c));
        }

    }

    @Override
    public void addFavorite() {

    }

    @Override
    public List<String> getAllBookmarks() {

        return null;

    }

    @Override
    public void takeScreenshot() {

    }

    @Override
    public void bookmark() {

     //   sbreceiver.sendKey("DOWN");
      //  sbreceiver.sendKey("ENTER");
        navigateTo("Favorite");
    //    sbreceiver.sendKey("ENTER");

    }
}
