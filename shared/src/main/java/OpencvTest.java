/**
 * Created by Nidhin on 10/26/2014.
 */

import org.opencv.core.*;
import org.opencv.highgui.Highgui;
import org.opencv.imgproc.Imgproc;

import java.util.ArrayList;
import java.util.List;

import static org.opencv.imgproc.Imgproc.COLOR_RGB2GRAY;





public class OpencvTest {


    static {

        System.loadLibrary(org.opencv.core.Core.NATIVE_LIBRARY_NAME);
    }

    public static void main(String[] args) {
        System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
        //Mat mat = Mat.eye( 3, 3, CvType.CV_8UC1 );
        //System.out.println( "mat = " + mat.dump() );

        Mat img = Highgui.imread("samples/screen/show_recent.png");


        System.out.println(img.rows());
        System.out.println(img.cols());

        Mat mat1 = new Mat(img.rows(), img.cols(), CvType.CV_8UC3);
        Mat imgray = new Mat(img.rows(), img.cols(), CvType.CV_8UC1);
        Mat threshold = new Mat(img.rows(), img.cols(), CvType.CV_8UC1);
        Mat b_img = new Mat(img.rows(), img.cols(), CvType.CV_8UC1);

        //convert img to rgb
        //Imgproc.cvtColor(img, img, Imgproc.COLOR_BGR2RGB);


        Highgui.imwrite("original.jpg", img);

        //convert img to gray scale
        Imgproc.cvtColor(img, imgray, Imgproc.COLOR_RGB2GRAY);

        //threshold image
        Imgproc.threshold(imgray, threshold, 220, 255, Imgproc.THRESH_BINARY);


        //bilateral filter
        Imgproc.bilateralFilter(threshold, b_img, 9, 75, 75);

        int element_size = 4;
        Mat element = Imgproc.getStructuringElement(Imgproc.MORPH_RECT,
                new Size(element_size, element_size));


        Mat opened_image = new Mat(img.rows(), img.cols(), CvType.CV_8UC1);

        Highgui.imwrite("threshold.jpg", threshold);

        Imgproc.erode(threshold, opened_image, element);
        Imgproc.dilate(opened_image, opened_image, element);


        Highgui.imwrite("tmp.jpg", opened_image);

        List<MatOfPoint> contours = new ArrayList<MatOfPoint>();
        Imgproc.findContours(opened_image, contours, new Mat(), Imgproc.RETR_TREE, Imgproc.CHAIN_APPROX_SIMPLE);

        System.out.println(contours.size());

        Point [] min_max=ret_coordinates(contours.get(0), contours.get(1));




        Mat mask = new Mat(img.rows(), img.cols(), CvType.CV_8UC3,new Scalar( 0.0,0.0,0.0 ));

        Rect rt = new Rect(min_max[0],min_max[1]);

        mask.submat(rt).setTo(new Scalar(255,255,255));

        Mat cleaned = new Mat(img.rows(), img.cols(), CvType.CV_8UC3);
        Core.bitwise_and(img,mask,cleaned);

        Highgui.imwrite("cleaned.jpg", cleaned);


        Mat final_image = new Mat(img.rows(), img.cols(), CvType.CV_8UC1);

        Imgproc.threshold(cleaned, final_image, 100, 255, Imgproc.THRESH_BINARY);

        Highgui.imwrite("final_image.jpg", final_image);

    }

    public static Point[] ret_coordinates(MatOfPoint contour_1, MatOfPoint contour_2) {

        double min_x = Integer.MAX_VALUE;
        double min_y = Integer.MAX_VALUE;
        double max_x = Integer.MIN_VALUE;
        double max_y = Integer.MIN_VALUE;


        Point[] c1 = contour_1.toArray();
        Point[] c2 = contour_2.toArray();


        int size = c1.length;


        for (int i = 0; i < size; i++) {
            Point p;
            for (int j = 0; j < 2; j++) {
                if (j == 0) {
                    p = c1[i];
                } else {
                    p = c2[i];
                }

                double x = p.x;
                double y = p.y;

                if (x < min_x) {
                    min_x = x;
                }

                if (x > max_x) {
                    max_x = x;
                }

                if (y < min_y) {
                    min_y = y;
                }

                if (y > max_y) {
                    max_y = y;
                }
            }


        }

        Point points [] = new Point[2];
        points[0] = new Point(min_x,min_y+4);
        points[1] = new Point(max_x,max_y-4);

        return points;


    }
}
