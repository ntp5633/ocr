package processing2;

import net.sourceforge.tess4j.Tesseract;
import net.sourceforge.tess4j.TesseractException;
import org.apache.log4j.Logger;

import java.io.File;

/**
 * Created by npatta001c on 1/12/2015.
 */
public class TextProcessor {

    private Tesseract instance;
    private static Logger logger = Logger.getLogger(TextProcessor.class);
    public TextProcessor(){
        instance = Tesseract.getInstance();
    }

    public String extractContent(String image_path ){
        File imageFile = new File(image_path);

        String result="";
        try {


            result = instance.doOCR(imageFile).trim();
            return result;
        } catch (TesseractException e) {
            logger.error(e.getMessage());
        }


        return result;
    }
}
