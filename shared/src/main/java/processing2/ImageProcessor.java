package processing2;

import org.apache.commons.io.FilenameUtils;
import org.apache.log4j.Logger;
import org.opencv.core.*;
import org.opencv.highgui.Highgui;
import org.opencv.imgproc.Imgproc;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.function.UnaryOperator;


public class ImageProcessor {

    private static Logger logger = Logger.getLogger(ImageProcessor.class);

    static {

        System.loadLibrary(org.opencv.core.Core.NATIVE_LIBRARY_NAME);
    }

    public ImageProcessor(){

    }

    private  Point[] ret_coordinates(MatOfPoint contour_1, MatOfPoint contour_2) {

        double min_x = Integer.MAX_VALUE;
        double min_y = Integer.MAX_VALUE;
        double max_x = Integer.MIN_VALUE;
        double max_y = Integer.MIN_VALUE;


        Point[] c1 = contour_1.toArray();
        Point[] c2 = contour_2.toArray();


        int size = c1.length;


        for (int n = 0; n < 2; n++) {
            Point[] points;

            if (n == 0) {
                points = c1;
            } else {
                points = c2;
            }

            for (int i = 0; i < size; i++) {
                Point p = points[i];
                double x = p.x;
                double y = p.y;

                if (x < min_x) {
                    min_x = x;
                }

                if (x > max_x) {
                    max_x = x;
                }

                if (y < min_y) {
                    min_y = y;
                }

                if (y > max_y) {
                    max_y = y;
                }
            }

        }


        Point points[] = new Point[2];
        points[0] = new Point(min_x, min_y );
        points[1] = new Point(max_x, max_y );

        return points;


    }


    private  int ELEMENT_SIZE = 3;



    public  List<MatOfPoint> filterLinearContours(List<MatOfPoint> points){
        List<MatOfPoint> filteredPoints= new LinkedList<MatOfPoint>();

        for (MatOfPoint p:points){
            int num_points=p.toArray().length;
            if (num_points==4){//line hs 4 points
                filteredPoints.add(p);
            }
        }
        return  filteredPoints;
    }


    public  String getAreaOnFocus(String filepath) {

        logger.debug(String.format("Debugging image %s using item on focus",filepath));
        Mat img = Highgui.imread(filepath);

        String fileNameWithOutExt = FilenameUtils.removeExtension(filepath);
        String basepath=FilenameUtils.getFullPathNoEndSeparator(filepath);


        UnaryOperator<String> newFileName = (operation) -> {
            String dest= fileNameWithOutExt+"_"+operation+".jpg";

            return dest;


           // return "Ich esse " + container[0] + " Kuchen.";
        };



        logger.debug(String.format("Image size: (%s,%s)",img.rows(),img.cols()));


        //convert img to gray scale
        Mat imgray = new Mat(img.rows(), img.cols(), CvType.CV_8UC1);
        Imgproc.cvtColor(img, imgray, Imgproc.COLOR_RGB2GRAY);

        //threshold image
        Mat threshold = new Mat(img.rows(), img.cols(), CvType.CV_8UC1);
        Imgproc.threshold(imgray, threshold, 220, 255, Imgproc.THRESH_BINARY);


        //bilateral filter
        Mat b_img = new Mat(img.rows(), img.cols(), CvType.CV_8UC1);
        Imgproc.bilateralFilter(threshold, b_img, 9, 75, 75);


        Mat erode = Imgproc.getStructuringElement(Imgproc.MORPH_RECT,
                new Size(ELEMENT_SIZE, ELEMENT_SIZE));




        Mat opened_image = new Mat(img.rows(), img.cols(), CvType.CV_8UC1);

        logger.debug(String.format("Image size: (%s,%s)",img.rows(),img.cols()));

        Highgui.imwrite(newFileName.apply("threshold"), threshold);

        Imgproc.erode(threshold, opened_image,erode );
        Imgproc.dilate(opened_image, opened_image, erode);


        Highgui.imwrite(newFileName.apply("opened_image"), opened_image);

        List<MatOfPoint> contours = new ArrayList<MatOfPoint>();
        Imgproc.findContours(opened_image, contours, new Mat(), Imgproc.RETR_TREE, Imgproc.CHAIN_APPROX_SIMPLE);


        contours = filterLinearContours(contours);
        logger.debug(String.format("Found %s contours", contours.size()));




        Point[] min_max = ret_coordinates(contours.get(0), contours.get(1));


        Mat mask = new Mat(img.rows(), img.cols(), CvType.CV_8UC1, new Scalar(0.0));

        Rect rt = new Rect(min_max[0], min_max[1]);

        mask.submat(rt).setTo(new Scalar(255));

        Mat cleaned = new Mat(img.rows(), img.cols(), CvType.CV_8UC3);
        Core.bitwise_and(imgray, mask, cleaned);

        Highgui.imwrite(newFileName.apply("cleaned"), cleaned);


        Mat final_image =cleaned;

        String dest_file_name=newFileName.apply("final_image");
        Highgui.imwrite(dest_file_name, final_image);

        return dest_file_name;

    }



}
