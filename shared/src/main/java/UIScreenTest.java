import model.UIAction;
import org.apache.commons.io.IOUtils;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader;
import org.yaml.snakeyaml.Yaml;
import org.yaml.snakeyaml.constructor.Constructor;
import ui.UIScreen;
import org.apache.velocity.app.VelocityEngine;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.io.InputStream;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

/**
 * Created by npatta001c on 1/13/2015.
 */
public class UIScreenTest {

    public static void main(String[] args) throws Exception {

        /*
        Constructor c = new Constructor(UIScreen.class);
        Yaml yaml = new Yaml(c );

        InputStream resourceAsStream = Main.class.getResourceAsStream("ui_screens/guide.yml");


        String content = IOUtils.toString(resourceAsStream);


        UIScreen us = yaml.loadAs(content, UIScreen.class);
*/


        /*
        VelocityContext context = new VelocityContext();
        MyBean bean = createBean();
        context.put("bean", bean);


        VelocityEngine ve = new VelocityEngine();
        ve.setProperty("file.resource.loader.class", ClasspathResourceLoader.class.getName());
        ve.init();

        Template t = ve.getTemplate("mybean1.vm");
        StringWriter writer = new StringWriter();
        t.merge(context, writer);
        String output = writer.toString().trim().replaceAll("\\r\\n", "\n");

        */


        VelocityContext context = new VelocityContext();
        context.put("mode", "show");


        VelocityEngine ve = new VelocityEngine();
        ve.setProperty("file.resource.loader.class", ClasspathResourceLoader.class.getName());
        ve.init();

        Template t = ve.getTemplate("ui_screens/guide.yml");
        StringWriter writer = new StringWriter();
        t.merge(context, writer);




        Constructor c = new Constructor(UIScreen.class);
        Yaml yaml = new Yaml(c );



        String content = writer.toString().trim();


        UIScreen us = yaml.loadAs(content, UIScreen.class);


        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        Validator validator = factory.getValidator();

        Set<ConstraintViolation<UIScreen>> constraintViolations =
                validator.validate( us );


        int k = 7;
    }


    private static MyBean createBean() {
                  MyBean bean = new MyBean();
                  bean.setId("id123");
                  List<String> list = new ArrayList<String>();
                  list.add("aaa");
                  list.add("bbb");
                  list.add("ccc");
                  bean.setList(list);

                  return bean;
              }
}
