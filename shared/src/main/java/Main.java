import com.google.inject.Guice;
import com.google.inject.Injector;
import config.ApplicationModule;
import driver.BaseDriver;
import model.ActionArguments;
import model.ActionEnum;
import model.Arguments;
import screenshot.IScreenshotTaker;
import ui.UIActionList;

/**
 * Created by Nidhin on 11/21/2014.
 */
public class Main {

    public static void main (String []args){


        Injector injector = Guice.createInjector(new ApplicationModule());

        IScreenshotTaker ist=injector.getInstance(IScreenshotTaker.class);

        ist.takeScreenshot("temp.jpg");


        BaseDriver bd= injector.getInstance(BaseDriver.class);

        ActionArguments a_arguments = new ActionArguments();

        a_arguments.addNamedArgument(Arguments.channeL,"25");


        ActionEnum hot_channel_recording=ActionEnum.valueOf("guide_navigate");

        bd.performAction(hot_channel_recording,a_arguments);




    }
}
