package skeleton.steps;

import cucumber.api.java.Before;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import driver.AndroidDriver;
import driver.ApplicationInterface;
import driver.StbDriver;

import java.util.List;

/**
 * Created by npatta001c on 10/21/2014.
 */
public class StbStepDefs {


    private ApplicationInterface driver;

    @Before("@android")
    public void beforeAndroidScenario() {

        driver = new AndroidDriver();
    }

    @Before("@stb")
    public void beforeSTBScenario() {

        driver = new StbDriver();

    }


    @Given("I have no favorites")
    public void removeAllFavorites() {
        driver.removeAllFavorites();
    }


    @And("^I am on the (.*) screen$")
    public void navigate_to_screen(String screen) throws Throwable {
        driver.navigateToScreen(screen);

    }

    @When("^I type (.*)$")
    public void typed(String word) throws Throwable {
        driver.type(word);

    }

    @And("^I bookmark the top result$")
    public void bookmarkTopResult() throws Throwable {
        driver.bookmark();

    }


    @Then("^I should see (.*) on the (.*) screen in region (.*)$")
    public void validateText(String expectedText, String screen, String region) throws Throwable {
        if (screen.equals("bookmark")) {
            List<String> bookmarks = driver.getAllBookmarks();
        } else {
            throw new Exception("Not Implemented");
        }

    }


}
