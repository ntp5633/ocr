Feature: Component Failure and Channel Recording
  Checks behaviour of channel recording given that a service is down


  Scenario Outline: New Hot recording
    Given I make a hot channel recording
    When RWS is unable <component_down>
    Then I should see a success recording toaster
    And the logs should be valid according to "valid_log"




    Examples:
    |component_down|
    |after_request           |
    |before_end_time        |
    |      after_end_time                 |

