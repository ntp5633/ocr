

Feature: Video Resolution
  Updating to a new firmware should keep the old video resolution

  Scenario: Updating to the latest firmware
    Given I have a device with id 6823055674311460572
    And the current firmware is 14566666
    And I am on the Menu->Settings->Preferences->General screen
    And I capture the below options:
      | option                   |
      | Auto-Tune to HD Channels |
      | Recently Viewed Content  |
      | Show Recent Apps         |
    When I migrate to firmware latest
    And I restart the box
    Then the captured settings should match

