Feature: Bookmark
  User should be able to add a bookmark of a show and add it later

  @stb
  Scenario: Adding a bookmark
    Given I have no favorites
    And I am on the Search screen
    When I type Grimm
    And I bookmark the top result
    Then I should see Grimm on the Bookmark screen in region default


  Scenario: Adding a Bookmark
      Given I have aexisitng favorites
      And I am on the OnDemand Screen