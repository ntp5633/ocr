/*
 *  ===========================================================================
 *  Copyright (c) 2014 Comcast Corp. All rights reserved.
 *  ===========================================================================
 *
 *  Author: ${kguntu001c}
 *  Created: ${9/30/2014}  ${10:22 AM}
 * /
 */

package com.comcast.guide.functests;

import com.comcast.xre.events.VirtualKey;
import com.comcast.xre.testframework.IXREReceiver;
import com.comcast.xre.testframework.testng.XREReceiverDataProvider;
import com.comcast.guide.GuideTestBase;
import com.comcast.guide.actionhandlers.SavedActions;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.Test;

public class SavedMenuTest extends GuideTestBase {

    public static Logger log = LoggerFactory.getLogger(SavedMenuTest.class);

    @Test(dataProviderClass = XREReceiverDataProvider.class, dataProvider="receiverDataProvider")
    public void traverseSavedRecentScreen(IXREReceiver receiver) {
        SavedActions action = init(receiver);
        action.enterSpecifiedScreenAndSelectSubMenu("SAVED_DVR","menu","Recent");
        sleep(1000);
        String currentRowName = action.getCurrentRowName();
        if( StringUtils.isEmpty(currentRowName) ) {
            if( action.isShingleShown() ) {
                //nothing to do here
                return;
            }
        }
        action.sendKey(VirtualKey.DOWN);
        boolean changed = action.waitForRowChange(5000, currentRowName, false);
        if( action.isShingleShown() ) {
            //nothing to do here
            return;
        }
        currentRowName = action.getCurrentRowName();
        action.sendKey(VirtualKey.UP);
        action.waitForRowChange(5000, currentRowName, false);

        action.moveDownOneRow(currentRowName);
        action.traverseGalleryRows();

        action.evaluateForExceptions();
    }

    public SavedActions init(IXREReceiver receiver){
        prepareForTest(receiver, getTestModuleDataClient(), getTestModuleLoggingClient());
        return new SavedActions(receiver,getTestModuleDataClient(),getTestModuleLoggingClient());
    }
}
