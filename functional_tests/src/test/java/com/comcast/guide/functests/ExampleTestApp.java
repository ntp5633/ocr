/* ===========================================================================
 * Copyright (c) 2014 Comcast Corp. All rights reserved.
 * ===========================================================================
 *
 * Author: Comcast
 * Created: 7/9/14  10:58 AM
 */

package com.comcast.guide.functests;

import com.comcast.xre.testframework.IXREReceiver;
import com.comcast.xre.testframework.receivers.HeadlessXREReceiver;
import com.comcast.xre.testframework.services.TestModuleDataClient;
import com.comcast.xre.testframework.testng.XREReceiverDataProvider;
import org.apache.commons.cli.*;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;

public class ExampleTestApp {
    public static Logger log = LoggerFactory.getLogger(ExampleTestApp.class);

    private String serverUrl;
    private String headlessDeviceId;
    private String stbDeviceId;
    public HashMap<String, String> params;
    private TestModuleDataClient dataClient;

    public ExampleTestApp(String xreServerUrl, String headlessDeviceId, String stbDeviceId) {
        this.serverUrl = xreServerUrl;
        this.headlessDeviceId = headlessDeviceId;
        this.stbDeviceId = stbDeviceId;
        this.dataClient = new TestModuleDataClient();
    }

    public void runTest() {
        IXREReceiver[] receiverList = new IXREReceiver[2];
        //receiverList[0] = new StbXREReceiver(stbDeviceId);
        receiverList[0] = new HeadlessXREReceiver(headlessDeviceId, serverUrl);
        XREReceiverDataProvider.addReceivers(receiverList);

        clearStats(stbDeviceId, false);
        clearStats(headlessDeviceId, true);

        receiverList[0].setupReceiver();
        waitForTestModeEnabled(stbDeviceId, receiverList[0]);
        waitForGuideStart(stbDeviceId);
        log.info("TC: XG1 guide is running");

        receiverList[1].setupReceiver();
        waitForTestModeEnabled(headlessDeviceId, receiverList[1]);
        waitForGuideStart(headlessDeviceId);
        log.info("TC: Headless guide is running");

        sleep(4000);

        log.info("TC: Send EXIT key");
        receiverList[0].sendKey("EXIT");
        receiverList[1].sendKey("EXIT");
        sleep(2000);

        log.info("TC: Send GUIDE key");
        receiverList[0].sendKey("GUIDE");
        receiverList[1].sendKey("GUIDE");
        sleep(3000);
        assertStatesEqual(stbDeviceId, headlessDeviceId, "GUIDE key");

        log.info("TC: Send MENU key");
        receiverList[0].sendKey("MENU");
        receiverList[1].sendKey("MENU");
        sleep(3000);
        assertStatesEqual(stbDeviceId, headlessDeviceId, "MENU key");

        log.info("TC: Send ENTER key");
        receiverList[0].sendKey("ENTER");
        receiverList[1].sendKey("ENTER");
        sleep(4000);
        assertStatesEqual(stbDeviceId, headlessDeviceId, "ENTER key");

        receiverList[0].sendKey("EXIT");
        receiverList[1].sendKey("EXIT");

        receiverList[0].exitTest();
        receiverList[1].exitTest();
    }

    private void clearStats(String deviceId, boolean clearStartupValue) {

        dataClient.clearKeys(deviceId+":guide:testModeEnabled", null);//takes two first is dbKey and second is hashKey, dbKey cannot be null
        if( clearStartupValue ) {
            dataClient.clearKeys(deviceId+":guide:startupStatus", null);
        }
        dataClient.clearKeys(deviceId+":guide:state", null);
        dataClient.clearKeys(deviceId+":updatelog", null);

    }

    private void waitForGuideStart(String deviceId) {

        do {
            sleep(5000);
        } while(!isGuideStartupCompleted(deviceId) );

    }

    private boolean isGuideStartupCompleted(String deviceId) {

        String startupCompleted = dataClient.getStringValue(deviceId + ":guide:startupStatus");
        if( startupCompleted!=null && StringUtils.isNotEmpty(startupCompleted)) {
            return StringUtils.equals(startupCompleted,"completed");
        }
        return false;
    }

    private void waitForTestModeEnabled(String deviceId, IXREReceiver receiver) {

        do {
            sleep(5000);
            receiver.sendDeeplinkUrl("shell:setTestMode#enabled=true&url=localhost&testModeEnableAck=testModeEnabled");
        } while(!isGuideStartupCompleted(deviceId) );

    }

    private boolean isTestModeEnabled(String deviceId) {

        String startupCompleted = dataClient.getStringValue(deviceId + ":guide:testModeEnabled");
        if( startupCompleted!=null && StringUtils.isNotEmpty(startupCompleted)) {
            return StringUtils.equals(startupCompleted,"true");
        }

        return false;
    }

    private boolean assertStatesEqual(String deviceId1, String deviceId2, String operation) {

        boolean areEqual = StringUtils.equals(dataClient.hashGet(deviceId1 + ":guide:state", "currentState"), dataClient.hashGet(deviceId2 + ":guide:state", "currentState"));

        if( !areEqual ) {
            log.error("States are not equal after operation: " + operation);
            printStatus(deviceId1);
            printStatus(deviceId2);
        }

        return areEqual;
    }

    private void printStatus(String deviceId) {

        log.info("TJC: " + dataClient.hashGet(deviceId + ":guide:state", "currentState"));
        log.info("TJC: " + dataClient.hashGet(deviceId + ":guide:state", "currentOverlay"));
        log.info("TJC: " + dataClient.hashGet(deviceId + ":guide:state", "activateView"));

    }



    private void sleep(long millis) {
        try {
            Thread.sleep(millis);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    static public void main(String[] args) {
        CommandLine commandLine;
        Options options = buildOptions();
        CommandLineParser parser = new BasicParser();
        try {
            commandLine = parser.parse( options, args);
        }
        catch(Exception e) {
            printUsageAndExit(options);
            return;
        }

        if(args.length < 6) {
            printUsageAndExit(options);
            return;
        }

        String xreServerUrl=null;
        if(commandLine.hasOption("url")) {
            xreServerUrl = commandLine.getOptionValue("url");
        }

        String stbDeviceId=null;
        if(commandLine.hasOption("stbDeviceId")) {
            stbDeviceId = commandLine.getOptionValue("stbDeviceId");
        }

        String headlessDeviceId=null;
        if(commandLine.hasOption("headlessDeviceId")) {
            headlessDeviceId = commandLine.getOptionValue("headlessDeviceId");
        }

        if( StringUtils.isEmpty(xreServerUrl) ) {
            log.error("xreServerUrl is empty");
            System.exit(-1);
        }

        ExampleTestApp test = new ExampleTestApp(xreServerUrl, headlessDeviceId, stbDeviceId);
        test.runTest();
    }

    private static Options buildOptions() {
        Options options = new Options();
        options.addOption("u", "url", true, "xre server url.");
        options.addOption("hdeviceId", "headlessDeviceId", true, "headless device ID.");
        options.addOption("sdeviceId", "stbDeviceId", true, "STB device ID.");
        options.addOption("h", "help", false, "Prints this message.");
        return options;
    }

    private static void printUsageAndExit(Options options) {
        HelpFormatter formatter = new HelpFormatter();
        formatter.printHelp(200,
                "    Test -url <xre server url> -hdeviceId <headless Device ID> -sdeviceId <STB Device ID> [options]",
                "Run example test",
                options,
                "\n");
        System.exit(1);
    }

}
