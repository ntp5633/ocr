package com.comcast.guide.functests;

import com.comcast.guide.actionhandlers.AppDashboardActions;
import com.comcast.guide.GuideTestBase;
import com.comcast.xre.testframework.IXREReceiver;
import com.comcast.xre.testframework.testng.XREReceiverDataProvider;
import com.comcast.xre.testframework.util.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.*;

public class AppDashboardTest extends GuideTestBase
{
    public static Logger log = LoggerFactory.getLogger(AppDashboardTest.class);

    @Test(dataProviderClass = XREReceiverDataProvider.class, dataProvider="receiverDataProvider")
    public void horoscopesLaunch(IXREReceiver receiver)
    {
        launchApp(receiver, "HoroscopesApp");
    }

    @Test(dataProviderClass = XREReceiverDataProvider.class, dataProvider="receiverDataProvider")
    public void pandoraLaunch(IXREReceiver receiver)
    {
        launchApp(receiver, "Pandora");
    }

    @Test(dataProviderClass = XREReceiverDataProvider.class, dataProvider="receiverDataProvider")
    public void sportsLaunch(IXREReceiver receiver)
    {
        launchApp(receiver, "SportsApp");
    }

    @Test(dataProviderClass = XREReceiverDataProvider.class, dataProvider="receiverDataProvider")
    public void stocksLaunch(IXREReceiver receiver)
    {
        launchApp(receiver, "StocksApp");
    }

    @Test(dataProviderClass = XREReceiverDataProvider.class, dataProvider="receiverDataProvider")
    public void trafficLaunch(IXREReceiver receiver)
    {
        launchApp(receiver, "TrafficApp");
    }

    @Test(dataProviderClass = XREReceiverDataProvider.class, dataProvider="receiverDataProvider")
    public void weatherLaunch(IXREReceiver receiver)
    {
        launchApp(receiver, "WeatherApp");
    }

    public void launchApp(IXREReceiver receiver, String durableAppId)
    {
        prepareForTest(receiver, getTestModuleDataClient(), getTestModuleLoggingClient());
        AppDashboardActions action = new AppDashboardActions(receiver,getTestModuleDataClient(),getTestModuleLoggingClient());

        String lastTerminateApp = action.getGuideStateValue("terminatedApp");
        if( lastTerminateApp == null ) lastTerminateApp = "";
        action.launchAppByDurableAppId(durableAppId, 3, X1ToolkitActions.FailureMode.FAIL_ON_TIMEOUT);
        action.pressExit();
        action.waitForStateValueChange("terminatedApp", lastTerminateApp, 20000, true);
        sleep(2000);
        action.evaluateForExceptions();
    }


}
