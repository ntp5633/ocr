/* ===========================================================================
 * Copyright (c) 2014 Comcast Corp. All rights reserved.
 * ===========================================================================
 *
 * Author: kguntu001c
 * Created: 10/12/2014  4:55 PM
 */

package com.comcast.guide.functests;

import com.comcast.guide.actionhandlers.GeneralPreferencesSettingsAction;
import com.comcast.xre.events.VirtualKey;
import com.comcast.xre.testframework.IXREReceiver;
import com.comcast.xre.testframework.testng.XREReceiverDataProvider;
import com.comcast.guide.GuideTestBase;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.Test;
import static com.comcast.guide.GuideTestPoints.*;
import static com.comcast.xre.ToolkitTestPoints.*;
import static org.junit.Assert.assertFalse;
import static org.testng.AssertJUnit.assertTrue;


public class GeneralPreferencesSettingsTest extends GuideTestBase {

    public static Logger log = LoggerFactory.getLogger(GeneralPreferencesSettingsTest.class);

    @Test(dataProviderClass = XREReceiverDataProvider.class, dataProvider="receiverDataProvider")
    public void BackGroundDarknessLevelSettingTest(IXREReceiver receiver)
    {
        GeneralPreferencesSettingsAction action = init(receiver);
        action.clearStackExpectVideoState();
        action.enterSettingsScreenAndSelectPreferences(TP_BACKGROUND_DARKNESS_LEVEL_SETTING_MODULEMODEL);
        action.selectDifferentDarknessLevelsAndCheckPreferenceManagerIsSetWithNewPreference();
        action.evaluateForExceptions();
    }

    @Test(dataProviderClass = XREReceiverDataProvider.class, dataProvider="receiverDataProvider")
    public void setPassedBackGroundDarknessLevelSettingTest(IXREReceiver receiver)
    {
        GeneralPreferencesSettingsAction action = init(receiver);
        action.clearStackExpectVideoState();
        action.enterSettingsScreenAndSelectPreferences(TP_BACKGROUND_DARKNESS_LEVEL_SETTING_MODULEMODEL);
        // Pressing the ENTER key calls up an overlay
        action.sendKey(VirtualKey.ENTER,1000);
        // Wait for specific overlay name
        action.waitForStateValue(TP_CURRENT_OVERLAY,TP_BACKGROUND_TRANSPARENCY_OVERLAY_NAME,5000,true);
        action.selectDesiredDarknessLevelAndCheckItIsSetInPreferenceManager(DARKNESS_LEVEL_10);
        action.evaluateForExceptions();
    }

    @Test(dataProviderClass = XREReceiverDataProvider.class, dataProvider="receiverDataProvider")
    public void BackGroundImageSettingTest(IXREReceiver receiver)
    {
        GeneralPreferencesSettingsAction action = init(receiver);
        action.clearStackExpectVideoState();
        action.enterSettingsScreenAndSelectPreferences(TP_BACKGROUND_DARKNESS_LEVEL_SETTING_MODULEMODEL);
        action.selectDifferentBackgroundImagesAndCheckPreferenceManagerIsSetWithNewPreference();
        action.evaluateForExceptions();
    }

    @Test(dataProviderClass = XREReceiverDataProvider.class, dataProvider="receiverDataProvider")
    public void TurnOnAutoTuneToHDSettingTest(IXREReceiver receiver)
    {
        GeneralPreferencesSettingsAction action = init(receiver);
        action.clearStackExpectVideoState();
        action.enterSettingsScreenAndSelectPreferences(TP_AUTO_TUNE_TO_HD_SETTING_MODULEMODEL);
        action.turnOnSetting(TP_AUTO_TUNE_TO_HD_SETTING);
        action.goBackToFullScreenVideoAndEvaluateExceptions();
        boolean notificationShown = action.tuneToTheSpecifiedChannelAndCheckForX2NotificationIsShown(TP_CNN_CHANNEL_NUMBER);
        assertTrue(notificationShown);
        action.evaluateForExceptions();
    }

    @Test(dataProviderClass = XREReceiverDataProvider.class, dataProvider="receiverDataProvider")
    public void TurnOffAutoTuneToHDSettingTest(IXREReceiver receiver)
    {
        GeneralPreferencesSettingsAction action = init(receiver);
        action.clearStackExpectVideoState();
        action.enterSettingsScreenAndSelectPreferences(TP_AUTO_TUNE_TO_HD_SETTING_MODULEMODEL);
        action.turnOffSetting(TP_AUTO_TUNE_TO_HD_SETTING);
        action.goBackToFullScreenVideoAndEvaluateExceptions();
        boolean notificationShown = action.tuneToTheSpecifiedChannelAndCheckForX2NotificationIsShown(TP_CNN_CHANNEL_NUMBER);
        assertFalse(notificationShown);
        action.evaluateForExceptions();
    }

    @Test(dataProviderClass = XREReceiverDataProvider.class, dataProvider="receiverDataProvider")
    public void recentlyViewedContentSettingOnTest(IXREReceiver receiver)
    {
        GeneralPreferencesSettingsAction action = init(receiver);
        action.clearStackExpectVideoState();
        action.enterSettingsScreenAndSelectPreferences(TP_RECENTLY_WATCHED_SETTING_MODULEMODEL);
        action.turnOnSetting(TP_RECENTLY_WATCHED_SETTING);
        action.goBackToFullScreenVideoAndEvaluateExceptions();
        action.testToSeeLastWatched();
        action.evaluateForExceptions();
    }

    @Test(dataProviderClass = XREReceiverDataProvider.class, dataProvider="receiverDataProvider")
    public void recentlyViewedContentSettingOffTest(IXREReceiver receiver)
    {
        GeneralPreferencesSettingsAction action = init(receiver);
        action.clearStackExpectVideoState();
        action.enterSettingsScreenAndSelectPreferences(TP_RECENTLY_WATCHED_SETTING_MODULEMODEL);
        action.turnOffSetting(TP_RECENTLY_WATCHED_SETTING);
        action.goBackToFullScreenVideoAndEvaluateExceptions();
        action.testToSeeLastWatched();
        action.evaluateForExceptions();
    }

    @Test(dataProviderClass = XREReceiverDataProvider.class, dataProvider = "receiverDataProvider")
    public void recentAppsSettingTest(IXREReceiver receiver) {

        GeneralPreferencesSettingsAction action = init(receiver);
        action.clearStackExpectVideoState();
        action.enterSettingsScreenAndSelectPreferences(TP_SHOW_RECENT_APPS_SETTING_MODULEMODEL);
        action.turnOnSetting(TP_REMEMBER_RECENT_APPS_SETTING);
        action.goBackToFullScreenVideoAndEvaluateExceptions();
        action.testToSeeRecentApps();
        action.evaluateForExceptions();
    }

    public GeneralPreferencesSettingsAction init(IXREReceiver receiver){
        prepareForTest(receiver, getTestModuleDataClient(), getTestModuleLoggingClient());
        return new GeneralPreferencesSettingsAction(receiver,getTestModuleDataClient(),getTestModuleLoggingClient());
    }

}


