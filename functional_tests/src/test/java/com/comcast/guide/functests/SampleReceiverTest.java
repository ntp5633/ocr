package com.comcast.guide.functests;

import com.comcast.xre.events.VirtualKey;
import com.comcast.xre.testframework.IXREReceiver;
import com.comcast.xre.testframework.receivers.HeadlessXREReceiver;
import com.comcast.xre.testframework.receivers.StbXREReceiver;
import com.comcast.xre.testframework.testng.XREReceiverDataProvider;
import com.comcast.xre.testframework.util.TestBase;
import com.comcast.guide.actionhandlers.X1GuideBaseTestActions;
import org.apache.commons.lang.BooleanUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.*;

public class SampleReceiverTest extends TestBase
{
    public static Logger log = LoggerFactory.getLogger(SampleReceiverTest.class);

    @BeforeSuite(alwaysRun = true)
    public void setup() {
        String xreServerUrl = System.getProperty("xreServer");
        String deviceId = System.getProperty("deviceId");
        boolean isHeadless = BooleanUtils.toBoolean(System.getProperty("isHeadless"));

        IXREReceiver[] receiverList = new IXREReceiver[1];
        if( isHeadless ) {
            receiverList[0] = new HeadlessXREReceiver(deviceId, xreServerUrl);
        } else {
            receiverList[0] = new StbXREReceiver(deviceId);
        }
        XREReceiverDataProvider.addReceivers(receiverList);
    }

    @Test(groups="sample", dataProviderClass = com.comcast.xre.testframework.testng.XREReceiverDataProvider.class, dataProvider="receiverDataProvider")
    public void sampleTest(IXREReceiver receiver)
    {
        setReceiver(receiver);
        receiver.setupReceiver();
        X1GuideBaseTestActions action = new X1GuideBaseTestActions(receiver,getTestModuleDataClient(),getTestModuleLoggingClient());
        action.clearStats(receiver.isHeadless());
        action.setAndWaitForTestModeEnabled("guide");
        action.waitForGuideStart(receiver.isHeadless());

        action.sendKey(VirtualKey.EXIT, 2000);
        sleep(2000);
        action.sendKeyExpectState(VirtualKey.GUIDE, "CHANNEL_GRID", 4000, true);
        action.sendKey(VirtualKey.EXIT, 2000);
        action.sendKeyExpectState(VirtualKey.MENU, "X2/MAIN_MENU/PRESENTATION.XML", 4000, true);
        action.sendKey(VirtualKey.RIGHT, 500);
        sleep(1000);
        action.rotateThroughActionBarUtil("mainmenu", "APPS", 10000, true);
        action.sendKey(VirtualKey.ENTER, 500);
        action.sendKey(VirtualKey.RIGHT, 500);
        action.sendKey(VirtualKey.ENTER, 500);
        sleep(10000);
        action.sendKey(VirtualKey.COLOR_KEY_0, 2000);
        sleep(2000);
        action.sendKey(VirtualKey.EXIT, 2000);
        action.simulateEasNotification("START_EAS");
        sleep(12000);
        action.simulateEasNotification("END_EAS");
        action.sendKey(VirtualKey.EXIT,2000);
        action.sendKey(VirtualKey.EXIT, 2000);
        action.clearLogs();
        action.endTestMode("guide");
    }

    @BeforeClass(alwaysRun = true)
    public void init() {
    }

    @BeforeTest(alwaysRun = true)
    public void beforeTest() {
    }

    @AfterTest(alwaysRun = true)
    public void shutdown()
    {
    }
}
