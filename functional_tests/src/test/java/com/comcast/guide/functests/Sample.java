package com.comcast.guide.functests;

import com.comcast.xre.testframework.IXREReceiver;
import com.comcast.xre.testframework.receivers.StbXREReceiver;
import com.comcast.xre.testframework.services.TestModuleDataClient;
import com.comcast.xre.testframework.testng.XREReceiverDataProvider;

/**
 * Created by npatta001c on 10/21/2014.
 */
public class Sample {


    private static String stbDeviceId;
    private static TestModuleDataClient dataClient;

    public static void main(String []args){

        stbDeviceId = "6823055674311460572";
        IXREReceiver[] receiverList = new IXREReceiver[2];
        receiverList[0] = new StbXREReceiver(stbDeviceId);
        XREReceiverDataProvider.addReceivers(receiverList);

        dataClient = new TestModuleDataClient();

        clearStats(stbDeviceId, false);

        receiverList[0].setupReceiver();
//        waitForTestModeEnabled(stbDeviceId, receiverList[0]);
        //waitForGuideStart(stbDeviceId);


        sleep(4000);

        receiverList[0].sendKey("EXIT");
        sleep(2000);

        //log.info("TC: Send GUIDE key");
        receiverList[0].sendKey("GUIDE");
        sleep(3000);

        //log.info("TC: Send MENU key");
        receiverList[0].sendKey("MENU");
        receiverList[1].sendKey("MENU");
        sleep(3000);

        //log.info("TC: Send ENTER key");
        receiverList[0].sendKey("ENTER");
        receiverList[1].sendKey("ENTER");
        sleep(4000);

        receiverList[0].sendKey("EXIT");
        receiverList[1].sendKey("EXIT");

        receiverList[0].exitTest();
        receiverList[1].exitTest();

    }



    private static void sleep(long millis) {
        try {
            Thread.sleep(millis);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }



    private static void clearStats(String deviceId, boolean clearStartupValue) {

        dataClient.clearKeys(deviceId+":guide:testModeEnabled", null);//takes two first is dbKey and second is hashKey, dbKey cannot be null
        if( clearStartupValue ) {
            dataClient.clearKeys(deviceId+":guide:startupStatus", null);
        }
        dataClient.clearKeys(deviceId+":guide:state", null);
        dataClient.clearKeys(deviceId+":updatelog", null);

    }




}
