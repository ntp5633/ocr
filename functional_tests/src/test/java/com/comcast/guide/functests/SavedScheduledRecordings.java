/*
 *  ===========================================================================
 *  Copyright (c) 2014 Comcast Corp. All rights reserved.
 *  ===========================================================================
 *
 *  Author: ${kguntu001c}
 *  Created: ${10/10/8/2014}  ${10:22 AM}
 * /
 */

package com.comcast.guide.functests;

import com.comcast.xre.testframework.IXREReceiver;
import com.comcast.xre.testframework.testng.XREReceiverDataProvider;
import com.comcast.guide.GuideTestBase;
import com.comcast.guide.actionhandlers.SavedActions;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.Test;

public class SavedScheduledRecordings extends GuideTestBase {

    public static Logger log = LoggerFactory.getLogger(SavedFavoritesTest.class);

    @Test(dataProviderClass = XREReceiverDataProvider.class, dataProvider="receiverDataProvider")
    public void TestToSeeRecordingsFromChannelGridAppearInScheduledRecordingsSection(IXREReceiver receiver){
        SavedActions action = init(receiver);

        action.enterFullGuideScreenByMenu();
        String listingIDFromGuide = action.recordFromChannelGridAndReturnItsListingID();

        action.enterSpecifiedScreenAndSelectSubMenu("SAVED_DVR","menu","Scheduled");
        sleep(1000);
        String currentRowName = action.getCurrentRowName();
        if( StringUtils.isEmpty(currentRowName) ) {
            if( action.isShingleShown() ) {
                //nothing to do here
                return;
            }
        }
        action.scrollThroughScheduledRecordingsToFindThePassedListingID(listingIDFromGuide);
    }

    public SavedActions init(IXREReceiver receiver){
        prepareForTest(receiver, getTestModuleDataClient(), getTestModuleLoggingClient());
        return new SavedActions(receiver,getTestModuleDataClient(),getTestModuleLoggingClient());
    }
}


