
package com.comcast.guide.functests;

import com.comcast.xre.events.VirtualKey;
import com.comcast.xre.testframework.IXREReceiver;
import com.comcast.xre.testframework.receivers.HeadlessXREReceiver;
import com.comcast.xre.testframework.receivers.StbXREReceiver;
import com.comcast.xre.testframework.testng.XREReceiverDataProvider;
import com.comcast.guide.actionhandlers.AppDashboardActions;
import com.comcast.xre.testframework.util.TestBase;
import com.comcast.guide.actionhandlers.X1GuideBaseTestActions;
import org.apache.commons.lang.BooleanUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.*;

import static com.comcast.xre.events.VirtualKey.*;

public class BasicSmokeTest2 extends TestBase
{
    public static Logger log = LoggerFactory.getLogger(BasicSmokeTest2.class);

    @BeforeSuite(alwaysRun = true)
    public void setup() {
        String xreServerUrl = System.getProperty("xreServer");
        String deviceId = System.getProperty("deviceId");
        boolean isHeadless = BooleanUtils.toBoolean(System.getProperty("isHeadless"));

        if( XREReceiverDataProvider.receiverListEmpty() ) {
            IXREReceiver[] receiverList = new IXREReceiver[1];
            if (isHeadless) {
                receiverList[0] = new HeadlessXREReceiver(deviceId, xreServerUrl);
            } else {
                receiverList[0] = new StbXREReceiver(deviceId);
            }
            XREReceiverDataProvider.addReceivers(receiverList);
        }
    }

    @Test(groups="sample", dataProviderClass = XREReceiverDataProvider.class, dataProvider="receiverDataProvider")
    public void runBasicPresentations(IXREReceiver receiver)
    {
        setReceiver(receiver);
        receiver.setupReceiver();
        X1GuideBaseTestActions action = new X1GuideBaseTestActions(receiver,getTestModuleDataClient(),getTestModuleLoggingClient());
        action.clearStats(receiver.isHeadless());
        action.setAndWaitForTestModeEnabled("guide");
        action.waitForGuideStart(receiver.isHeadless());

        action.setGuideStateValue("myValue", "value100");
        sleep(1000);
        String myValue = action.getGuideStateValue("placeholder.placeHolderPresentation.initComplete");
        System.out.println("myValue = " + myValue);

        //tryGuideVariations(action);
        trySavedVariations(action);
        //tryOnDemandVariations(action);
        String myValue2 = action.getGuideStateValue("placeholder.placeHolderPresentation.initComplete");
        System.out.println("myValue2 = " + myValue2);
        tryAppDashboard(receiver);

        action.clearLogs();
    }

    private void tryGuideVariations(X1GuideBaseTestActions action) {
        action.clearStackExpectVideoState();
        action.sendKeyMultipleTimes(EXIT, 5);
        action.sendKeyExpectState(VirtualKey.MENU, "X2/MAIN_MENU/PRESENTATION.XML", 4000, true);
        action.rotateThroughActionBarUtil("mainmenu", "GUIDE", 10000, true);
        action.sendKeyExpectState(VirtualKey.ENTER, "CHANNEL_GRID", 4000, true);
        action.sendKeyThenDelay(MENU);
        action.sendKeyThenDelay(GUIDE);
    }


    private void trySavedVariations(X1GuideBaseTestActions action) {
        action.clearStackExpectVideoState();
        action.sendKeyMultipleTimes(EXIT, 5);
        action.sendKeyExpectState(VirtualKey.MENU, "X2/MAIN_MENU/PRESENTATION.XML", 4000, true);
        action.rotateThroughActionBarUtil("mainmenu", "SAVED_DVR", 10000, true);
        action.sendKeyExpectState(VirtualKey.ENTER, "X2/DVR", 10000, true);
        action.evaluateForExceptions();
        sleep(500);

        //forYou(action);
        //recordings(action);
        //scheduled(action);
        favourites(action);
        //purchases(action);
//        action.sendKeyThenDelay(MENU);
    }
    //For You
    private void forYou(X1GuideBaseTestActions action) {
        action.moveThroughActionBarUtil("menu", "Recent", 10000, true);
        action.waitForStateValueChange("placeholder.placeHolderPresentation.initComplete", "", 8000, true);
        sleep(3000);
        String currentRowName = action.getCurrentRowName();
        action.sendKey(VirtualKey.DOWN);
        action.waitForRowChange(5000, currentRowName, false);
        // row should open - wait for an action bar choice
        action.setGuideStateValue("actionBarChoice", "");
        action.sendKey(VirtualKey.ENTER,1000);
        action.waitForStateValueChange("actionBarChoice", "", 8000, true);
        // Action bar choice should be something like watch or resume - we probably should check it here
        // Press ENTER to watch or resume
        action.sendKeyExpectState(VirtualKey.ENTER, "FULL_SCREEN", 10000, true);
        sleep(2000);
        // go back to Full Screen Video
        action.clearStackExpectVideoState();
        action.evaluateForExceptions();
        sleep(500);
        action.sendKeyThenDelay(MENU);
    }

    //Recordings
    private void recordings(X1GuideBaseTestActions action) {
        action.rotateThroughActionBarUtil("mainmenu", "SAVED_DVR", 10000, true);
        action.sendKey(VirtualKey.ENTER);
        action.moveThroughActionBarUtil("menu", "Recordings", 10000, true);
        action.sendKeyExpectState(VirtualKey.ENTER,"X2/DVR", 4000, true);
        sleep(1000);
        action.sendKey(VirtualKey.ENTER,4000);
        sleep(4000);
        action.sendKey(VirtualKey.ENTER,4000);
        sleep(4000);
        action.sendKeyExpectState(VirtualKey.ENTER, "FULL_SCREEN", 4000, true);
        action.evaluateForExceptions();
        sleep(1500);
        action.sendKeyThenDelay(MENU);
    }

    //Scheduled
    private void scheduled(X1GuideBaseTestActions action) {
        action.rotateThroughActionBarUtil("mainmenu", "SAVED_DVR", 10000, true);
        action.sendKey(VirtualKey.ENTER,500);
        action.moveThroughActionBarUtil("menu", "Scheduled", 10000, true);
        action.sendKeyExpectState(VirtualKey.ENTER,"X2/DVR", 4000, true);
        sleep(1000);
        action.sendKey(VirtualKey.ENTER,4000);
        sleep(4000);
        action.sendKey(VirtualKey.ENTER,4000);
        sleep(4000);
        action.sendKey(VirtualKey.ENTER,4000);
        sleep(4000);
        action.sendKeyExpectState(VirtualKey.ENTER, "X2/DVR", 4000, true);
        action.evaluateForExceptions();
        sleep(1500);
        action.sendKeyThenDelay(MENU);
    }

    //Favourites
    private void favourites(X1GuideBaseTestActions action) {
        action.sendKeyMultipleTimes(EXIT, 5);
        action.sendKeyExpectState(VirtualKey.MENU, "X2/MAIN_MENU/PRESENTATION.XML", 4000, true);
        action.rotateThroughActionBarUtil("mainmenu", "SAVED_DVR", 10000, true);
        action.sendKey(VirtualKey.ENTER,500);
        action.moveThroughActionBarUtil("menu", "Favorites", 10000, true);
        action.waitForStateValueChange("placeholder.placeHolderPresentation.initComplete", "", 8000, true);
        sleep(3000);
        String currentRowName = action.getCurrentRowName();
        action.sendKey(VirtualKey.DOWN);
        action.waitForRowChange(5000, currentRowName, false);

        sleep(1000);
        action.sendKey(VirtualKey.DOWN,4000);
        action.evaluateForExceptions();
        sleep(1500);
        action.sendKeyThenDelay(MENU);
    }

    //Purchases
    private void purchases(X1GuideBaseTestActions action) {
        action.rotateThroughActionBarUtil("mainmenu", "SAVED_DVR", 10000, true);
        action.sendKey(VirtualKey.ENTER,500);
        action.moveThroughActionBarUtil("menu", "Purchases", 10000, true);
        action.sendKeyExpectState(VirtualKey.ENTER,"X2/DVR", 4000, true);
        action.sendKeyMultipleTimes(VirtualKey.UP,2);
        sleep(1000);
        action.sendKey(VirtualKey.ENTER,4000);
        sleep(4000);
        action.sendKey(VirtualKey.ENTER,4000);
        sleep(4000);
        action.sendKeyExpectState(VirtualKey.ENTER, "FULL_SCREEN", 4000, true);
        action.evaluateForExceptions();
        sleep(1500);
        action.sendKeyThenDelay(MENU);
    }


   /* private void trySavedVariations(X1GuideBaseTestActions action) {
        action.clearStackExpectVideoState();
        action.sendKeyMultipleTimes(EXIT, 5);
        action.sendKeyExpectState(VirtualKey.MENU, "X2/MAIN_MENU/PRESENTATION.XML", 4000, true);
        action.rotateThroughActionBarUtil("mainmenu", "SAVED_DVR", 10000, true);
        action.sendKeyExpectState(VirtualKey.ENTER, "X2/DVR", 4000, true);
        //For You
        action.moveThroughActionBarUtil("menu", "Recent", 10000, true);
        action.sendKeyExpectState(VirtualKey.ENTER,"X2/DVR", 4000, true);
        action.sendKey(VirtualKey.UP);
        //Recordings
        action.moveThroughActionBarUtil("menu", "Recordings", 10000, true);
        action.sendKeyExpectState(VirtualKey.ENTER,"X2/DVR", 4000, true);
        action.sendKeyMultipleTimes(VirtualKey.UP,2);
        //Scheduled
        action.moveThroughActionBarUtil("menu", "Scheduled", 10000, true);
        action.sendKeyExpectState(VirtualKey.ENTER,"X2/DVR", 4000, true);
        action.sendKeyMultipleTimes(VirtualKey.UP,2);
        //Favourites
        action.moveThroughActionBarUtil("menu", "Favorites", 10000, true);
        action.sendKeyExpectState(VirtualKey.ENTER,"X2/DVR", 4000, true);
        action.sendKeyMultipleTimes(VirtualKey.UP,2);
        //Purchases
        action.moveThroughActionBarUtil("menu", "Purchases", 10000, true);
        action.sendKeyExpectState(VirtualKey.ENTER,"X2/DVR", 4000, true);
        action.sendKeyMultipleTimes(VirtualKey.UP,2);

        action.evaluateForExceptions();
        sleep(500);

        action.sendKeyThenDelay(MENU);
    }*/

   /* private void tryOnDemandVariations(X1GuideBaseTestActions action) {
        action.clearStackExpectVideoState();
        action.sendKeyMultipleTimes(EXIT, 5);
        action.sendKeyExpectState(VirtualKey.MENU, "X2/MAIN_MENU/PRESENTATION.XML", 4000, true);
        action.rotateThroughActionBarUtil("mainmenu", "ONDEMAND", 10000, true);
        action.sendKeyExpectState(VirtualKey.ENTER, "X2/ON_DEMAND/NGB/ROOT.XML", 4000, true);
        action.sendKeyThenDelay(MENU);
    }*/

    private void tryAppDashboard(IXREReceiver receiver) {
        AppDashboardActions action = new AppDashboardActions(receiver,getTestModuleDataClient(),getTestModuleLoggingClient());
        if( action.selectAppInDashboard("Pandora") ) {
            action.sendKey(ENTER, 500);
            sleep(12000);
            action.sendKey(EXIT, 2000);
        }
        action.evaluateForExceptions();
        sleep(500);
        action.sendKey(EXIT, 2000);

        action.endTestMode("guide");
    }

    @BeforeClass(alwaysRun = true)
    public void init() {
    }

    @BeforeTest(alwaysRun = true)
    public void beforeTest() {
    }

    @AfterTest(alwaysRun = true)
    public void endTestMode() {
    }


}
