package com.comcast.guide.functests;

import com.comcast.guide.GuideTestBase;
import com.comcast.guide.actionhandlers.X1GuideBaseTestActions;
import static com.comcast.guide.GuideTestPoints.*;
import static com.comcast.guide.GuideConstants.*;
import com.comcast.xre.testframework.IXREReceiver;
import com.comcast.xre.testframework.testng.XREReceiverDataProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.Test;

public class AppLaunchFromXapiTest extends GuideTestBase
{
    public static Logger log = LoggerFactory.getLogger(AppLaunchFromXapiTest.class);

    @Test(dataProviderClass = XREReceiverDataProvider.class, dataProvider="receiverDataProvider")
    public void horoscopesLaunchByXapi(IXREReceiver receiver)
    {
        prepareForTest(receiver, getTestModuleDataClient(), getTestModuleLoggingClient());
        X1GuideBaseTestActions action = new X1GuideBaseTestActions(receiver,getTestModuleDataClient(),getTestModuleLoggingClient());
        action.launchAppByXAPI("appmanager:launchApplication#activate=true&appId=teamamerica.horoscopes.ga&makeVisible=true&bringToFront=true");
        sleep(2000);
        action.evaluateForExceptions();
    }

    @Test(dataProviderClass = XREReceiverDataProvider.class, dataProvider="receiverDataProvider")
    public void launchApiByXapiAfterScaledVideoModeSet(IXREReceiver receiver)
    {
        prepareForTest(receiver, getTestModuleDataClient(), getTestModuleLoggingClient());
        X1GuideBaseTestActions action = new X1GuideBaseTestActions(receiver,getTestModuleDataClient(),getTestModuleLoggingClient());
        action.setScaledVideoMode(ScaledVideoState.SCALED);
        action.launchAppByXAPI("appmanager:launchApplication#activate=true&appId=teamamerica.horoscopes.ga&makeVisible=true&bringToFront=true");
        action.waitForGuideState("FULL_SCREEN", 5000, true);
        String scaledVideoState = action.getGuideStateValue(TP_SCALED_VIDEO_STATE);
        if( ScaledVideoState.SCALED.name().equals(scaledVideoState) ) {
            action.failTest("Expected full video when app exits back to full screen state.");
        }
        sleep(2000);

        action.evaluateForExceptions();
    }

}
