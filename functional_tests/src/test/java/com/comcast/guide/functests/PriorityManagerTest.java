package com.comcast.guide.functests;

import com.comcast.guide.actionhandlers.PriorityMgrActions;
import static com.comcast.guide.actionhandlers.PriorityMgrActions.RequestStatus;
import com.comcast.xre.events.VirtualKey;
import com.comcast.xre.testframework.IXREReceiver;
import com.comcast.xre.testframework.testng.XREReceiverDataProvider;
import com.comcast.guide.GuideTestBase;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.Test;

import static org.testng.AssertJUnit.assertTrue;

public class PriorityManagerTest extends GuideTestBase
{
    public static Logger log = LoggerFactory.getLogger(PriorityManagerTest.class);

    @Test(dataProviderClass = XREReceiverDataProvider.class, dataProvider="receiverDataProvider")
    public void canEnterPriorityManager(IXREReceiver receiver)
    {
        prepareForTest(receiver, getTestModuleDataClient(), getTestModuleLoggingClient());
        PriorityMgrActions action = new PriorityMgrActions(receiver,getTestModuleDataClient(),getTestModuleLoggingClient());
        action.callupPriorityManager();
    }

    @Test(dataProviderClass = XREReceiverDataProvider.class, dataProvider="receiverDataProvider")
    public void canExitUsingCancelButton(IXREReceiver receiver)
    {
        prepareForTest(receiver, getTestModuleDataClient(), getTestModuleLoggingClient());
        PriorityMgrActions action = new PriorityMgrActions(receiver,getTestModuleDataClient(),getTestModuleLoggingClient());
        RequestStatus status = action.callupPriorityManager();
        switch(status) {
            case SUCCEEDED:
                action.navigateRight();
                action.pressEnter();
                action.waitForStateValue("stateAfterOverlaysPopped", "X2/DVR", 5000, true);
                break;
            case NO_SCHEDULED_RECORDINGS:
                log.warn("No scheduled recordings found.");
                break;
            case FAILED:
                action.failTest("Failed to call up priority manager");
                break;
        }
        action.evaluateForExceptions();
    }

    @Test(dataProviderClass = XREReceiverDataProvider.class, dataProvider="receiverDataProvider")
    public void canExitUsingLastKey(IXREReceiver receiver)
    {
        canExitUsingKeyPress(receiver, VirtualKey.PREV);
    }

    @Test(dataProviderClass = XREReceiverDataProvider.class, dataProvider="receiverDataProvider")
    public void canExitUsingExitKey(IXREReceiver receiver)
    {
        canExitUsingKeyPress(receiver, VirtualKey.EXIT);
    }

    private void canExitUsingKeyPress(IXREReceiver receiver, VirtualKey key)
    {
        prepareForTest(receiver, getTestModuleDataClient(), getTestModuleLoggingClient());
        PriorityMgrActions action = new PriorityMgrActions(receiver,getTestModuleDataClient(),getTestModuleLoggingClient());
        RequestStatus status = action.callupPriorityManager();
        switch(status) {
            case SUCCEEDED:
                action.sendKey(key);
                action.waitForStateValue("stateAfterOverlaysPopped", "X2/DVR", 5000, true);
                break;
            case NO_SCHEDULED_RECORDINGS:
                log.warn("No scheduled recordings found.");
                break;
            case FAILED:
                action.failTest("Failed to call up priority manager");
                break;
        }
        action.evaluateForExceptions();
    }

    @Test(dataProviderClass = XREReceiverDataProvider.class, dataProvider="receiverDataProvider")
    public void canEnterPriorityManager2(IXREReceiver receiver)
    {
        prepareForTest(receiver, getTestModuleDataClient(), getTestModuleLoggingClient());
        PriorityMgrActions action = new PriorityMgrActions(receiver,getTestModuleDataClient(),getTestModuleLoggingClient());
        RequestStatus status = action.callupPriorityManager();
        switch(status) {
            case SUCCEEDED:
                boolean changed;
                int rowCount;
                for(rowCount = 0; rowCount < 15; ++rowCount) {
                    changed = action.moveDownAndWaitForRowChange();
                    if( !changed ) break;
                }
                break;
            case NO_SCHEDULED_RECORDINGS:
                log.warn("No scheduled recordings found.");
                break;
            case FAILED:
                action.failTest("Failed to call up priority manager");
                break;
        }

    }

}
