
package com.comcast.guide.functests;

import com.comcast.guide.actionhandlers.AppDashboardActions;
import com.comcast.guide.GuideTestBase;
import com.comcast.guide.actionhandlers.SavedActions;
import com.comcast.guide.actionhandlers.X1GuideBaseTestActions;
import com.comcast.xre.events.VirtualKey;
import com.comcast.xre.testframework.IXREReceiver;
import com.comcast.xre.testframework.testng.XREReceiverDataProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.*;
import static com.comcast.xre.events.VirtualKey.*;

public class BasicSmokeTest extends GuideTestBase
{
    public static Logger log = LoggerFactory.getLogger(BasicSmokeTest.class);

    @Test(dataProviderClass = XREReceiverDataProvider.class, dataProvider="receiverDataProvider")
    public void runBasicPresentations(IXREReceiver receiver)
    {
        prepareForTest(receiver, getTestModuleDataClient(), getTestModuleLoggingClient());
        X1GuideBaseTestActions action = new X1GuideBaseTestActions(receiver,getTestModuleDataClient(),getTestModuleLoggingClient());

        action.setGuideStateValue("myValue", "value100");
        sleep(1000);
        String myValue = action.getGuideStateValue("placeholder.placeHolderPresentation.initComplete");
        System.out.println("myValue = " + myValue);

//        tryAppDashboard(receiver);




//        tryGuideVariations(action);
        trySaved(receiver);
        //tryOnDemandVariations(action);
        String myValue2 = action.getGuideStateValue("placeholder.placeHolderPresentation.initComplete");
        System.out.println("myValue2 = " + myValue2);
    }

    private void tryGuideVariations(X1GuideBaseTestActions action) {
        action.clearStackExpectVideoState();
        action.sendKeyMultipleTimes(EXIT, 5);
        action.sendKeyExpectState(VirtualKey.MENU, "X2/MAIN_MENU/PRESENTATION.XML", 4000, true);
        action.rotateThroughActionBarUtil("mainmenu", "GUIDE", 10000, true);
        action.sendKeyExpectState(VirtualKey.ENTER, "CHANNEL_GRID", 4000, true);
        action.sendKeyThenDelay(MENU);
        action.sendKeyThenDelay(GUIDE);
//        action.evaluateForExceptions();
    }

    private void trySaved(IXREReceiver receiver) {

        SavedActions savedActions = new SavedActions(receiver,getTestModuleDataClient(),getTestModuleLoggingClient());
        savedActions.clearStackExpectVideoState();
        sleep(500);
        savedActions.testForYouSectionInSavedMenu("Recent");
        savedActions.testRecordingsSectionInSavedMenu("Recordings");
        savedActions.testScheduleRecordingsInSavedMenu("Scheduled");
//        savedActions.recordAndLookForItInScheduledRecordingsInSavedMenu("Scheduled");
//        savedActions.testFavourites("Favorites");


    }







    private void tryAppDashboard(IXREReceiver receiver) {
        AppDashboardActions action = new AppDashboardActions(receiver,getTestModuleDataClient(),getTestModuleLoggingClient());
        if( action.selectAppInDashboard("Pandora") ) {
            action.sendKey(ENTER, 500);
            sleep(12000);
            action.sendKey(EXIT, 2000);
        }
        action.evaluateForExceptions();
        sleep(500);
        action.sendKey(EXIT, 2000);
    }

}
