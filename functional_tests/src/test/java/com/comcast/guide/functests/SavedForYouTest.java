package com.comcast.guide.functests;

import com.comcast.xre.testframework.IXREReceiver;
import com.comcast.xre.testframework.testng.XREReceiverDataProvider;
import com.comcast.guide.GuideTestBase;
import com.comcast.guide.actionhandlers.SavedActions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.Test;

import static org.testng.AssertJUnit.assertTrue;

public class SavedForYouTest extends GuideTestBase
{
    public static Logger log = LoggerFactory.getLogger(SavedForYouTest.class);

    @Test(dataProviderClass = XREReceiverDataProvider.class, dataProvider="receiverDataProvider")
    public void enterPriorityManager(IXREReceiver receiver)
    {
        prepareForTest(receiver, getTestModuleDataClient(), getTestModuleLoggingClient());
        SavedActions action = new SavedActions(receiver,getTestModuleDataClient(),getTestModuleLoggingClient());
        action.enterSpecifiedScreenAndSelectSubMenu("SAVED_DVR","menu","Scheduled");
        if( action.isShingleShown() ) {
            // nothing to do
            return;
        }
        boolean changed;
        changed = action.moveDownAndWaitForPlaceholderFocus();
        assertTrue("Focus should have moved to first row", changed);
        changed = action.moveUpAndWaitForActionBarChoice("seriesPriorityAction");
        if( changed ) {

        }
    }

}
