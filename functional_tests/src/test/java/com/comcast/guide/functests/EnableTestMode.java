
package com.comcast.guide.functests;

import com.comcast.xre.testframework.IXREReceiver;
import com.comcast.xre.testframework.testng.XREReceiverDataProvider;
import com.comcast.guide.GuideTestBase;
import com.comcast.guide.actionhandlers.X1GuideBaseTestActions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.*;

public class EnableTestMode extends GuideTestBase
{
    public static Logger log = LoggerFactory.getLogger(EnableTestMode.class);

    @Test(dataProviderClass = XREReceiverDataProvider.class, dataProvider="receiverDataProvider")
    public void enableTestMode(IXREReceiver receiver)
    {
        prepareForTest(receiver, getTestModuleDataClient(), getTestModuleLoggingClient());
        X1GuideBaseTestActions action = new X1GuideBaseTestActions(receiver,getTestModuleDataClient(),getTestModuleLoggingClient());
        action.clearStats(receiver.isHeadless());
        for(long k = 0; k < 99999; ++k) {
            action.sendX2Notification("Title of notifiation", "Body of Notification");
            Math.random();
        }
    }

    protected void shuttingDownAllTests2(IXREReceiver receiver) {
    }
}
