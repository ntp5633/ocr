package com.comcast.guide.functests;

import com.comcast.guide.GuideTestBase;
import com.comcast.guide.actionhandlers.EntityDataServiceActions;
import com.comcast.xre.testframework.IXREReceiver;
import com.comcast.xre.testframework.testng.XREReceiverDataProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.Test;

public class EntityDataServiceTest extends GuideTestBase {
    public static Logger log = LoggerFactory.getLogger(BasicSmokeTest.class);

    @Test(dataProviderClass = XREReceiverDataProvider.class, dataProvider = "receiverDataProvider")
    public void runBasicPresentations(IXREReceiver receiver) {
        prepareForTest(receiver, getTestModuleDataClient(), getTestModuleLoggingClient());

        EntityDataServiceActions entityDataActions = new EntityDataServiceActions(receiver, getTestModuleDataClient(), getTestModuleLoggingClient());
        entityDataActions.testTVorMoviesSectionOnDemandEpisodesActionBar("Movies");
        entityDataActions.testTVSectionOnDemandEpisodesorMovieTraverse("Movies");

        entityDataActions.testTVorMoviesSectionOnDemandEpisodesActionBar("TV");
        entityDataActions.testTVSectionOnDemandEpisodesorMovieTraverse("TV");


        entityDataActions.testTVorMoviesSectionOnDemandEpisodesActionBar("Kids");
        entityDataActions.testTVSectionOnDemandEpisodesorMovieTraverse("Kids");

        entityDataActions.testTVorMoviesSectionOnDemandEpisodesActionBar("Networks");
        entityDataActions.testTVSectionOnDemandEpisodesorMovieTraverse("Networks");

        entityDataActions.testTVorMoviesSectionOnDemandEpisodesActionBar("Music");
        entityDataActions.testTVSectionOnDemandEpisodesorMovieTraverse("Music");

        entityDataActions.testTVorMoviesSectionOnDemandEpisodesActionBar("Latino");
        entityDataActions.testTVSectionOnDemandEpisodesorMovieTraverse("Latino");

        entityDataActions.testTVorMoviesSectionOnDemandEpisodesActionBar("Streampix");
        entityDataActions.testTVSectionOnDemandEpisodesorMovieTraverse("Streampix");


        entityDataActions.testTVorMoviesSectionOnDemandEpisodesActionBar("Sports & Fitness");
        entityDataActions.testTVSectionOnDemandEpisodesorMovieTraverse("Sports & Fitness");


        entityDataActions.testTVorMoviesSectionOnDemandEpisodesActionBar("Multicultural");
        entityDataActions.testTVSectionOnDemandEpisodesorMovieTraverse("Multicultural");

        entityDataActions.testTVorMoviesSectionOnDemandEpisodesActionBar("Local");
        entityDataActions.testTVSectionOnDemandEpisodesorMovieTraverse("Local");

        entityDataActions.testTVorMoviesSectionOnDemandEpisodesActionBar("Searchlight");
        entityDataActions.testTVSectionOnDemandEpisodesorMovieTraverse("Searchlight");

        entityDataActions.testTVorMoviesSectionOnDemandEpisodesActionBar("XFINITY Services");
        entityDataActions.testTVSectionOnDemandEpisodesorMovieTraverse("XFINITY Services");

    }


}
