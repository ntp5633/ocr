package com.comcast.guide.functests;

import com.comcast.guide.GuideTestBase;
import com.comcast.guide.actionhandlers.AppDashboardActions;
import com.comcast.guide.actionhandlers.X1GuideBaseTestActions;
import com.comcast.xre.testframework.IXREReceiver;
import com.comcast.xre.testframework.testng.XREReceiverDataProvider;
import com.comcast.xre.testframework.util.X1ToolkitActions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.Test;

public class ReferenceAppTest extends GuideTestBase
{
    public static final String DURABLEAPPID_REFAPP = "refappci";
    public static Logger log = LoggerFactory.getLogger(ReferenceAppTest.class);

    @Override
    protected void prepareReceiver(IXREReceiver receiver) {
        super.prepareReceiver(receiver);
        X1GuideBaseTestActions action = new X1GuideBaseTestActions(receiver,getTestModuleDataClient(),getTestModuleLoggingClient());
        action.clearStats(receiver.getDeviceId(), DURABLEAPPID_REFAPP, receiver.isHeadless());
    }

    @Test(dataProviderClass = XREReceiverDataProvider.class, dataProvider="receiverDataProvider")
    public void refappLaunch(IXREReceiver receiver)
    {
        launchApp(receiver, DURABLEAPPID_REFAPP);
    }

    public void launchApp(IXREReceiver receiver, String durableAppId)
    {
        prepareForTest(receiver, getTestModuleDataClient(), getTestModuleLoggingClient());
        AppDashboardActions action = new AppDashboardActions(receiver,getTestModuleDataClient(),getTestModuleLoggingClient());

        action.launchAppByDurableAppId(durableAppId, 3, X1ToolkitActions.FailureMode.FAIL_ON_TIMEOUT);
        action.setAndWaitForTestModeEnabled(durableAppId); //"teamalderaan.localRefApp.test");
        action.moveDownAndWaitForRowChange();
        System.out.println("rowControlChoice=" + action.getGuideStateValue("rowControlChoice"));
        action.moveDownAndWaitForRowChange();
        System.out.println("rowControlChoice=" + action.getGuideStateValue("rowControlChoice"));
        action.moveDownAndWaitForRowChange();
        System.out.println("rowControlChoice=" + action.getGuideStateValue("rowControlChoice"));
        sleep(4000);
        action.pressExit();
    }

}
