/*
 *  ===========================================================================
 *  Copyright (c) 2014 Comcast Corp. All rights reserved.
 *  ===========================================================================
 *
 *  Author: ${kguntu001c}
 *  Created: ${10/7/2014}  ${10:22 AM}
 * /
 */

package com.comcast.guide.functests;

import com.comcast.guide.GuideTestBase;
import com.comcast.guide.actionhandlers.FavoritesActions;
import com.comcast.guide.actionhandlers.SavedActions;
import com.comcast.xre.events.VirtualKey;
import com.comcast.xre.testframework.IXREReceiver;
import com.comcast.xre.testframework.testng.XREReceiverDataProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.Test;

public class RemoveFavoritesTest extends GuideTestBase {

    public static Logger log = LoggerFactory.getLogger(RemoveFavoritesTest.class);

    @Test(dataProviderClass = XREReceiverDataProvider.class, dataProvider="receiverDataProvider")
    public void removeAllFavoritesTest(IXREReceiver receiver)
    {
        FavoritesActions action = init(receiver);
        action.addAChannelFavorite();

        action.clearStackExpectVideoState();
        action.removeAllFavorites();
    }
    public FavoritesActions init(IXREReceiver receiver){
        prepareForTest(receiver, getTestModuleDataClient(), getTestModuleLoggingClient());
        return new FavoritesActions(receiver,getTestModuleDataClient(),getTestModuleLoggingClient());
    }

}

