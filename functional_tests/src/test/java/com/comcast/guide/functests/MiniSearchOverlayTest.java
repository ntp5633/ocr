package com.comcast.guide.functests;

import com.comcast.xre.events.VirtualKey;
import com.comcast.xre.testframework.IXREReceiver;
import com.comcast.xre.testframework.testng.XREReceiverDataProvider;
import com.comcast.guide.GuideTestBase;
import com.comcast.guide.actionhandlers.X1GuideBaseTestActions;
import com.comcast.xre.toolkit.presentation.model.ModuleModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.*;

public class MiniSearchOverlayTest extends GuideTestBase
{
    public static Logger log = LoggerFactory.getLogger(MiniSearchOverlayTest.class);

    @Test(dataProviderClass = XREReceiverDataProvider.class, dataProvider="receiverDataProvider")
    public void miniSearchTest(IXREReceiver receiver)
    {
        prepareForTest(receiver, getTestModuleDataClient(), getTestModuleLoggingClient());
        X1GuideBaseTestActions action = new X1GuideBaseTestActions(receiver,getTestModuleDataClient(),getTestModuleLoggingClient());

        action.clearStackExpectVideoState();
        sleep(2000);
        // Try 244 (Big Bang Theory)
        action.sendKey(VirtualKey.NUMBER_2, 500);
        waitForInput(action, "2");
        action.sendKey(VirtualKey.NUMBER_4, 500);
        waitForInput(action, "24");
        action.sendKey(VirtualKey.NUMBER_4, 500);
        waitForInput(action, "244");
        for(int k = 0; k < 5; ++k) {
            sleep(500);
            ModuleModel moduleModel = action.getModuleModelFromGuideStateValue("searchResults.rowControlChoice");
        }

        action.evaluateForExceptions();
    }

    @Test(dataProviderClass = XREReceiverDataProvider.class, dataProvider="receiverDataProvider")
    public void quickSecondKeyEntryTest(IXREReceiver receiver)
    {
        prepareForTest(receiver, getTestModuleDataClient(), getTestModuleLoggingClient());
        X1GuideBaseTestActions action = new X1GuideBaseTestActions(receiver,getTestModuleDataClient(),getTestModuleLoggingClient());

        action.clearStackExpectVideoState();
        sleep(1000);
        // Try 244 (Big Bang Theory)
        action.sendKey(VirtualKey.NUMBER_2);
        waitForInput(action, "2");
        action.sendKey(VirtualKey.NUMBER_4);
        waitForInput(action, "24");
        action.sendKey(VirtualKey.NUMBER_4, 500);
        waitForInput(action, "244");
        for(int k = 0; k < 5; ++k) {
            sleep(500);
            ModuleModel moduleModel = action.getModuleModelFromGuideStateValue("searchResults.rowControlChoice");
        }

        action.evaluateForExceptions();
    }

    private void waitForInput(X1GuideBaseTestActions action, String desiredInput) {
        action.waitForStateValue("inputModuleText", desiredInput, 5000, true);
    }
}
