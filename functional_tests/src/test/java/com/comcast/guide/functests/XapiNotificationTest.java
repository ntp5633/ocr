package com.comcast.guide.functests;

import com.comcast.guide.GuideTestBase;
import com.comcast.guide.actionhandlers.X1GuideBaseTestActions;
import com.comcast.xre.testframework.IXREReceiver;
import com.comcast.xre.testframework.testng.XREReceiverDataProvider;
import com.comcast.xre.testframework.util.X1ToolkitActions;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.Test;

import java.util.HashMap;

import static org.testng.Assert.assertTrue;

public class XapiNotificationTest extends GuideTestBase
{
    public static Logger log = LoggerFactory.getLogger(XapiNotificationTest.class);

    @Test(dataProviderClass = XREReceiverDataProvider.class, dataProvider="receiverDataProvider")
    public void testX2NotificationByAlertWithValidAction(IXREReceiver receiver)
    {
        sendX2TestNotificationByAlert(receiver, "appmanager:launchApp#appId=teamalderaan.localRefApp.ci", true);
    }

    @Test(dataProviderClass = XREReceiverDataProvider.class, dataProvider="receiverDataProvider")
    public void testX2NotificationByAlertWithInvalidAction(IXREReceiver receiver)
    {
        X1GuideBaseTestActions action = new X1GuideBaseTestActions(receiver,getTestModuleDataClient(),getTestModuleLoggingClient());
        sendX2TestNotificationByAlert(receiver, "appmanager:launchApp#appId=invalidAppId", false);
        String value = action.getAppStateValue("shell", "x2NotificationShown");
        assertTrue(StringUtils.isEmpty(value));
    }

    private void sendX2TestNotificationByAlert(IXREReceiver receiver, String deeplinkActionUrl, boolean failOnTimeout) {
        prepareForTest(receiver, getTestModuleDataClient(), getTestModuleLoggingClient());
        X1GuideBaseTestActions action = new X1GuideBaseTestActions(receiver,getTestModuleDataClient(),getTestModuleLoggingClient());

        HashMap<String,String> paramsMap = new HashMap<String,String>();
        paramsMap.put("version", "2.0");
        paramsMap.put("messageType", "TVMessage");
        paramsMap.put("messageTitle", "title");
        paramsMap.put("messageBody", "body");
        paramsMap.put("validateDeeplinkAction", "true");
        paramsMap.put("btnActions", deeplinkActionUrl);
        paramsMap.put("btnTexts", "Info");
        //paramsMap.put("iconUrl");

        action.setAppStateValue("shell", "x2NotificationShown", "");
        action.sendX2Notification(paramsMap);
        sleep(2000);
        action.waitForAppStateValueChange("shell", "x2NotificationShown", "", 8000, failOnTimeout);
        action.evaluateForExceptions();

    }

    // Placeholder
    // "notification:queueNotification#
    // notificationTitleText=15%20minutes%20timeout&amp;
    // notificationDetailText=Useful%20to%20test%20with%20Guide%20and%20ScreenSaver&amp;
    // notificationImage=X2_NOTIFICATION_TEXTMESSAGE&amp;
    // notificationActionImage=X2_INFO&amp;
    // notificationDeepLinkUrl=guideoverlaymgr%3ApopupModalOverlay%23type%3DX2_NOTIFICATION_OVERLAY%26url%3D%257BSTATIC_FILES_BASE_URL%257D%2Ffeeds%2Fx2%2Fsettings%2Foverlays%2Fnotification_overlay.xml&amp;
    // notificationTimeoutInSeconds=900"/>
    //paramsMap.put("notificationDeepLinkUrl", "xyz:bbb");
    //paramsMap.put("messageBodyImage");
    //paramsMap.put("duration");

}
