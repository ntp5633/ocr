package com.comcast.xre

import org.scalatest.FunSuite

import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner
import org.testng.annotations.Test

/**
 * This class is a test suite
 */
@RunWith(classOf[JUnitRunner])
@Test
class HellowWorldTest extends FunSuite {
  test("this or that") {
      println("Hello")
  }
}
