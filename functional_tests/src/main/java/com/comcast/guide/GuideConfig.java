/* ===========================================================================
 * Copyright (c) 2014 Comcast Corp. All rights reserved.
 * ===========================================================================
 *
 * Author: Comcast
 * Created: 10/15/2014  12:47 PM
 */

package com.comcast.guide;

public class GuideConfig {
    private final String STATE_MAIN_MENU = "X2/MAIN_MENU/PRESENTATION.XML";
    private final String STATE_FULL_GRID = "CHANNEL_GRID";
    private final String STATE_FULL_SCREEN = "FULL_SCREEN";
    private final String STATE_LAST_NINE = "X2/LAST_NINE";

    public enum MainMenuChoice {
        GUIDE, SAVED, APPS, SETTINGS
    }

    public String getMainMenuStateID() {
        return STATE_MAIN_MENU;
    }

    public String getFullGridStateID() {
        return STATE_FULL_GRID;
    }

    public String getFullScreenStateID() {
        return STATE_FULL_SCREEN;
    }

    public String getMainMenuActionBarName() {
        return "mainmenu";
    }

    public String getMainMenuChoiceName(MainMenuChoice choiceName) {
        String name = "unknown";

        switch(choiceName) {
            case GUIDE:
                name = "GUIDE";
                break;
        }

        return name;
    }

    public String getLastNineStateID() {
        return STATE_LAST_NINE;
    }
}
