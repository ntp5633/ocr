/* ===========================================================================
 * Copyright (c) 2014 Comcast Corp. All rights reserved.
 * ===========================================================================
 *
 * Author: Comcast
 * Created: 10/15/2014  12:47 PM
 */

package com.comcast.guide;

public interface GuideTestPoints {
    String TP_STARTUP_STATUS = "startupStatus";
    String TP_ACTIVATED_VIEW = "activatedView";
    String TP_CURRENT_STATE = "currentState";
    String TV_STARTUP_COMPLETED = "completed";

    String TP_STATE_AFTER_OVERLAYS_POPPED = "stateAfterOverlaysPopped";

    //Written when ScaledVideoController's setState is called
    String TP_SCALED_VIDEO_STATE = "scaledVideoState";

    // When listing ID changes (in Grid)
    String TP_CURR_CHOICE_LISTING_ID = "listingIdOfCurrentChoice";

    // Written when an application, in particular an IAbstractApplication type app, is activated
    String TP_ACTIVATED_APP = "activatedApp";

    // Written when an application, in particular an IAbstractApplication type app, is deactivated
    String TP_DEACTIVATED_APP = "deactivatedApp";

    // Written when an application is terminated
    String TP_TERMINATED_APP = "terminatedApp";

    //Written if an old style XcalApplication fails to complete initialization
    String TP_APPRESOURCELOADFAIL = "appResourceLoadFail";

    // Written when Recently Watched Settings is changed
    String TP_RECENTLY_WATCHED_SETTING = "REMEMBER_WATCHED_PROGRAMS";

    // Written when Auto tune to HD Settings is changed
    String TP_AUTO_TUNE_TO_HD_SETTING = "ALWAYS_WATCH_IN_HD";

    String TP_RECENTLY_WATCHED_SETTING_MODULEMODEL = "RecentlyViewedContentModuleModel";

    String TP_AUTO_TUNE_TO_HD_SETTING_MODULEMODEL = "AutoTunetoHDModuleModel";

    String TP_BACKGROUND_DARKNESS_LEVEL_SETTING_MODULEMODEL = "BackgroundTransparencyOverlayModuleModel";

    String TP_SHOW_RECENT_APPS_SETTING_MODULEMODEL = "RecentAppsModuleModel";

    String TP_CNN_CHANNEL_NUMBER = "005";

    //Written when Background Darkness Level Is changed
    String TP_BACKGROUND_TRANSPARENCY_OVERLAY_NAME = "BACKGROUND_TRANSPARENCY_OVERLAY";

    // Written when Show Recent Apps Setting is set
    String TP_REMEMBER_RECENT_APPS_SETTING = "REMEMBER_RECENT_APPS";

    String DARKNESS_LEVEL_10 = "DARKNESS_LEVEL_10";



}
