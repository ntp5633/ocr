/* ===========================================================================
 * Copyright (c) 2014 Comcast Corp. All rights reserved.
 * ===========================================================================
 *
 * Author: Comcast
 * Created: 9/26/2014  12:47 PM
 */

package com.comcast.guide;

import com.comcast.guide.actionhandlers.X1GuideBaseTestActions;
import com.comcast.xre.testframework.IXREReceiver;
import com.comcast.xre.testframework.services.TestModuleDataClient;
import com.comcast.xre.testframework.services.TestModuleLoggingClient;
import com.comcast.xre.testframework.util.TestBase;

public class GuideTestBase extends TestBase {

    protected void prepareReceiver(IXREReceiver receiver) {
        X1GuideBaseTestActions action = new X1GuideBaseTestActions(receiver,getTestModuleDataClient(),getTestModuleLoggingClient());
        if( !action.testDataConnection(receiver.getDeviceId()) ) {
            setConnectionFailure(true);
            return;
        }
        action.clearStats(receiver.isHeadless());
        action.clearLogs();
        action.setAndWaitForTestModeEnabled("shell");
        action.setAndWaitForTestModeEnabled("guide");
        action.waitForGuideStart(receiver.isHeadless());
    }

    protected void prepareForTest(IXREReceiver receiver, TestModuleDataClient dataClient, TestModuleLoggingClient logClient) {
        setReceiver(receiver);
        X1GuideBaseTestActions action = new X1GuideBaseTestActions(receiver,getTestModuleDataClient(),getTestModuleLoggingClient());
    }

    protected void shuttingDownAllTests(IXREReceiver receiver) {
        X1GuideBaseTestActions action = new X1GuideBaseTestActions(receiver,getTestModuleDataClient(),getTestModuleLoggingClient());
        String exceptions = action.getExceptionLogs();
        if( !exceptions.isEmpty() ) {
            System.out.println(exceptions);
        }
        action.endTestMode("guide");
        action.endTestMode("shell");
    }
}
