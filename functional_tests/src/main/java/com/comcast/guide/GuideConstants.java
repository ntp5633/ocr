/* ===========================================================================
 * Copyright (c) 2014 Comcast Corp. All rights reserved.
 * ===========================================================================
 *
 * Author: Comcast
 * Created: 10/15/2014  12:47 PM
 */

package com.comcast.guide;

public interface GuideConstants {
    enum ScaledVideoState {
        FULL, SCALED
    }

}
