/* ===========================================================================
 * Copyright (c) 2014 Comcast Corp. All rights reserved.
 * ===========================================================================
 *
 * Author: TCarro004
 * Created: 9/10/2014  12:45 PM
 */

package com.comcast.guide.actionhandlers;

import com.comcast.guide.GuideConstants;
import com.comcast.xre.events.VirtualKey;
import com.comcast.guide.GuideConfig;
import static com.comcast.guide.GuideTestPoints.*;
import static com.comcast.xre.ToolkitTestPoints.*;
import static com.comcast.guide.GuideConfig.MainMenuChoice.*;
import com.comcast.xre.testframework.IXREReceiver;
import com.comcast.xre.testframework.services.TestModuleDataClient;
import com.comcast.xre.testframework.services.TestModuleLoggingClient;
import com.comcast.xre.testframework.util.X1ToolkitActions;
import com.comcast.xre.toolkit.presentation.model.ModuleModel;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

import static org.testng.AssertJUnit.fail;

public class X1GuideBaseTestActions extends X1ToolkitActions {
    public static Logger log = LoggerFactory.getLogger(X1GuideBaseTestActions.class);

    public static final String SUBKEY_GUIDE_TEST_MODE_ENABLED = ":guide:testModeEnabled";
    private static final String SUBKEY_GUIDE_STARTUP_STATUS = ":guide:startupStatus";
    private static final String SUBKEY_GUIDE_STATE_GROUP = ":guide:state";
    private static final String SUBKEY_SHELL_STATE_GROUP = ":shell:state";
    private static final String GUIDE_STATE_KEY_CURRENT_STATE = "currentState";
    private static final String GUIDE_STATE_KEY_ACTIONBAR_CHOICE = TP_ACTIONBAR_CHOICE;
    private static final String SUBKEY_GUIDE_RECEIVERID = ":guide:receiverId";
    private static final String VALUE_COMPLETED = "completed";
    public static final String KEY_RECEIVER_ID = "receiverId";

    protected GuideConfig guideConfig;

    public X1GuideBaseTestActions(IXREReceiver receiver, TestModuleDataClient dataClient, TestModuleLoggingClient loggingClient) {
        super(receiver, dataClient, loggingClient);
        guideConfig = new GuideConfig();
    }

    public void setAndWaitForTestModeEnabled() {
        setAndWaitForTestModeEnabled(receiver.getDeviceId(), receiver, "guide");
    }

    public boolean sendKeyExpectState(VirtualKey key, String expectedState, long maxWaitMillis, boolean failOnTimeout) {
        sendKey(key);
        return waitForGuideState(expectedState, maxWaitMillis, failOnTimeout);
    }

    public void goBackToFullScreenVideoAndEvaluateExceptions() {
        clearStackExpectVideoState();
        evaluateForExceptions();
        sleep(500);
    }

    private static String lastStateSeen;
    public boolean clearStackExpectVideoState() {
        sendKeyExpectState(VirtualKey.MENU, guideConfig.getMainMenuStateID(), 4000, false);
        DoUntilDoneOrTimeout task = new DoUntilDoneOrTimeout("clearStackExpectVideoState",16000, FailureMode.FAIL_ON_TIMEOUT) {
            public boolean task() {
                sendKey(VirtualKey.EXIT, 300);
                String currentState = getGuideState();
                lastStateSeen = currentState;
                return StringUtils.equals(guideConfig.getFullScreenStateID(),currentState);
            }
            public void onTimeout() {
                System.err.println("clearStackExpectVideoState() timeout: lastStateSeen=" + lastStateSeen);
            }
            };
        return task.start();
    }

    private String getKeyName(String viewName, String hashKey) {
        String modifier = "";
        if( viewName != null && !viewName.isEmpty() ) {
            modifier = viewName + ".";
        }

        return modifier + hashKey;
    }

    public String getCurrentActionBarChoiceName(String actionBarName) {
        String rtnName = "";
        String currentChoice = getCurrentActionBarChoice(actionBarName);
        if( !StringUtils.isEmpty(currentChoice)) {
            String[] elements = currentChoice.split(",");
            if( elements.length >= 2 ) {
                if ( !StringUtils.isEmpty(elements[0]) ) {
                    rtnName = elements[0];
                } else {
                    rtnName = elements[1];
                }
            }
        }
        return rtnName;
    }

    public void setCurrentActionBarChoice(String actionBarName, String value) {
        setGuideStateValue(getKeyName(actionBarName,GUIDE_STATE_KEY_ACTIONBAR_CHOICE), value);
    }

    public String getCurrentActionBarChoice(String actionBarName) {
        return dataClient.hashGet(receiver.getDeviceId() + SUBKEY_GUIDE_STATE_GROUP, getKeyName(actionBarName,GUIDE_STATE_KEY_ACTIONBAR_CHOICE));
    }

    public boolean waitForActionBarChoiceChange(String actionBarName, long maxWaitMillis, boolean failOnTimeout) {
        String currentValue = getCurrentActionBarChoice(actionBarName);
        return waitForStateValueChange(getKeyName(actionBarName,GUIDE_STATE_KEY_ACTIONBAR_CHOICE), currentValue, maxWaitMillis, failOnTimeout);
    }

    public void waitForGuideStart(boolean isHeadlessReceiver) {
        waitForGuideStart(receiver.getDeviceId(), isHeadlessReceiver);
    }

    private void waitForGuideStart(String deviceId, boolean isHeadlessReceiver) {
        int timeoutCounter = 24;

        do {
            sleep(5000);
            if( --timeoutCounter == 0 ) {
                failTest("timeout waiting for test mode enabled acknowledgement");
            }
        } while(!isGuideStartupCompleted(deviceId, isHeadlessReceiver) );

    }

    private boolean isGuideStartupCompleted(String deviceId, boolean isHeadlessReceiver) {
        if( dataClient == null ) {
            return false;
        }

        if( isHeadlessReceiver ) {
            String startupCompleted = dataClient.getStringValue(deviceId + SUBKEY_GUIDE_STARTUP_STATUS);
            if( startupCompleted!=null && StringUtils.isNotEmpty(startupCompleted)) {
                return StringUtils.equals(startupCompleted,VALUE_COMPLETED);
            }
        } else {
            sendKey(VirtualKey.EXIT,2000);
            String currentGuideState = getGuideState();
            if( currentGuideState == null ) {
                sendKey(VirtualKey.MENU, 1000);
                sendKey(VirtualKey.EXIT, 500);
            }
            sleep(500);
            boolean success = sendKeyExpectState(VirtualKey.GUIDE, guideConfig.getFullGridStateID(), 4000, false);
            if( success ) {
                return true;
            }
        }
        return false;
    }

    public String getGuideState() {
        return dataClient.hashGet(receiver.getDeviceId() + SUBKEY_GUIDE_STATE_GROUP, GUIDE_STATE_KEY_CURRENT_STATE);
    }

    public String getShellStateValue(String key) {
        return dataClient.hashGet(receiver.getDeviceId() + SUBKEY_SHELL_STATE_GROUP, key);
    }

    public void setShellStateValue(String key, String value) {
        String requestKey = this.buildRequestKey(receiver.getDeviceId(), "shell", "state");
        String groupValue = dataClient.setHashOrSetValue(null, key, value);
        dataClient.setHashValue(requestKey, groupValue);
    }

    public String getGuideStateValue(String key) {
        return dataClient.hashGet(receiver.getDeviceId() + SUBKEY_GUIDE_STATE_GROUP, key);
    }

    public void setGuideStateValue(String key, String value) {
        String requestKey = this.buildRequestKey(receiver.getDeviceId(), "guide", "state");
        String groupValue = dataClient.setHashOrSetValue(null, key, value);
        dataClient.setHashValue(requestKey, groupValue);
    }

    public boolean waitForGuideState(String expectedState, long maxWaitMillis, boolean failOnTimeout) {
        long delayInMillis = 500;
        long timeoutCounter = maxWaitMillis/delayInMillis;
        if( timeoutCounter == 0 ) {
            timeoutCounter = 1;
        }

        String currentState;
        do {
            sleep(delayInMillis);
            currentState = getGuideState();
            if( StringUtils.equals(expectedState,currentState) ) {
                return true;
            }
        } while( --timeoutCounter != 0 );

        if( failOnTimeout ) {
            failTest("waitForGuideState: timeout waiting for currentGuideState to change to " + expectedState + ".  Last seen state=" + currentState);
        }

        return false;
    }

    public boolean waitForStateValue(String valueSubKey, String expectedValue, long maxWaitMillis, boolean failOnTimeout) {
        long delayInMillis = 500;
        long timeoutCounter = maxWaitMillis/delayInMillis;
        if( timeoutCounter == 0 ) {
            timeoutCounter = 1;
        }

        String currentValue;
        do {
            sleep(delayInMillis);
            currentValue = getGuideStateValue(valueSubKey);
            if( StringUtils.equals(expectedValue,currentValue) ) {
                return true;
            }
        } while( --timeoutCounter != 0 );

        if( failOnTimeout ) {
            failTest("timeout waiting for stateValue[" + valueSubKey + "] to change to <" + expectedValue + ">.  Last seen state=" + currentValue);
        }

        return false;
    }

    public boolean waitForStateValueChange(String valueSubKey, String initialValue, long maxWaitMillis, boolean failOnTimeout) {
        long delayInMillis = 500;
        long endTime = System.currentTimeMillis() + maxWaitMillis;

        String currentValue;
        do {
            sleep(delayInMillis);
            currentValue = getGuideStateValue(valueSubKey);
            if( !StringUtils.equals(initialValue,currentValue) ) {
                return true;
            }
        } while( System.currentTimeMillis() < endTime );

        if( failOnTimeout ) {
            failTest("waitForStateValueChange: timeout waiting for guideStateValue(" + valueSubKey + ") to change from " + initialValue + ".  Last seen value=" + currentValue);
        }

        return false;
    }

    public boolean moveAndWaitForActionBarChoice(VirtualKey key, String actionBarName, long maxWaitMillis, boolean failOnTimeout) {
        final long delayTimeMillis = 300;
        long endTime = System.currentTimeMillis() + maxWaitMillis;

        String prevChoice = getCurrentActionBarChoice(actionBarName);
        sendKey(key);
        String currentChoice;
        // Wait until choice changes
        do {
            sleep(delayTimeMillis);
            currentChoice = getCurrentActionBarChoice(actionBarName);
            if( currentChoice != null && !StringUtils.equals(prevChoice,currentChoice) ) {
                return true;
            }
            prevChoice = currentChoice;
        } while( System.currentTimeMillis() < endTime );

        if( failOnTimeout ) {
            failTest("timeout waiting for actionBarChoice to change. Last seen choice=" + currentChoice);
        }

        return false;
    }

    public boolean rotateThroughActionBarUtil(String actionBarName, String desiredChoice, long maxWaitMillis, boolean failOnTimeout) {
        final long delayTimeMillis = 300;
        long endTime = System.currentTimeMillis() + maxWaitMillis;

        String currentChoice;
        do {
            sleep(delayTimeMillis);
            moveAndWaitForActionBarChoice(VirtualKey.RIGHT, actionBarName, maxWaitMillis, false);
            currentChoice = getCurrentActionBarChoiceName(actionBarName);
            if( !StringUtils.isEmpty(currentChoice)) {
                if (StringUtils.equals(desiredChoice, currentChoice)) {
                    return true;
                }
            }
        } while( System.currentTimeMillis() < endTime );

        evaluateForFatalExceptions();

        if( failOnTimeout ) {
            failTest("timeout waiting for actionBarChoice to change to " + desiredChoice + ".  Last seen state=" + currentChoice);
        }

        return false;
    }

    public boolean keepMovingRightOnTheActionBarUntilPassedActionBarNameIsFound(String desiredChoice,long maxWaitMillis, boolean failOnTimeout){
        long delayTimeMillis = 250;
        String currentChoice;
        long endTime = System.currentTimeMillis() + maxWaitMillis;

        do {
            sleep(delayTimeMillis);
            currentChoice = getActionBarChoiceName(getGuideStateValue(TP_ACTIONBAR_CHOICE));
            if( !StringUtils.isEmpty(currentChoice)) {

                if (StringUtils.equals(desiredChoice, currentChoice)) {
                    return true;
                }
            }
            sendKey(VirtualKey.RIGHT);
        } while( System.currentTimeMillis() < endTime );

        evaluateForFatalExceptions();

        if( failOnTimeout ) {
            failTest("timeout waiting for actionBarChoice to change to " + desiredChoice + ".  Last seen state=" + currentChoice);
        }

        return false;
    }


    public String getActionBarChoiceName(String actionBarData) {
        if( StringUtils.isEmpty(actionBarData) ) {
            return "";
        }
        String[] elements = actionBarData.split(",");
        if( elements.length < 2 ) {
            return "";
        }

        String name;
        if( !elements[0].isEmpty() ) {
            name = elements[0];
        } else {
            name = elements[1];
        }
        return name;
    }



   //ToDo get rid of hard coded strings
    enum Direction {
       RIGHT, LEFT
    }

    public boolean moveThroughActionBarUtil(String actionBarName, String desiredChoice, long maxWaitMillis, boolean failOnTimeout) {
        Direction direction = Direction.RIGHT;
        long delayTimeMillis = 200;
        long timeoutCounter = maxWaitMillis/delayTimeMillis;
        if( timeoutCounter == 0 ) {
            timeoutCounter = 1;
        }
        String currentChoice = null;
        do {
            sleep(delayTimeMillis);
            String previousChoice = currentChoice;

            currentChoice = getCurrentActionBarChoice(actionBarName);
            if( !StringUtils.isEmpty(currentChoice)) {
                String[] elements = currentChoice.split(",");
                if (StringUtils.equals(desiredChoice, elements[0])) {
                    return true;
                }else if (StringUtils.equals(desiredChoice, elements[1])) {
                    return true;
                }
            }
            if(direction == Direction.RIGHT && currentChoice.equals(previousChoice) ){
                direction = Direction.LEFT;
            }
            else if(direction == Direction.LEFT && currentChoice.equals(previousChoice) ){
                // stop, we didn't find anything;
            }

            setGuideStateValue(TP_SHINGLE_SHOWN, "");
            setGuideStateValue(TP_PLACEHOLDER_INITCOMPLETE, "");
            if( direction == Direction.LEFT ){
                sendKey(VirtualKey.LEFT);
            }
            else if( direction == Direction.RIGHT ){
                sendKey(VirtualKey.RIGHT);
                waitForStateValueChange(TP_PLACEHOLDER_INITCOMPLETE,"",8000, false);
            }
        } while( --timeoutCounter != 0 );

        evaluateForFatalExceptions();

        if( failOnTimeout ) {
            failTest("timeout waiting for actionBarChoice to change to " + desiredChoice + ".  Last seen state=" + currentChoice);
        }

        return false;
    }

    public boolean moveThroughRowControlPresentation(String desiredChoice,long maxWaitMillis, boolean failOnTimeout){
        long delayTimeMillis = 200;
        String currentChoice;
        long endTime = System.currentTimeMillis() + maxWaitMillis;

        do {
            sleep(delayTimeMillis);
            currentChoice = getGuideStateValue(TP_ROWCONTROL_CHOICE);
            if( !StringUtils.isEmpty(currentChoice)) {
                String[] elements = currentChoice.split(",");
                if (StringUtils.equals(desiredChoice, elements[0])) {
                    return true;
                }
            }

            sendKey(VirtualKey.DOWN);
            waitForStateValueChange(TP_ROWCONTROL_CHOICE,currentChoice,8000,false);
        }  while( System.currentTimeMillis() < endTime );

        evaluateForFatalExceptions();

        if( failOnTimeout ) {
            failTest("timeout waiting for rowControlChoice : " + desiredChoice + ".  Last seen state=" + currentChoice);
        }

        return false;
    }

    public String getCurrentRowName() {
        return getCurrentRowNameFromModuleModel(getGuideStateValue(TP_ROWCONTROL_CHOICE));
    }

    public String getCurrentSelectionListingId() {
        return getCurrentSelectionListingIDFromModuleModel(getGuideStateValue(TP_ROWCONTROL_CHOICE));
    }

    public String getCurrentSelectionEntityType() {
        return getCurrentSelectionEntityTypeFromModuleModel(getGuideStateValue(TP_ROWCONTROL_CHOICE));
    }

    public String getCurrentSelectionEntityID() {
        return getCurrentSelectionEntityIDFromModuleModel(getGuideStateValue(TP_ROWCONTROL_CHOICE));
    }

    public boolean enterFullGuideScreenByMenu() {
        goToFullScreenVideoAndEvaluateExceptions();
        findItemInMainMenu(guideConfig.getMainMenuChoiceName(GUIDE));
        sendKeyExpectState(VirtualKey.ENTER, guideConfig.getFullGridStateID(), 5000, true);
        return true;
    }

    public boolean enterFullGuideScreenByGuideKey() {
        goToFullScreenVideoAndEvaluateExceptions();
        sendKeyExpectState(VirtualKey.GUIDE, guideConfig.getFullGridStateID(), 5000, true);
        return true;
    }

    public boolean findItemInMainMenu(String mainMenuItem){
        sendKeyExpectState(VirtualKey.MENU, guideConfig.getMainMenuStateID(), 4000, true);
        return rotateThroughActionBarUtil(guideConfig.getMainMenuActionBarName(), mainMenuItem, 14000, true);
    }

    public boolean findItemInSubMenu(String subMenuActionBarName, String subMenuItem) {
        boolean isFound = moveThroughActionBarUtil(subMenuActionBarName, subMenuItem, 15000, true);
        if (!isFound) {
            return false;
        }
        waitForStateValueChange(TP_PLACEHOLDER_INITCOMPLETE, "", 8000, false);
        sleep(3000);
        return true;
    }

    public boolean enterSpecifiedScreenAndSelectSubMenu(String screenName,String subMenuActionBarName, String subMenuItem) {
        goBackToFullScreenVideoAndEvaluateExceptions();
        findItemInMainMenu(screenName);
        setGuideStateValue(TP_PLACEHOLDER_INITCOMPLETE, "");
        sendKeyExpectState(VirtualKey.ENTER, "X2/DVR", 6000, true);
        waitForStateValueChange(TP_PLACEHOLDER_INITCOMPLETE, "", 8000, true);

        if (subMenuItem != null) {
            findItemInSubMenu("menu",subMenuItem);
        }
        return true;
    }

    private void goToFullScreenVideoAndEvaluateExceptions(){
        clearStackExpectVideoState();
        evaluateForFatalExceptions();
        sleep(500);
    }

    /**
     * Send DOWN key and wait for rowControlChoice change.
     * Doesn't force failure.  Only fails if exceptions are found.
     * //TODO: may want to generalize this to include specific rowControlChoice name, such as
     * [viewName.]rowControlChoice.
     *
     * @return true if the rowControlChoiceChanged
     */
    public boolean moveDownAndWaitForRowChange() {
        setGuideStateValue(TP_ROWCONTROL_CHOICE, "");
        sendKey(VirtualKey.DOWN);
        boolean changed = waitForRowChange(5000, "", false);
        evaluateForFatalExceptions();
        return changed;
    }

    /**
     * Send DOWN key and wait for container.placeholderFocused change.
     * Forces failure if no change occurs.  Fails if exceptions are found.
     * [viewName.]rowControlChoice.
     *
     * @return true if the rowControlChoiceChanged
     */
    public boolean moveDownAndWaitForPlaceholderFocus() {
        String key = TP_CONTAINER_PLACEHOLDER_FOCUSED;
        String expectedValue = "true";
        setGuideStateValue(key, "");
        sendKey(VirtualKey.DOWN);
        boolean changed = waitForStateValueChange(key, "", 5000, true);
        evaluateForFatalExceptions();
        String actualValue = getGuideStateValue(key);
        if( !expectedValue.equals(actualValue) ) {
            failTest("Expected container placeholder to get focus");
            return false;
        } else {
            return true;
        }
    }

    public boolean moveUpAndWaitForPlaceholderFocus(FailureMode failMode) {
        return moveUpAndWaitForKeyValue(TP_CONTAINER_PLACEHOLDER_FOCUSED, "true", X1ToolkitActions.FailureMode.FAIL_ON_TIMEOUT);
    }

    public boolean moveUpAndWaitForViewControlPresentation(FailureMode failMode) {
        return moveUpAndWaitForKeyValue(TP_CONTAINER_VCP_FOCUSED, "true", failMode);
    }

    public boolean moveUpAndWaitForKeyValue(String key, String expectedValue, X1ToolkitActions.FailureMode failMode) {
        boolean success = false;
        setGuideStateValue(key, "");
        sendKey(VirtualKey.UP);
        waitForStateValueChange(key, "", 5000, false);
        evaluateForFatalExceptions();
        String actualValue = getGuideStateValue(key);
        if( !expectedValue.equals(actualValue) ) {
            if( failMode == X1ToolkitActions.FailureMode.FAIL_ON_TIMEOUT ) {
                failTest("Expected value " + expectedValue + " for key[" + key + "]");
            }
        } else {
            success = true;
        }

        return success;
    }

    public boolean moveUpAndWaitForActionBarChoice(String actionName) {
        setGuideStateValue(TP_ACTIONBAR_CHOICE, "");
        sendKey(VirtualKey.UP);
        boolean changed = waitForStateValueChange(TP_ACTIONBAR_CHOICE, "", 5000, false);
        String actionBarData = getGuideStateValue(TP_ACTIONBAR_CHOICE);
        String name = getActionBarChoiceName(actionBarData);
        return( name.equals(actionName) );
    }

    public void moveDownOneRow(String currentRowName) {
        sendKey(VirtualKey.DOWN);
        waitForRowChange(5000, currentRowName, false);
    }

    public boolean isShingleShown() {
        String status = getGuideStateValue(TP_SHINGLE_SHOWN);
        return !(status == null || status.isEmpty() || status.equals("null"));
    }

    public void failIfShingleShown() {
        if( isShingleShown() ) {
            failTest("Shingle is shown");
        }
    }

    public boolean gotoMainMenu() {
        clearStackExpectVideoState();
        sendKeyExpectState(VirtualKey.MENU, "X2/MAIN_MENU/PRESENTATION.XML", 4000, true);
        return true;
    }

    public boolean selectMainMenuAction(String mainMenuActionName) {
        boolean found = rotateThroughActionBarUtil("mainmenu", mainMenuActionName, 20000, true);
        return found;
    }

    public boolean launchAppByXAPI(String deeplinkUrl) {
        setShellStateValue("appLaunchError", "");
        setGuideStateValue("appResourceLoadFail", "");
        setGuideStateValue("\"terminatedApp\"", "");
        receiver.sendDeeplinkUrl(deeplinkUrl);
        sleep(10000);
        String status = getShellStateValue("appLaunchError");
        if( !StringUtils.isEmpty(status) ) {
            failTest("Failed to launch app: " + status);
            return false;
        }
        status = getGuideStateValue("appResourceLoadFail");
        if( !StringUtils.isEmpty(status) ) {
            failTest("Failed to launch app: " + status);
            return false;
        }
        pressExit();
        waitForStateValueChange("terminatedApp", "", 20000, true);
        return true;

    }

    public boolean setScaledVideoMode(GuideConstants.ScaledVideoState desiredState) {
        gotoMainMenu();
        gotoAppDashboardScreen();
        String initialValue = getGuideStateValue(TP_SCALED_VIDEO_STATE);
        sendKey(VirtualKey.COLOR_KEY_1);

        waitForStateValueChange(TP_SCALED_VIDEO_STATE, initialValue, 5000, true);
        String value = getGuideStateValue(TP_SCALED_VIDEO_STATE);
        if( !desiredState.name().equals(value) ) {

            sendKey(VirtualKey.COLOR_KEY_1);
            waitForStateValue(TP_SCALED_VIDEO_STATE, "SCALED", 5000, true);
        }
        pressExit();
        return true;
    }

    public boolean gotoAppDashboardScreen() {
        boolean found = selectMainMenuAction("APPS");
        if( !found ) {
            failTest("Could not find APPS menu action");
            return false;
        }
        setGuideStateValue(TP_SHINGLE_SHOWN, "");
        setGuideStateValue(TP_ROWCONTROL_CHOICE, "");
        sendKeyExpectState(VirtualKey.ENTER, "X2/APPSDASHBOARD.XML", 8000, true);
        return true;
    }

    public ModuleModel getModuleModelFromGuideStateValue(String guideStateSubKey) {
        return getModuleModelFromJSON(getGuideStateValue(guideStateSubKey));
    }

    public boolean clearLogs(){
        String receiverId = dataClient.hashGet(receiver.getDeviceId() + SUBKEY_GUIDE_RECEIVERID, KEY_RECEIVER_ID);
        return loggingClient.clearForDevice(receiverId);
    }

    public String getExceptionsInLog() {
        List<String> exceptionList = getAllExceptionsInLog();
        if( exceptionList != null && !exceptionList.isEmpty() ) {
            return getExceptionsAsString(exceptionList);
        } else {
            return "";
        }
    }

    public void evaluateForExceptions() {
    }

    public void evaluateForFatalExceptions() {
    }

    public void evaluateForCriticalExceptions() {
        List<String> exceptionList = getAllExceptionsInLog();
        clearLogs();
        if( exceptionList != null && !exceptionList.isEmpty() ) {
            failTest("Exceptions exist: " + getExceptionsAsString(exceptionList));
        }
    }

    private String getExceptionsAsString(List<String> exceptionList) {
        StringBuilder sb = new StringBuilder();
        for(String message : exceptionList) {
            sb.append(message);
            sb.append("\r\n");
        }

        return sb.toString();
    }

    //get the receiverId(P010****) instead of the deviceId(4756****)
    public boolean getLogs(){
        String receiverId = dataClient.hashGet(receiver.getDeviceId()+SUBKEY_GUIDE_RECEIVERID, KEY_RECEIVER_ID);
        List<String> logs = loggingClient.getAllLogs(receiverId);
        for(String s : logs){
            log.info(s);//captured logs are sent to logs file
        }
        return !logs.isEmpty();
    }

    public boolean getLogsForTime(){
        String receiverId = dataClient.hashGet(receiver.getDeviceId() + SUBKEY_GUIDE_RECEIVERID, KEY_RECEIVER_ID);
        List<String> logs = loggingClient.getLogsForTimeAndMarker(receiverId, System.currentTimeMillis() - (5 * 60 * 1000), System.currentTimeMillis());
        for(String s : logs){
            log.info(s);//captured logs are sent to logs file
        }
        return !logs.isEmpty();
    }

    public void getNullPointerExceptionLog(){
        String receiverId = dataClient.hashGet(receiver.getDeviceId()+SUBKEY_GUIDE_RECEIVERID, KEY_RECEIVER_ID);
        List<String> logs = loggingClient.getExceptionFromLogs(receiverId);
        List<String> nullLogs = new ArrayList<String>();
        for(String s : logs){
            if(s.contains("NullPointerException")){
                nullLogs.add(s);
            }
        }
        if(!nullLogs.isEmpty()){
            log.error("Found Null Pointer Exceptions");
        }
    }

    public boolean exceptionsExistInLog(){
        int exceptionCount = 0;

        String receiverId = dataClient.hashGet(receiver.getDeviceId() + SUBKEY_GUIDE_RECEIVERID, KEY_RECEIVER_ID);
        List<String> logs = loggingClient.getExceptionFromLogs(receiverId);
        for(String s : logs){
            if(s.contains("Exception")){
                ++exceptionCount;
            }
        }

        return (exceptionCount > 0);
    }

    public List<String> getAllExceptionsInLog(){
        String receiverId = dataClient.hashGet(receiver.getDeviceId() + SUBKEY_GUIDE_RECEIVERID, KEY_RECEIVER_ID);
        List<String> logs = loggingClient.getExceptionFromLogs(receiverId);
        List<String> exceptionLogs = new ArrayList<String>();
        for(String s : logs){
            if(s.contains("Exception") && !isInIgnoreList(s)){
                exceptionLogs.add(s);
            }
        }
        return exceptionLogs;
    }

    private boolean isInIgnoreList(String message) {
        if(message.contains("Failed to handle HTTP Response: Remotely Closed")) {
            return true;
        }
        else if(message.contains("java.io.IOException: Remotely Closed")) {
            return true;
        }
        else if(message.contains("java.net.ConnectException")) {
            return true;
        }
        /*
        else if( message.contains("showAndBringToTop could not find component") ) {
            return true;
        }
        else if( message.contains("RegisterForNotificationThatConditionOccurredCommand") ) {
            return true;
        }
        else if( message.contains("IllegalArgumentException") && message.contains("player") ) {
            return true;
        }
        */
        return false;
    }

    public void clearStats(boolean clearStartupValue) {
        clearStats(receiver.getDeviceId(), "guide", clearStartupValue);
    }

    private void clearGuideStats(String deviceId, boolean clearStartupValue) {
        if( dataClient == null ) {
            return;
        }
        dataClient.clearKeys(deviceId + SUBKEY_GUIDE_TEST_MODE_ENABLED, null);//takes two first is dbKey and second is hashKey, dbKey cannot be null
        if( clearStartupValue ) {
            dataClient.clearKeys(deviceId+SUBKEY_GUIDE_STARTUP_STATUS, null);
        }
        dataClient.clearKeys(deviceId+SUBKEY_GUIDE_STATE_GROUP, null);
        dataClient.clearKeys(deviceId+SUBKEY_UPDATELOG, null);
    }

    abstract class DoUntilDoneOrTimeout {
        public abstract boolean task();
        public abstract void onTimeout();
        private String taskName;
        private long maxWaitMillis;
        private FailureMode failOnTimeout;

        public DoUntilDoneOrTimeout(String taskName, long maxWaitMillis, FailureMode failOnTimeout) {
            this.taskName = taskName;
            this.maxWaitMillis = maxWaitMillis;
            this.failOnTimeout = failOnTimeout;
        }

        public boolean start() {
            return start(taskName, maxWaitMillis, failOnTimeout);
        }

        public boolean start(String taskName, long maxWaitMillis, FailureMode failOnTimeout) {
            boolean completed;
            long delayTimeMillis = 200;
            long endTime = System.currentTimeMillis() + maxWaitMillis;

            do {
                completed = task();
                sleep(delayTimeMillis);
            } while( !completed &&  System.currentTimeMillis() < endTime );

            evaluateForFatalExceptions();

            if( !completed ) onTimeout();

            if( !completed && (failOnTimeout == FailureMode.FAIL_ON_TIMEOUT) ) {
                failTest("timeout waiting for task to complete: task=" + taskName);
            }

            return completed;
        }
    }

    public String getExceptionLogs() {
        StringBuilder sbuf = new StringBuilder();
        String exceptionMsg = getExceptionsInLog();
        sbuf.append("Exceptions seen during test run:\n");
        if( !StringUtils.isEmpty(exceptionMsg) ) {
            sbuf.append("\n\nErrorLogs seen:\n");
            sbuf.append(exceptionMsg);
            clearLogs();
            return sbuf.toString();
        }

        return  "";
    }

    public void failTest(String message)  {
        StringBuilder sbuf = new StringBuilder();
        String exceptionMsg = getExceptionsInLog();
        sbuf.append(message);
        if( !StringUtils.isEmpty(exceptionMsg) ) {
            sbuf.append("\n\nErrorLogs seen:\n");
            sbuf.append(exceptionMsg);
            clearLogs();
        }
        super.failTest(sbuf.toString());
    }

    private void waitForInput(X1GuideBaseTestActions action, String desiredInput) {
        action.waitForStateValue("inputModuleText", desiredInput, 5000, true);
    }

    public boolean tuneToTheSpecifiedChannelAndCheckForX2NotificationIsShown(String channelNumber){
        char[] a = channelNumber.toCharArray();
        StringBuilder input = new StringBuilder();
        for(char i : a){
            StringBuilder sb = new StringBuilder();
            sb.append("NUMBER_");
            sb.append(i);
            input.append(i);
            sendKey(VirtualKey.valueOf(sb.toString()), 500);
            waitForInput(this, input.toString());
        }
        // Set the value before before ENTER key is pressed
        setAppStateValue("shell",TP_X2_NOTIFICATION_SHOWN,"");
        sendKeyExpectState(VirtualKey.ENTER,"FULL_SCREEN",8000,true);
        return waitForAppStateValueChange("shell", TP_X2_NOTIFICATION_SHOWN, "", 10000, false);
    }

}
