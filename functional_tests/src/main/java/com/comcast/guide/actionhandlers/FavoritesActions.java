/*
 *  ===========================================================================
 *  Copyright (c) 2014 Comcast Corp. All rights reserved.
 *  ===========================================================================
 *
 *  Author: ${kguntu001c}
 *  Created: ${9/19/2014}  ${2:09 PM}
 * /
 */

package com.comcast.guide.actionhandlers;

import com.comcast.xre.events.VirtualKey;
import com.comcast.xre.testframework.IXREReceiver;
import com.comcast.xre.testframework.services.TestModuleDataClient;
import com.comcast.xre.testframework.services.TestModuleLoggingClient;
import com.comcast.xre.toolkit.presentation.model.DataModelEntry;
import org.apache.commons.lang.StringUtils;

import static com.comcast.xre.events.VirtualKey.ENTER;
import static com.comcast.xre.events.VirtualKey.EXIT;
import static org.testng.AssertJUnit.fail;

public class FavoritesActions extends SavedActions {

    public FavoritesActions(IXREReceiver receiver, TestModuleDataClient dataClient, TestModuleLoggingClient logClient) {
        super(receiver, dataClient, logClient);
    }

    public void addAChannelFavorite() {
        clearStackExpectVideoState();
        enterFullGuideScreenByGuideKey();
        // wait for page update
        sleep(1500);
        sendKey(VirtualKey.LEFT, 2000);
        // wait for detail info update
        sendKey(VirtualKey.ENTER, 3000);
        boolean foundRemoveFavorite = rotateThroughActionBarUtil("", "Favorite", 5000, false);
        sendKey(VirtualKey.ENTER, 1000);
    }


    public void removeAllFavorites() {
        enterSpecifiedScreenAndSelectSubMenu("SAVED_DVR","menu","Favorites");
        sleep(1000);
        String currentRowName = getCurrentRowName();
        if( StringUtils.isEmpty(currentRowName) ) {
            if( isShingleShown() ) {
                //nothing to do here
                return;
            }
        }
        for( ;; ) {
            sendKey(VirtualKey.DOWN);
            boolean changed = waitForRowChange(5000, currentRowName, false);
            if (isShingleShown()) {
                //nothing to do here
                break;
            }
            currentRowName = getCurrentRowName();
            setCurrentActionBarChoice("", "");
            sendKey(VirtualKey.ENTER, 2000);
            waitForActionBarChoiceChange("", 5000, false);
            boolean foundRemoveFavorite = rotateThroughActionBarUtil("", "Remove Favorite", 5000, false);
            if( !foundRemoveFavorite ) {
                break;
            }
            pressEnter();
            sleep(2000);
            sendKeyExpectState(VirtualKey.PREV, "X2/DVR", 10000, false);
            if (isShingleShown()) {
                //nothing to do here
                break;
            }
            sendKey(VirtualKey.UP);
        }
    }

}
