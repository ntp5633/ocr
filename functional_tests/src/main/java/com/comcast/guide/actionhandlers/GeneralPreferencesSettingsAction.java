/* ===========================================================================
 * Copyright (c) 2014 Comcast Corp. All rights reserved.
 * ===========================================================================
 *
 * Author: kguntu001c
 * Created: 10/12/2014  10:55 AM
 */

package com.comcast.guide.actionhandlers;

import com.comcast.xre.events.VirtualKey;
import static com.comcast.guide.GuideTestPoints.*;
import static com.comcast.xre.ToolkitTestPoints.*;
import com.comcast.xre.testframework.IXREReceiver;
import com.comcast.xre.testframework.services.TestModuleDataClient;
import com.comcast.xre.testframework.services.TestModuleLoggingClient;
import com.comcast.xre.toolkit.presentation.model.DataModelEntry;
import org.apache.commons.lang.StringUtils;
import static org.testng.AssertJUnit.assertTrue;
import static org.testng.AssertJUnit.fail;


public class GeneralPreferencesSettingsAction extends X1GuideBaseTestActions {

    public GeneralPreferencesSettingsAction(IXREReceiver receiver, TestModuleDataClient dataClient, TestModuleLoggingClient logClient) {
        super(receiver, dataClient, logClient);
    }

    public boolean enterSettingsScreenAndSelectPreferences(String subMenu) {
        findItemInMainMenu("SETTINGS");
        sendKeyExpectState(VirtualKey.ENTER, "X2/SETTINGS/SETTINGS.XML", 10000, true);
        sleep(500);
        sendKeyExpectState(VirtualKey.ENTER,"X2/SETTINGS/X2_PREFERENCES_LIST.XML", 10000, true);
        sleep(500);
        sendKeyExpectState(VirtualKey.ENTER,"X2/SETTINGS/X2_GENERAL_PREFERENCES.XML", 10000, true);
        sleep(500);

        //NOTE: For some reason the settings screen do not go through the choice selector on initial entry.
        //Move down one row and then back up to force a choice change.
        String currentRowName = this.getCurrentRowName();
        sendKey(VirtualKey.DOWN, 300);
        waitForRowChange(5000, currentRowName, false);
        currentRowName = this.getCurrentRowName();
        sendKey(VirtualKey.UP, 300);
        waitForRowChange(5000, currentRowName, false);

        if (subMenu != null) {
            findItemInRowControlPresentation(subMenu);
        }

        return true;
    }

    public boolean findItemInRowControlPresentation(String rowName) {
        return moveThroughRowControlPresentation(rowName, 18000, true);
    }

    public boolean turnOnSetting(String key){
        sendKey(VirtualKey.ENTER);
        waitForStateValueChange(key,"",8000,true);
        String status = getGuideStateValue(key);

        if(status.equalsIgnoreCase("Off")){
            sendKey(VirtualKey.ENTER);
            waitForStateValue(key,"On",8000,true);
            return true;
        }
        else
            return true;
    }

    public boolean turnOffSetting(String key){
        sendKey(VirtualKey.ENTER);
        waitForStateValueChange(key,"",8000,true);
        String status = getGuideStateValue(key);

        if(status.equalsIgnoreCase("On")){
            sendKey(VirtualKey.ENTER);
            waitForStateValue(key,"Off",8000,true);
            return true;
        }
        else
            return true;
    }

    public void selectDesiredDarknessLevelAndCheckItIsSetInPreferenceManager(String desiredDarknessLevel){
        boolean found =  rotateThroughOptionSelectorUtil(desiredDarknessLevel, 30000, true);
        if(found){
            sendKeyExpectState(VirtualKey.ENTER,"X2/SETTINGS/X2_GENERAL_PREFERENCES.XML",5000,true);
            //Overlay will be removed.   Wait for the currentOverlay value to clear
            waitForStateValue("currentOverlay","",5000,true);
            waitForStateValue("X2_BACKGROUND_TRANSPARENCY",desiredDarknessLevel, 10000, true);
        }
    }

    public String getCurrentOptionSelectorValue(){
        String currentChoice = getGuideStateValue("optionSelectorChoice");
        if( !StringUtils.isEmpty(currentChoice)) {
            DataModelEntry dataModelEntry = getDataModelEntryFromJSON(currentChoice);
            if (dataModelEntry != null) {
                Object value = dataModelEntry.getParam("value");
                if (value != null) {
                    currentChoice = value.toString();
                    return currentChoice;
                }
            }
        }
        return "";
    }

    public String getPreferenceManagerDarknessLevel(){
        sendKey(VirtualKey.ENTER);
        waitForStateValue("currentOverlay", "BACKGROUND_TRANSPARENCY_OVERLAY", 1000, true);

        String optionSelectorChoice = getCurrentOptionSelectorValue();

        sendKey(VirtualKey.RIGHT);
        waitForStateValueChange("optionSelectorChoice", optionSelectorChoice, 1000, true);
        String currentChoice = getCurrentOptionSelectorValue();

        sendKeyExpectState(VirtualKey.ENTER, "X2/SETTINGS/X2_GENERAL_PREFERENCES.XML", 1200, true);
        waitForStateValue("currentOverlay", "", 3000, true);
        // wait for preference value to be set
        waitForStateValue("X2_BACKGROUND_TRANSPARENCY", currentChoice, 10000, true);
        String preferenceManagerDarknessLevel = getGuideStateValue("X2_BACKGROUND_TRANSPARENCY");

        if (!StringUtils.equalsIgnoreCase(preferenceManagerDarknessLevel, currentChoice)) {
            fail("DarknessLevel is not properly set in PreferenceManager.  Expected " + preferenceManagerDarknessLevel + " but got <" + currentChoice + ">");
        }

        return preferenceManagerDarknessLevel;
    }

    public String getPreferenceManagerBackgroundImage(){
        sendKey(VirtualKey.ENTER);
        waitForStateValue("currentOverlay", "BACKGROUND_TRANSPARENCY_OVERLAY", 1000, true);
        sendKey(VirtualKey.DOWN);
        sleep(500);

        String optionSelectorChoice = getCurrentOptionSelectorValue();
        sendKey(VirtualKey.RIGHT);
        waitForStateValueChange("optionSelectorChoice", optionSelectorChoice, 1000, true);
        String currentChoice = getCurrentOptionSelectorValue();

        sendKey(VirtualKey.UP);
        sleep(500);
        sendKeyExpectState(VirtualKey.ENTER, "X2/SETTINGS/X2_GENERAL_PREFERENCES.XML", 1200, true);
        waitForStateValue("currentOverlay", "", 5000, true);
        String preferenceManagerBackgroundImage = getGuideStateValue("X2_BACKGROUND_IMAGE");

        if (!StringUtils.equalsIgnoreCase(preferenceManagerBackgroundImage, currentChoice)) {
            fail("BackgroundImage is not properly set in PreferenceManager");
        }

        return preferenceManagerBackgroundImage;
    }

    public boolean selectDifferentDarknessLevelsAndCheckPreferenceManagerIsSetWithNewPreference() {
        final int delayTimeMillis = 300;
        String currentDarknessLevel;

        final String initialValue = getPreferenceManagerDarknessLevel();

        do {
            sleep(delayTimeMillis);
            currentDarknessLevel = getPreferenceManagerDarknessLevel();
        } while ( !StringUtils.equalsIgnoreCase(currentDarknessLevel,initialValue));

        return true;
    }

    public boolean selectDifferentBackgroundImagesAndCheckPreferenceManagerIsSetWithNewPreference() {
        final int delayTimeMillis = 300;
        String currentDarknessLevel;

        // Pressing the ENTER key calls up an overlay
        sendKey(VirtualKey.ENTER,1000);
        // Wait for specific overlay name
        waitForStateValue("currentOverlay", "BACKGROUND_TRANSPARENCY_OVERLAY", 1000, true);
        moveDownAndWaitForRowChange();

        final String initialValue = getPreferenceManagerBackgroundImage();

        do {
            sleep(delayTimeMillis);
            currentDarknessLevel = getPreferenceManagerBackgroundImage();
        } while ( !StringUtils.equalsIgnoreCase(currentDarknessLevel,initialValue));

        return true;
    }

    public boolean rotateThroughOptionSelectorUtil(String desiredChoice, long maxWaitMillis, boolean failOnTimeout) {
        final long delayTimeMillis = 300;
        long endTime = System.currentTimeMillis() + maxWaitMillis;

        String currentChoice;
        String currentOptionValue;
        do {
            sleep(delayTimeMillis);
            // Get the current value before making a change
            currentOptionValue = getGuideStateValue("optionSelectorChoice");
            // Change the value
            sendKey(VirtualKey.RIGHT);
            // Wait for the change
            waitForStateValueChange("optionSelectorChoice", currentOptionValue, 5000, true);
            currentChoice = getCurrentOptionSelectorValue();
            if (StringUtils.equals(desiredChoice, currentChoice)) {
                return true;
            }
        } while( System.currentTimeMillis() < endTime );

        evaluateForFatalExceptions();

        if( failOnTimeout ) {
            failTest("timeout waiting for darknessLevel to change to " + desiredChoice + ".  Last seen state=" + currentChoice);
        }

        return false;
    }

    public void testToSeeLastWatched(){
        setGuideStateValue(TP_LASTNINE_TILECONTAINER_CHOICE, "");
        sendKeyExpectState(VirtualKey.PREV, guideConfig.getLastNineStateID(), 1500, true);
        boolean changed = waitForStateValueChange(TP_LASTNINE_TILECONTAINER_CHOICE,"",10000, false);
        if(getGuideStateValue(TP_RECENTLY_WATCHED_SETTING).equalsIgnoreCase("On")){
            //Last Nine Program Tiles will be displayed
            assertTrue(changed);
        }else{
            //Shingle will be shows when the setting is off
            if(!isShingleShown()){
                failTest("Recently Watched Programs are displayed even if Recently Watched Content setting is turned off");
            }
        }
    }

    //ToDo not yet done
    public void testToSeeRecentApps() {
        enterSpecifiedScreenAndSelectSubMenu("SAVED_DVR","menu","Recent");
    }

}
