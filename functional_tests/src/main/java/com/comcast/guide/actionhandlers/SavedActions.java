/*
 *  ===========================================================================
 *  Copyright (c) 2014 Comcast Corp. All rights reserved.
 *  ===========================================================================
 *
 *  Author: ${kguntu001c}
 *  Created: ${9/19/2014}  ${2:09 PM}
 * /
 */

package com.comcast.guide.actionhandlers;

import com.comcast.xre.events.VirtualKey;
import com.comcast.xre.testframework.IXREReceiver;
import com.comcast.xre.testframework.services.TestModuleDataClient;
import com.comcast.xre.testframework.services.TestModuleLoggingClient;
import com.comcast.xre.toolkit.presentation.model.DataModelEntry;
import org.apache.commons.lang.StringUtils;
import static com.comcast.xre.events.VirtualKey.*;
import static org.testng.AssertJUnit.fail;

public class SavedActions extends X1GuideBaseTestActions {

    public SavedActions(IXREReceiver receiver, TestModuleDataClient dataClient, TestModuleLoggingClient logClient) {
        super(receiver, dataClient, logClient);
    }

    public boolean enterSavedScreen() {
        return enterSpecifiedScreenAndSelectSubMenu("SAVED_DVR","menu",null);
    }

    public boolean traverseGalleryRows() {
        boolean done;
        do {
            String rowName = getCurrentRowName();
            if (rowName == null) {
                rowName = "";
            }
            done = moveThroughGalleryRow(rowName);
            if (!done) {
                sendKey(VirtualKey.DOWN);
                boolean rowChanged = waitForRowChange(1200, rowName, false);
                if (!rowChanged) {
                    // end of rows
                    return false;
                }
            }
        } while (!done);

        return done;
    }

    protected boolean moveThroughGalleryRow(String currentRowName) {
        String currentTileName;
        boolean done = false;
        do {
            currentTileName = getCurrentTileName(currentRowName);
            setGuideStateValue("tileContainerChoice", "");
            if (!StringUtils.isEmpty(currentRowName)) {
                setGuideStateValue(encodeString(currentRowName + ".tileContainerChoice"), "");
            }
            sendKey(VirtualKey.RIGHT);
            boolean tileChanged = waitForTileChange(3000, currentRowName, currentTileName, false);
            if (!tileChanged) {
                // end of row
                return false;
            }
            sleep(10);
        } while (!done);

        return false;
    }

    private String lastTileName = "";

    protected String getCurrentTileName(String currentRowName) {
        if (currentRowName == null) {
            currentRowName = "";
        }

        StringBuilder key = new StringBuilder();
        if (!currentRowName.isEmpty()) {
            key.append(currentRowName);
            key.append(".");
        }
        key.append("tileContainerChoice");
        String tileContainerChoice = getGuideStateValue(encodeString(key.toString()));
        if (tileContainerChoice == null) {
            tileContainerChoice = getGuideStateValue("tileContainerChoice");
        }
        DataModelEntry dataModelEntry;
        dataModelEntry = getDataModelEntryFromJSON(tileContainerChoice);
        String title = dataModelEntry.getTitle();
        StringBuilder currentTileName = new StringBuilder();
        currentTileName.append(title);
        Long startTime = dataModelEntry.getLongParam("startTime", 0);
        if (startTime != null) {
            currentTileName.append(":").append(startTime);
        }

        return currentTileName.toString();
    }

    protected String getCurrentTileEntityIdOrSeriesIdOrProgramIdOrPersonalityIdDependingOnTheEntityType(String currentRowName) {
        Object id = "";
        if (currentRowName == null) {
            currentRowName = "";
        }
        StringBuilder key = new StringBuilder();
        if (!currentRowName.isEmpty()) {
            key.append(currentRowName);
            key.append(".");
        }
        key.append("tileContainerChoice");
        String tileContainerChoice = getGuideStateValue(encodeString(key.toString()));
        if (tileContainerChoice == null) {
            tileContainerChoice = getGuideStateValue("tileContainerChoice");
        }
        DataModelEntry dataModelEntry;
        dataModelEntry = getDataModelEntryFromJSON(tileContainerChoice);
        Object entityType = dataModelEntry.getParam("entityType");
        if(entityType != null){
            String entity = entityType.toString();
            if(entity.equalsIgnoreCase("TV_SERIES")){
                id = dataModelEntry.getParam("seriesId");
            }
            else if(entity.equalsIgnoreCase("TV_PROGRAM")){
                id = dataModelEntry.getParam("programId");
            }
            else if(entity.equalsIgnoreCase("PERSONALITY")){
                id = dataModelEntry.getParam("personalityId");
            }
            else {
                id = dataModelEntry.getParam("entityId");
            }
        }
        if(id == null){
            return "";
        }
        return id.toString();
    }


    private boolean waitForTileChange(long maxWaitMillis, String currentRowName, String currentAppName, boolean failOnTimeout) {
        long maxTileWaitTime = 0;
        long delayTimeMills = 200;
        long startingCounter;
        long timeoutCounter = maxWaitMillis / delayTimeMills;
        if (timeoutCounter == 0) {
            timeoutCounter = 1;
        }
        startingCounter = timeoutCounter;

        do {
            sleep(delayTimeMills);
            String tileName = getCurrentTileName(currentRowName);
            if (tileName != null && !tileName.isEmpty() && !StringUtils.equals(tileName, currentAppName)) {
                long diffCount = startingCounter - timeoutCounter + 1;
                if ((diffCount * delayTimeMills) > maxTileWaitTime) {
                    maxTileWaitTime = diffCount * delayTimeMills;
                }
                return true;
            }
        } while (--timeoutCounter != 0);

        if (failOnTimeout) {
            fail("timeout waiting tile to change");
        }

        evaluateForExceptions();

        return false;
    }

    public void testForYouSectionInSavedMenu(String id) {
        scrollToNthRow(id, 1);
        whichStageToRun(getCurrentRowName());

        scrollToNthRow(id, 2);
        whichStageToRun(getCurrentRowName());

        scrollToNthRow(id, 3);
        whichStageToRun(getCurrentRowName());

//        scrollToNthRow(id, 3);
//        whichStageToRun(getCurrentRowName());

        goBackToFullScreenVideoAndEvaluateExceptions();
    }

    public void testRecordingsSectionInSavedMenu(String id) {
        scrollToNthRow(id, 1);
        testRecordings();

        scrollToNthRow(id, 2);
        testRecordings();

        scrollToNthRow(id, 3);
        testRecordings();

        goBackToFullScreenVideoAndEvaluateExceptions();
    }

    public void testScheduleRecordingsInSavedMenu(String id) {

        scrollToNthRow(id, 1);
        testScheduledRecording();

        scrollToNthRow(id, 2);
        testScheduledRecording();

        scrollToNthRow(id, 3);
        testScheduledRecording();

        goBackToFullScreenVideoAndEvaluateExceptions();
    }


    private void testScheduledRecording() {
        if (waitForActionBarChoice()) {
            testWhenActionBarChoiceIsChanged();
        }
    }

    private void scrollToNthRow(String id, int rowNum) {
        if (findItemInMainMenu("SAVED_DVR")) {
      /*      sendKeyExpectState(VirtualKey.ENTER, "X2/DVR", 18000, true);
            sleep(1000);*/
            sendKey(VirtualKey.ENTER, 500);
            waitForStateValueChange("X2/DVR", "", 8000, true);

            if (findItemInSubMenu("menu",id)) {
                for (int i = 1; i <= rowNum; i++) {
                    String currentRowName = getCurrentRowName();
                    sendKey(VirtualKey.DOWN);
                    waitForRowChange(5000, currentRowName, false);
                }
            }
        }
    }

    public void testWhenActionBarChoiceIsChanged() {
        // Action bar choice should be something like watch or resume
        // Press ENTER to watch or resume
        String guideState = getGuideStateValue("actionBarChoice");
        String[] elements = guideState.split(",");
        String actionBarChoice = elements[1];
        if (actionBarChoice.equalsIgnoreCase("Watch") || actionBarChoice.equalsIgnoreCase("Resume")) {
            sendKeyExpectState(VirtualKey.ENTER, "FULL_SCREEN", 10000, true);
            sleep(2000);
        } else if (actionBarChoice.equalsIgnoreCase("Episodes")) {
            testTop100TVShowsSavedRow();
        }
    }

    public void testRecordings() {
        setGuideStateValue("actionBarChoice", "");
        sendKey(VirtualKey.ENTER, 1000);
        boolean isActionBarStateChanged = waitForStateValueChange("actionBarChoice", "", 8000, false);
        sleep(1000);

        if (isActionBarStateChanged) {
            testWhenActionBarChoiceIsChanged();
        } else {
            // row should open - wait for an action bar choice
            setGuideStateValue("actionBarChoice", "");
            sendKey(VirtualKey.ENTER, 1000);
            boolean isStateChanged = waitForStateValueChange("actionBarChoice", "", 8000, false);

            if (isStateChanged) {
                testWhenActionBarChoiceIsChanged();
            }
        }
        sendKey(VirtualKey.EXIT, 1000);
        goBackToFullScreenVideoAndEvaluateExceptions();
    }

    public void whichStageToRun(String rowName) {
        if (StringUtils.isBlank(rowName)) {
            testTop100TVShowsSavedRow();
        } else if (rowName.equalsIgnoreCase("just_saved") || rowName.equalsIgnoreCase("recently_watched")) {
            testJustSavedRecordingOrRecentlyWatchedRecording();
        } else if (rowName.equalsIgnoreCase("RecentApps")) {
            testRecentAppsRow();
        }
        goBackToFullScreenVideoAndEvaluateExceptions();
    }

    private void testJustSavedRecordingOrRecentlyWatchedRecording() {
        if (waitForActionBarChoice()) {
            testWhenActionBarChoiceIsChanged();
        }
    }

    private boolean waitForActionBarChoice() {
        // row should open - wait for an action bar choice
        setGuideStateValue("actionBarChoice", "");
        sendKey(VirtualKey.ENTER, 1000);
        return waitForStateValueChange("actionBarChoice", "", 8000, true);
    }

    private void testTop100TVShowsSavedRow() {
        sendKey(VirtualKey.ENTER, 4000);
        waitForStateValueChange("currentState", "", 180000, true);
        sleep(3000);
        // row should open - wait for an action bar choice
        setGuideStateValue("actionBarChoice", "");
        sendKey(VirtualKey.ENTER, 1000);
        boolean isStateChanged = waitForStateValueChange("actionBarChoice", "", 8000, true);
        // Action bar choice should be something like watch or resume
        // Press ENTER to watch or resume to select HD or SD
        if (isStateChanged) {
            String guideState = getGuideStateValue("actionBarChoice");
            String[] elements = guideState.split(",");
            if (elements[1].equalsIgnoreCase("Watch") || elements[1].equalsIgnoreCase("Resume")) {
                sendKey(VirtualKey.ENTER, 1000);
                waitForStateValueChange("currentState", "", 8000, true);
                // Press ENTER to select HD or SD
                sleep(1000);
                sendKeyExpectState(VirtualKey.ENTER, "FULL_SCREEN", 40000, true);
                sleep(2000);
            }

            sendKey(VirtualKey.EXIT, 4000);
        }
    }

    private void testRecentAppsRow() {
        sendKey(ENTER, 500);
        sleep(12000);
        sendKey(EXIT, 2000);
    }

    public String recordFromChannelGridAndReturnItsListingID() {
        String listingID = null;
        //ToDo may be wait for X2DetailPanel.java instead of sleep
        sleep(1200);
        sendKeyExpectState(VirtualKey.ENTER, "FULL_SCREEN", 15000, true);
        sendKey(VirtualKey.RECORD, 500);
        listingID = getGuideStateValue("Guide.listingIdOfCurrentChoice");
        if (getGuideStateValue("currentOverlay").equalsIgnoreCase("RecordOptionsOverlay")) {
            sendKey(VirtualKey.ENTER, 500);
            boolean isStateChanged = waitForStateValueChange("currentOverlay", "RecordOptionsOverlay", 8000, false);
            if (!isStateChanged) {
                sendKey(VirtualKey.ENTER, 500);
            }
            sendKeyExpectState(VirtualKey.ENTER, "FULL_SCREEN", 15000, true);
        }
        if (listingID == null) {
            return "";
        }
        return listingID;
    }

    public boolean scrollThroughScheduledRecordingsToFindThePassedListingID(String listingIDFromGuide) {
        boolean isRowChanged;
        boolean found = Boolean.FALSE;
        do {
            String rowName = getCurrentRowName();
            if (rowName == null) {
                rowName = "";
            }
            isRowChanged = moveDownAndWaitForRowChange();
            if (!isRowChanged) {
                // end of rows
                if (!found) {
                    fail("Recording from Guide is not found in the Scheduled Recording section");
                }
                return false;
            }
            sendKey(VirtualKey.ENTER, 500);
            String listingIDFromScheduledRecordings = getCurrentSelectionListingId();
            if (listingIDFromScheduledRecordings.equals(listingIDFromGuide)) {
                found = Boolean.TRUE;
                return found;
            }
        } while (!found);

        return found;
    }

    public String selectAnEntityAsFavoriteAndReturnItsEntityId() {
        String entityId;
        boolean found = false;
        //ToDo may be wait for X2DetailPanel.java instead of sleep
        sleep(1200);
        sendKeyExpectState(VirtualKey.ENTER, "FULL_SCREEN", 15000, true);
        sendKeyExpectState(VirtualKey.INFO, "X2/MINI_INFO", 15000, true);
        entityId = getCurrentSelectionEntityID();
        String entityTypeOfSelection = getCurrentSelectionEntityType();
        String actionBarData = getGuideStateValue("actionBarChoice");
        String actionBarName = getActionBarChoiceName(actionBarData);

        if(entityTypeOfSelection.equalsIgnoreCase("TV_SERIES")) {
            if (actionBarName.equalsIgnoreCase("Episodes")) {
                found = keepMovingRightOnTheActionBarUntilPassedActionBarNameIsFound("Series Info", 5000, true);
            } else {
                found = keepMovingRightOnTheActionBarUntilPassedActionBarNameIsFound("More Info", 5000, true);
            }
        }
        else if(entityTypeOfSelection.equalsIgnoreCase("TV_PROGRAM")) {
            found = keepMovingRightOnTheActionBarUntilPassedActionBarNameIsFound("More Info", 5000, true);
        }

        else if(entityTypeOfSelection.equalsIgnoreCase("MOVIE")) {
            found = keepMovingRightOnTheActionBarUntilPassedActionBarNameIsFound("Movie Info", 5000, true);
        }
        if(found) {
            sendKeyExpectState(VirtualKey.ENTER, "X2/ENTITY_INFO", 10000, false);
            boolean findRemoveFavorite = keepMovingRightOnTheActionBarUntilPassedActionBarNameIsFound("Remove Favorite",5000,false);
            if(findRemoveFavorite){
                return entityId;
            }
            boolean findFavorite = keepMovingRightOnTheActionBarUntilPassedActionBarNameIsFound("Favorite", 5000, true);
            if (findFavorite) {
                sendKey(VirtualKey.ENTER);
            }
        }

        if(entityId == null){
            return "";
        }

        return entityId;

    }

    public boolean scrollThroughFavoritesToFindThePassedEntityId(String entityIdFromGuide){
        boolean isRowChanged;
        boolean found = false;
        do {
            String rowName = getCurrentRowName();
            if (rowName == null) {
                rowName = "";
            }
            isRowChanged = moveDownAndWaitForRowChange();
            if (!isRowChanged) {
                // end of rows
                if (!found) {
                    fail("Favorite Entity from Guide is not found in the Favourites section");
                }
                return false;
            }
            found = moveThroughGalleryRowUntilPassedEntityIdIsFound(getCurrentRowName(),entityIdFromGuide);
            if (found) {
                return true;
            }
        } while (!found);

        return found;
    }

    protected boolean moveThroughGalleryRowUntilPassedEntityIdIsFound(String currentRowName, String entityIdFromGuide) {
        String currentTileName;
        String currentTileEntityId;
        boolean done = false;
        do {
            currentTileName = getCurrentTileName(currentRowName);
            currentTileEntityId = getCurrentTileEntityIdOrSeriesIdOrProgramIdOrPersonalityIdDependingOnTheEntityType(currentRowName);
            if(currentTileEntityId.equalsIgnoreCase(entityIdFromGuide)){
                done = true;
                return done;
            }
            setGuideStateValue("tileContainerChoice", "");
            if (!StringUtils.isEmpty(currentRowName)) {
                setGuideStateValue(encodeString(currentRowName + ".tileContainerChoice"), "");
            }
            sendKey(VirtualKey.RIGHT);
            boolean tileChanged = waitForTileChange(3000, currentRowName, currentTileName, false);
            if (!tileChanged) {
                // end of row
                return false;
            }
            sleep(100);
        } while (!done);

        return false;
    }

    private void waitForInput(X1GuideBaseTestActions action, String desiredInput) {
        action.waitForStateValue("inputModuleText", desiredInput, 5000, true);
    }


    public String searchForPersonToFavouriteItAndReturnEntityId() {
        String entityId;

        clearStackExpectVideoState();
        sleep(2000);
        // Try 2748 (Brittany Murphy)
        sendKey(VirtualKey.NUMBER_2, 500);
        waitForInput(this, "2");
        sendKey(VirtualKey.NUMBER_7, 500);
        waitForInput(this, "27");
        sendKey(VirtualKey.NUMBER_4, 500);
        waitForInput(this, "274");
        sendKey(VirtualKey.NUMBER_8, 500);
        waitForInput(this, "2748");

        sendKeyExpectState(VirtualKey.ENTER, "X2/ENTITY_INFO", 5000, true);
        entityId = getCurrentSelectionEntityID();
        if (entityId == null) {
            entityId = "";
        }
        boolean findRemoveFavorite = keepMovingRightOnTheActionBarUntilPassedActionBarNameIsFound("Remove Favorite", 5000, false);
        if (findRemoveFavorite) {
            return entityId;
        }
        boolean findFavorite = keepMovingRightOnTheActionBarUntilPassedActionBarNameIsFound("Favorite", 5000, true);
        if (findFavorite) {
            sendKey(VirtualKey.ENTER);
        }

        return entityId;
    }


}






