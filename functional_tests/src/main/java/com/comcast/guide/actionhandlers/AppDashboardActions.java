/* ===========================================================================
 * Copyright (c) 2014 Comcast Corp. All rights reserved.
 * ===========================================================================
 *
 * Author: TCarro004
 * Created: 9/10/2014  12:45 PM
 */

package com.comcast.guide.actionhandlers;

import com.comcast.xre.events.VirtualKey;
import com.comcast.xre.testframework.IXREReceiver;
import com.comcast.xre.testframework.services.TestModuleDataClient;
import com.comcast.xre.testframework.services.TestModuleLoggingClient;
import com.comcast.xre.toolkit.presentation.model.DataModelEntry;
import com.comcast.xre.toolkit.presentation.model.ModuleModel;
import org.apache.commons.lang.StringUtils;

import static org.testng.AssertJUnit.fail;

public class AppDashboardActions extends X1GuideBaseTestActions {
    private long maxAppWaitTime;
    private String lastFailureReason = "";

    public AppDashboardActions(IXREReceiver receiver, TestModuleDataClient dataClient, TestModuleLoggingClient logClient) {
        super(receiver, dataClient, logClient);
    }

    private void clearFailureReason() {
        lastFailureReason = "";
    }

    private void addFailureReason(String reason) {
        lastFailureReason = reason;
    }

    public boolean launchAppByDurableAppId(String durableAppId, int maxRetries, FailureMode failureMode) {
        boolean launched;
        do {
            launched = launchAppByDurableAppId(durableAppId);
        } while(maxRetries > 0 && !launched);

        if( !launched && failureMode == FailureMode.FAIL_ON_TIMEOUT ) {
            failTest((lastFailureReason.isEmpty())? "Reached retry limit" : lastFailureReason);
        }
        return launched;
    }

    public boolean launchAppByDurableAppId(String durableAppId) {
        if( selectAppInDashboard(durableAppId)) {
            setShellStateValue("appLaunchError", "");
            setGuideStateValue("appResourceLoadFail", "");
            sendKey(VirtualKey.ENTER, 500);
            sleep(10000);
            String status = getShellStateValue("appLaunchError");
            if( !StringUtils.isEmpty(status) ) {
                addFailureReason("Failed to launch app: " + status);
                return false;
            }
            status = getGuideStateValue("appResourceLoadFail");
            if( !StringUtils.isEmpty(status) ) {
                addFailureReason("Failed to launch app: " + status);
                return false;
            }
        }
        evaluateForFatalExceptions();
        sleep(500);
        return true;
    }

    public boolean selectAppInDashboard(String durableAppId) {
        clearStackExpectVideoState();
        sendKey(VirtualKey.EXIT, 2000);
        sendKeyExpectState(VirtualKey.MENU, "X2/MAIN_MENU/PRESENTATION.XML", 4000, true);
        boolean found = rotateThroughActionBarUtil("mainmenu", "APPS", 20000, true);
        if( !found ) {
            addFailureReason("Could not find APPS menu action");
            return false;
        }
        setGuideStateValue("shingleShown", "");
        sendKey(VirtualKey.ENTER, 500);
        sleep(1000);
        String currentRowName = getCurrentRowName();
        if( StringUtils.isEmpty(currentRowName) ) {
            failIfShingleShown();
        }
        sendKey(VirtualKey.DOWN);
        boolean changed = waitForRowChange(5000, currentRowName, false);
        if( !changed ) {
            failIfShingleShown();
        }
        currentRowName = getCurrentRowName();
        sendKey(VirtualKey.UP);
        waitForRowChange(5000, currentRowName, false);
        return findAppInRows(durableAppId);
    }

    protected boolean findAppInRows(String requestedDurableAppId) {
        boolean found;
        do {
            String rowName = getCurrentRowName();
            found = findAppInRow(requestedDurableAppId, rowName);
            if( !found ) {
                sendKey(VirtualKey.DOWN);
                boolean rowChanged = waitForRowChange(1200, rowName, false);
                if( !rowChanged ) {
                    // end of rows
                    return false;
                }
            }
        } while( !found );

        return found;
    }

    protected boolean findAppInRow(String requestedDurableAppId, String currentRowName) {
        String currentAppName;
        boolean found;
        do {
            found = isApp(requestedDurableAppId, currentRowName);
            if( !found ) {
                currentAppName = getCurrentAppName(currentRowName);
                sendKey(VirtualKey.RIGHT);
                boolean appChanged = waitForAppChange(1200, currentRowName, currentAppName, false);
                if( !appChanged ) {
                    // end of row
                    return false;
                }
                sleep(10);
            }
        } while( !found );

        return found;
    }

    private String lastDurableAppId = "";

    protected String getCurrentAppName(String currentRowName) {
        String currentAppName = "unknown";

        String tileContainerChoice = getGuideStateValue(encodeString(currentRowName+".tileContainerChoice"));
        DataModelEntry dataModelEntry;
        dataModelEntry = getDataModelEntryFromJSON(tileContainerChoice);
        String title = dataModelEntry.getTitle();
        String durableAppId = dataModelEntry.getStringParam("durableAppId");
        if( durableAppId != null ) {
            currentAppName = durableAppId;
        } else if (durableAppId == null ) {
            currentAppName = title;
        }

        return currentAppName;
    }

    protected boolean isApp(String requestedDurableAppId, String currentRowName) {
        ModuleModel moduleModel = getModuleModelFromJSON(getGuideStateValue("rowControlChoice"));

        String tileContainerChoice = getGuideStateValue(encodeString(currentRowName+".tileContainerChoice"));
        DataModelEntry dataModelEntry;
        dataModelEntry = getDataModelEntryFromJSON(tileContainerChoice);
        if( dataModelEntry == null ) {
            fail("Failed to find model entry for tileContainerChoice: " + dataModelEntry);
        }
        String title = dataModelEntry.getTitle();
        String durableAppId = dataModelEntry.getStringParam("durableAppId");
        if( durableAppId != null ) {
            lastDurableAppId = durableAppId;
            if( StringUtils.equals(durableAppId, requestedDurableAppId) ) {
                return true;
            }
        } else if (durableAppId == null ) {
            lastDurableAppId = title;
            if( StringUtils.equals(title, requestedDurableAppId) ) {
                return true;
            }
        } else {
            lastDurableAppId = null;
        }

        return false;
    }

    private boolean waitForAppChange(long maxWaitMillis, String currentRowName, String currentAppName, boolean failOnTimeout) {
        long delayTimeMills = 200;
        long startingCounter;
        long timeoutCounter = maxWaitMillis/delayTimeMills;
        if( timeoutCounter == 0 ) {
            timeoutCounter = 1;
        }
        startingCounter = timeoutCounter;

        do {
            sleep(delayTimeMills);
            String appName = getCurrentAppName(currentRowName);
            if( !StringUtils.equals(appName,currentAppName) ) {
                long diffCount = startingCounter - timeoutCounter + 1;
                if( (diffCount*delayTimeMills) > maxAppWaitTime ) {
                    maxAppWaitTime = diffCount*delayTimeMills;
                }
                return true;
            }
        } while( --timeoutCounter != 0 );

        if( failOnTimeout ) {
            fail("timeout waiting app to change");
        }

        evaluateForFatalExceptions();

        return false;
    }

}
