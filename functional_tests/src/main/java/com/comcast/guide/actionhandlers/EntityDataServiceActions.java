/*
 *  ===========================================================================
 *  Copyright (c) 2014 Comcast Corp. All rights reserved.
 *  ===========================================================================
 *
 *  Author: ${rsrini005c}
 *  Created: ${10/02/2014}
 * /
 */

package com.comcast.guide.actionhandlers;

import com.comcast.xre.events.VirtualKey;
import com.comcast.xre.testframework.IXREReceiver;
import com.comcast.xre.testframework.services.TestModuleDataClient;
import com.comcast.xre.testframework.services.TestModuleLoggingClient;
import com.comcast.xre.toolkit.presentation.model.DataModelEntry;
import org.apache.commons.lang.StringUtils;

import static org.testng.AssertJUnit.fail;


public class EntityDataServiceActions extends X1GuideBaseTestActions {

    public EntityDataServiceActions(IXREReceiver receiver, TestModuleDataClient dataClient, TestModuleLoggingClient logClient) {
        super(receiver, dataClient, logClient);
    }


    public boolean traverseGalleryRows() {
        boolean done;
        do {
            String rowName = getCurrentRowName();
            if (rowName == null) {
                rowName = "";
            }
            done = moveThroughGalleryRow(rowName);
            if (!done) {
                sendKey(VirtualKey.DOWN, 1000);
                sleep(1000);
                boolean rowChanged = waitForRowChange(1200, rowName, false);
                if (!rowChanged) {
                    // end of rows
                    return false;
                }
            }
        } while (!done);

        return done;
    }

    public void testTVSectionOnDemandEpisodesOtherTimesTraverse() {
        testActionBarChoice();
        traverseGalleryRows();
        sendKeyExpectState(VirtualKey.EXIT, "FULL_SCREEN", 10000, true);
        //  testTVSectionOnDemandMenu();

    }

    public void testTVSectionOnDemandEpisodesorMovieTraverse(String id) {

        testTVSectionOnDemandMenu(id);
        traverseGalleryRows();
        sendKeyExpectState(VirtualKey.EXIT, "FULL_SCREEN", 10000, true);
    }

    private void keyPress(String id) {

        if (id.equals("Kids") || id.equals("Music") || id.equals("Latino") || id.equals("Local") || id.equals("XFINITY Services") || id.equals("TV"))

        {  //sendKey(VirtualKey.DOWN, 1000);
            sleep(1000);
            sendKey(VirtualKey.ENTER, 5000);
            sleep(1000);
        } else if (id.equals("Networks") || id.equals("Sports & Fitness") || id.equals("Multicultural")) {
            sendKey(VirtualKey.DOWN, 1000);
            sleep(1000);
            sendKey(VirtualKey.ENTER, 1000);
            sleep(1000);
            sendKey(VirtualKey.ENTER, 1000);
            sleep(1000);
        }

    }

    public void
    testTVorMoviesSectionOnDemandEpisodesActionBar(String id) {

        testTVSectionOnDemandMenu(id);


        keyPress(id);
        if (keepMovingRightOnTheActionBarUntilPassedActionBarNameIsFound("Watch", 10000, false) == true) {
            sendKey(VirtualKey.ENTER, 5000);
            testActionBarChoice();
        }
        testTVSectionOnDemandMenu(id);
        keyPress(id);
        if (keepMovingRightOnTheActionBarUntilPassedActionBarNameIsFound("Resume", 10000, false) == true) {
            sendKey(VirtualKey.ENTER, 5000);
            testActionBarChoice();
        }

        testTVSectionOnDemandMenu(id);
        keyPress(id);
        if (keepMovingRightOnTheActionBarUntilPassedActionBarNameIsFound("Start Over", 10000, false) == true) {
            sendKey(VirtualKey.ENTER, 5000);
            testActionBarChoice();
        }
        testTVSectionOnDemandMenu(id);
        keyPress(id);
        if (keepMovingRightOnTheActionBarUntilPassedActionBarNameIsFound("Trailer", 10000, false) == true) {
            sendKey(VirtualKey.ENTER, 5000);
            testActionBarChoice();
        }


        testTVSectionOnDemandMenu(id);
        keyPress(id);
        if (keepMovingRightOnTheActionBarUntilPassedActionBarNameIsFound("Favorite", 10000, false) == true) {
            sendKey(VirtualKey.ENTER, 1000);
            keepMovingRightOnTheActionBarUntilPassedActionBarNameIsFound("Remove Favorite", 10000, true);
            sendKey(VirtualKey.EXIT, 1000);
            //   testActionBarChoice();
        }

        testTVSectionOnDemandMenu(id);
        keyPress(id);
        if (keepMovingRightOnTheActionBarUntilPassedActionBarNameIsFound("Remove Favorite", 10000, false) == true) {
            sendKey(VirtualKey.ENTER, 1000);
            sleep(1000);
            keepMovingRightOnTheActionBarUntilPassedActionBarNameIsFound("Favorite", 10000, true);
            sendKey(VirtualKey.EXIT, 1000);
            //  testActionBarChoice();
        }


        testTVSectionOnDemandMenu(id);
        keyPress(id);
        if (keepMovingRightOnTheActionBarUntilPassedActionBarNameIsFound("Other Times", 10000, false) == true) {
            sendKey(VirtualKey.ENTER, 1000);
            sleep(1000);
            sendKey(VirtualKey.ENTER, 1000);
            sleep(1000);
            testActionBarChoice();
        }

        testTVSectionOnDemandMenu(id);
        keyPress(id);
        if (keepMovingRightOnTheActionBarUntilPassedActionBarNameIsFound("Options", 10000, false) == true) {
            sendKey(VirtualKey.ENTER, 1000);
            testActionBarChoice();
        }

        testTVSectionOnDemandMenu(id);
        keyPress(id);
        if (keepMovingRightOnTheActionBarUntilPassedActionBarNameIsFound("Record", 10000, false) == true) {
            sendKey(VirtualKey.ENTER, 1000);
            testActionBarChoice();
        }

        testTVSectionOnDemandMenu(id);
        keyPress(id);
        if (keepMovingRightOnTheActionBarUntilPassedActionBarNameIsFound("Series Info", 10000, false) == true) {
            sendKey(VirtualKey.ENTER, 1000);
            sleep(1000);
            testActionBarChoice();
            sendKey(VirtualKey.EXIT, 1000);
        }


    }


    public void testTVSectionOnDemandMenu(String id) {
        int scrollDownToRowNo = 1;

        scrollToNthRow(id, scrollDownToRowNo);
        sleep(1000);
        sendKey(VirtualKey.ENTER, 5000);
        sleep(1000);



    }

    public void testMoviesSectionOnDemandMenu(String id) {
        int scrollDownToRowNo = 1;
        int scrollRightToColumnNo = 1;
        scrollToNthRow(id, scrollDownToRowNo);
        sleep(1000);
        whichStageToRun(getCurrentRowName(), scrollRightToColumnNo);
        sleep(1000);
        scrollToNthRow(id, scrollDownToRowNo);
        scrollRightToColumnNo = 2;
        whichStageToRun(getCurrentRowName(), scrollRightToColumnNo);
        sleep(1000);
        scrollToNthRow(id, scrollDownToRowNo);
        scrollRightToColumnNo = 3;
        whichStageToRun(getCurrentRowName(), scrollRightToColumnNo);
        sleep(1000);
        scrollToNthRow(id, scrollDownToRowNo);
        traverseGalleryRows();
     /*   scrollRightToColumnNo=4;
        whichStageToRun(getCurrentRowName(), scrollRightToColumnNo);
        scrollToNthRow(id, scrollDownToRowNo);
        scrollRightToColumnNo=5;
        whichStageToRun(getCurrentRowName(), scrollRightToColumnNo);*/


        goBackToFullScreenVideoAndEvaluateExceptions();
    }

    public void goBackToFullScreenVideoAndEvaluateExceptions() {
        clearStackExpectVideoState();
        evaluateForFatalExceptions();
        sleep(500);
    }


    public boolean findItemInSubMenu(String subMenuItem) {
        boolean isFound = moveThroughActionBarUtil("menu", subMenuItem, 10000, true);
        if (!isFound) {
            return false;
        }
        waitForStateValueChange("placeholder.placeHolderPresentation.initComplete", "", 8000, false);
        sleep(3000);
        return true;
    }


    private void scrollToNthRow(String id, int rowNum) {
        if (findItemInMainMenu("ONDEMAND")) {
            sendKey(VirtualKey.ENTER, 5000);
            waitForStateValueChange("X2/ON_DEMAND/NGB/ROOT.XML", "", 8000, true);
            sleep(1000);

            if (findItemInSubMenu(id)) {
                for (int i = 1; i <= rowNum; i++) {
                    String currentRowName = getCurrentRowName();
                    sendKey(VirtualKey.DOWN);
                    waitForRowChange(5000, currentRowName, false);
                }

            }
        }
    }


    private void whichStageToRun(String rowName, int actionBarcolumn) {
        testFeaturedMoviesEntityInfo(actionBarcolumn);
        goBackToFullScreenVideoAndEvaluateExceptions();
    }


    private void testFeaturedMoviesEntityInfo(int column) {

        waitForActionBarChoice();
        sleep(2000);
        testActionBarChoice();

    }

    private boolean waitForActionBarChoice() {
        setGuideStateValue("actionBarChoice", "");
        sendKey(VirtualKey.ENTER, 1000);
        return waitForStateValueChange("actionBarChoice", "", 8000, true);
    }

    private void testActionBarChoice() {
        String actionBarChoice;
        //  navigateActionBar(position);
        actionBarChoice = getActionBarChoice();
        performAction(actionBarChoice);
    }


    private void navigateActionBar(int position) {
        int navigateTo = 2;
        while (navigateTo <= position) {
            sendKey(VirtualKey.RIGHT);
            navigateTo++;
        }
    }

    private String getActionBarChoice() {
        String guideState = getGuideStateValue("actionBarChoice");
        String[] elements = guideState.split(",");
        return elements[1];
    }

    private void performAction(String actionBarChoice) {
        if (actionBarChoice.equalsIgnoreCase("Rent") || actionBarChoice.equalsIgnoreCase("Rent/Buy") || actionBarChoice.equalsIgnoreCase("Buy")) {
            testRentingMovie();
        } else if (actionBarChoice.equalsIgnoreCase("Resume") || actionBarChoice.equalsIgnoreCase("Start Over") | actionBarChoice.equalsIgnoreCase("Trailer") || actionBarChoice.equalsIgnoreCase("Watch")) {
            testMovieResumeOrStartOverOrTrailer();

        } else if (actionBarChoice.equalsIgnoreCase("Favorite")) {
            sendKey(VirtualKey.ENTER);
            sleep(1000);
            keepMovingRightOnTheActionBarUntilPassedActionBarNameIsFound("Remove Favorities", 10000, true);

        } else if (actionBarChoice.equalsIgnoreCase("Remove Favorities")) {
            //  sendKey(VirtualKey.ENTER);
            sleep(1000);
            keepMovingRightOnTheActionBarUntilPassedActionBarNameIsFound("Favorite", 10000, true);

        } else if (actionBarChoice.equalsIgnoreCase("Mark Watched")) {
            //sendKey(VirtualKey.ENTER);
            sleep(1000);
            keepMovingRightOnTheActionBarUntilPassedActionBarNameIsFound("Mark Unwatched", 10000, true);

        } else if (actionBarChoice.equalsIgnoreCase("Mark Unwatched")) {
            //sendKey(VirtualKey.ENTER);
            sleep(1000);
            keepMovingRightOnTheActionBarUntilPassedActionBarNameIsFound("Mark Watched", 10000, true);

        } else if (actionBarChoice.equalsIgnoreCase("Record")) {
            testRecordOptionOverlay();
        } else if (actionBarChoice.equalsIgnoreCase("Cancel")) {
            testCancelRecordingOverlay();
        } else if (actionBarChoice.equalsIgnoreCase("Other Times")) {
            sendKey(VirtualKey.ENTER, 1000);
            sendKey(VirtualKey.ENTER, 1000);
            //    testTVSectionOnDemandEpisodesOtherTimesTraverse();
        } else if (actionBarChoice.equalsIgnoreCase("Episodes")) {
            sendKey(VirtualKey.ENTER, 1000);
            //  testTVSectionOnDemandEpisodesOtherTimesTraverse();
        }
    }

    private void testRentingMovie() {
        String actionBarChoice = getActionBarChoice();
        if (actionBarChoice.equalsIgnoreCase("Rent Now")) {
            sendKey(VirtualKey.ENTER, 1000);
            sleep(1000);
            sendKey(VirtualKey.ENTER, 1000);
            sleep(1000);
            sendKeyExpectState(VirtualKey.ENTER, "FULL_SCREEN", 10000, true);
            sleep(2000);
        } else {

            sendKey(VirtualKey.ENTER, 1000);
            sleep(1000);
            sendKey(VirtualKey.RIGHT, 1000);
            sendKey(VirtualKey.ENTER, 1000);
            sleep(1000);
            sendKey(VirtualKey.ENTER, 1000);
            sleep(1000);
            sendKeyExpectState(VirtualKey.ENTER, "FULL_SCREEN", 10000, true);
            sleep(2000);
        }

    }


    private void testCancelRecordingOverlay() {
        sendKey(VirtualKey.ENTER, 1000);
        sleep(1000);
        String guideStateValue = getGuideStateValue("currentOverlay") == null ? "" : getGuideStateValue("currentOverlay");
        if (guideStateValue.equalsIgnoreCase("")) {
            sendKey(VirtualKey.RIGHT, 1000);
            sendKey(VirtualKey.LEFT, 1000);
            sendKey(VirtualKey.ENTER, 1000);
            sleep(1000);
            keepMovingRightOnTheActionBarUntilPassedActionBarNameIsFound("Record", 10000, true);


        }
    }

    private void testRecordOptionOverlay() {
        sendKey(VirtualKey.ENTER, 1000);
        sleep(1000);
        String guideStateValue = getGuideStateValue("currentOverlay") == null ? "" : getGuideStateValue("currentOverlay");
        if (guideStateValue.equalsIgnoreCase("RecordOptionsOverlay")) {
            sendKey(VirtualKey.RIGHT, 1000);
            sendKey(VirtualKey.LEFT, 1000);
            sendKey(VirtualKey.ENTER, 1000);
            sleep(1000);
            keepMovingRightOnTheActionBarUntilPassedActionBarNameIsFound("Options", 10000, true);


        }

    }

    private void testMovieResumeOrStartOverOrTrailer() {
        sendKey(VirtualKey.ENTER, 1000);
        sleep(1000);
        String guideStateValue = getGuideStateValue("currentOverlay") == null ? "" : getGuideStateValue("currentOverlay");
        if (guideStateValue.equalsIgnoreCase("WatchOptionsOverlay")) {

            sendKeyExpectState(VirtualKey.ENTER, "FULL_SCREEN", 10000, true);
        }


    }

    protected boolean moveThroughGalleryRow(String currentRowName) {
        String currentTileName;
        boolean done = false;
        do {
            currentTileName = getCurrentTileName(currentRowName);
            setGuideStateValue("tileContainerChoice", "");
            if (!StringUtils.isEmpty(currentRowName)) {
                setGuideStateValue(encodeString(currentRowName + ".tileContainerChoice"), "");
            }
            sendKey(VirtualKey.RIGHT);
            boolean tileChanged = waitForTileChange(3000, currentRowName, currentTileName, false);
            if (!tileChanged) {
                // end of row
                return false;
            }
            sleep(10);
        } while (!done);

        return false;
    }

    private String lastTileName = "";

    protected String getCurrentTileName(String currentRowName) {
        if (currentRowName == null) {
            currentRowName = "";
        }

        StringBuilder key = new StringBuilder();
        if (!currentRowName.isEmpty()) {
            key.append(currentRowName);
            key.append(".");
        }
        key.append("tileContainerChoice");
        String tileContainerChoice = getGuideStateValue(encodeString(key.toString()));
        if (tileContainerChoice == null) {
            tileContainerChoice = getGuideStateValue("tileContainerChoice");
        }
        DataModelEntry dataModelEntry;
        dataModelEntry = getDataModelEntryFromJSON(tileContainerChoice);
        String title = dataModelEntry.getTitle();
        StringBuilder currentTileName = new StringBuilder();
        currentTileName.append(title);
        Long startTime = dataModelEntry.getLongParam("startTime", 0);
        if (startTime != null) {
            currentTileName.append(":").append(startTime);
        }

        return currentTileName.toString();
    }

    private boolean waitForTileChange(long maxWaitMillis, String currentRowName, String currentAppName, boolean failOnTimeout) {
        long maxTileWaitTime = 0;
        long delayTimeMills = 200;
        long startingCounter;
        long timeoutCounter = maxWaitMillis / delayTimeMills;
        if (timeoutCounter == 0) {
            timeoutCounter = 1;
        }
        startingCounter = timeoutCounter;

        do {
            sleep(delayTimeMills);
            String tileName = getCurrentTileName(currentRowName);
            if (tileName != null && !tileName.isEmpty() && !StringUtils.equals(tileName, currentAppName)) {
                long diffCount = startingCounter - timeoutCounter + 1;
                if ((diffCount * delayTimeMills) > maxTileWaitTime) {
                    maxTileWaitTime = diffCount * delayTimeMills;
                }
                return true;
            }
        } while (--timeoutCounter != 0);

        if (failOnTimeout) {
            fail("timeout waiting tile to change");
        }

        evaluateForExceptions();

        return false;
    }

    private void testActionBarNameChange(String expectectedActionBarChoice, int navigateTo) {
        sendKey(VirtualKey.ENTER, 1000);
        sleep(1000);
        navigateActionBar(navigateTo);
        String actionBarChoice = getActionBarChoice();
        if (!expectectedActionBarChoice.equalsIgnoreCase("actionBarChoice")) {
            failTest(actionBarChoice + "Not changed to " + expectectedActionBarChoice);
        }

    }


}
