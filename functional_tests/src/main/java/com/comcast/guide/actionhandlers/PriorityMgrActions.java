/* ===========================================================================
 * Copyright (c) 2014 Comcast Corp. All rights reserved.
 * ===========================================================================
 *
 * Author: TCarro004
 * Created: 9/10/2014  12:45 PM
 */

package com.comcast.guide.actionhandlers;

import com.comcast.xre.events.VirtualKey;
import com.comcast.xre.testframework.IXREReceiver;
import com.comcast.xre.testframework.services.TestModuleDataClient;
import com.comcast.xre.testframework.services.TestModuleLoggingClient;

import static org.testng.AssertJUnit.assertTrue;
import static org.testng.AssertJUnit.fail;

public class PriorityMgrActions extends SavedActions {
    public enum RequestStatus { NO_SCHEDULED_RECORDINGS, SUCCEEDED, FAILED}
    private long maxAppWaitTime;
    private String lastFailureReason = "";

    public PriorityMgrActions(IXREReceiver receiver, TestModuleDataClient dataClient, TestModuleLoggingClient logClient) {
        super(receiver, dataClient, logClient);
    }

    public RequestStatus callupPriorityManager() {
        enterSpecifiedScreenAndSelectSubMenu("SAVED_DVR","menu","Scheduled");
        if( isShingleShown() ) {
            // nothing to do
            return RequestStatus.NO_SCHEDULED_RECORDINGS;
        }
        boolean changed;
        //changed = moveDownAndWaitForRowChange(action);
        changed = moveDownAndWaitForPlaceholderFocus();
        assertTrue("Focus should have moved to first row", changed);
        changed = moveUpAndWaitForActionBarChoice("seriesPriorityAction");
        if( changed ) {
            setGuideStateValue("currentOverlay", "");
            sendKey(VirtualKey.ENTER);
            changed = waitForStateValue("currentOverlay", "X2_PRIORITY_MANAGER_OVERLAY", 5000, true);
            sleep(3000);
        }

        return (changed)? RequestStatus.SUCCEEDED : RequestStatus.FAILED;

    }
}
