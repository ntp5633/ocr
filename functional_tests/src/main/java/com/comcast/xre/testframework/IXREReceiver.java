/* ===========================================================================
 * Copyright (c) 2014 Comcast Corp. All rights reserved.
 * ===========================================================================
 *
 * Author: Comcast
 * Created: 7/9/14  4:35 PM
 */

package com.comcast.xre.testframework;

import java.util.Map;

public interface IXREReceiver {
    String getDeviceId();
    boolean isHeadless();
    void setupReceiver();
    void sendKey(String key);
    void sendDeeplinkUrl(String deeplink);
    void sendMessage(String message);
    void sendX2Notification(Map<String,String> params);
    void exitTest();

}
