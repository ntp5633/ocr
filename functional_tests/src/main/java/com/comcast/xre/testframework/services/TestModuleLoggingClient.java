package com.comcast.xre.testframework.services;

import com.ning.http.client.AsyncHttpClient;
import com.ning.http.client.websocket.WebSocket;
import com.ning.http.client.websocket.WebSocketTextListener;
import com.ning.http.client.websocket.WebSocketUpgradeHandler;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.jboss.netty.handler.codec.http.HttpHeaders;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;

/**
 * Author: skode001c
 * Created on: 7/17/2014
 */
public class TestModuleLoggingClient implements WebSocketTextListener {

    private static org.slf4j.Logger log = LoggerFactory.getLogger(TestModuleLoggingClient.class);
    private String loggingServiceHost;
    private WebSocket websocket;
    private String result = null;
    private final String waitAndCont = "";
    private List<String> serverResponse = new ArrayList<String>();
    private List<String> logFromResponse = new ArrayList<String>();
    private static ExecutorService executorService = Executors.newSingleThreadExecutor();

    public void setLoggingServiceHost(String loggingServiceHost) {
        this.loggingServiceHost = loggingServiceHost;
    }

    public String getTestLoggingServiceHost() {
        return loggingServiceHost;
    }

    class SendAndReceive implements Runnable {
        private String message;
        private String method;

        SendAndReceive(String message, String method) {
            this.message = message;
            this.method = method;
        }

        @Override
        public void run() {
            connectToSocket(this.method);
            if (websocket != null) {
                websocket.sendTextMessage(this.message);
            }

            synchronized (waitAndCont){
                try {
                    waitAndCont.wait();
                    result = responseFromServer();
                } catch (InterruptedException e) {
                    log.warn("Interrupted exception:" + e);
                }
            }
        }
    }

    private void waitForLogResponse(Future future, long maxWaitTimeMillis) {
        try {
            future.get(maxWaitTimeMillis, TimeUnit.MILLISECONDS);
        } catch (InterruptedException e) {
            System.err.println("Error: " + e.getMessage() + ", " + e.toString());
            e.printStackTrace();
        } catch (ExecutionException e) {
            System.err.println("Error: " + e.getMessage() + ", " + e.toString());
            e.printStackTrace();
        } catch (TimeoutException e) {
            System.err.println("Error: " + e.getMessage() + ", " + e.toString());
            e.printStackTrace();
        }
    }

    public List<String> getAllLogs(String receiverId){
        logFromResponse = new ArrayList<String>();
        JSONObject jObj = new JSONObject();
        try {
            jObj.put("deviceId", receiverId);
        }catch(Exception e){
            log.warn("JSON Exception: "+e.getLocalizedMessage(), e);
        }
        Future future = executorService.submit(new SendAndReceive(jObj.toString(),"get"));
        waitForLogResponse(future, 1000);
        return logFromResponse;
    }

    public List<String> getExceptionFromLogs(String receiverId){
        logFromResponse = new ArrayList<String>();
        getAllLogs(receiverId);
        List<String> exceptionLogs = new ArrayList<String>();
        if(!logFromResponse.isEmpty()){
            for(int j=0; j<=logFromResponse.size()-1; j++){
                if(logFromResponse.get(j).contains("Exception") ||
                        logFromResponse.get(j).contains("exception") ||
                        logFromResponse.get(j).contains("Failed") ||
                        logFromResponse.get(j).contains("failed") ||
                        logFromResponse.get(j).contains("WARN") ||
                        logFromResponse.get(j).contains("ERROR")){
                    exceptionLogs.add(logFromResponse.get(j));
                }
            }
        }
        return exceptionLogs;
    }

    public List<String> getLogsForTimeAndMarker(String receiverId, double start, double end){
        logFromResponse = new ArrayList<String>();
        JSONObject jObj = new JSONObject();
        try {
            jObj.put("deviceId", receiverId);
            jObj.put("start", start);
            jObj.put("end", end);
        }catch(Exception e){
            log.warn("JSON Exception: "+e.getLocalizedMessage(), e);
        }
        Future future = executorService.submit(new SendAndReceive(jObj.toString(),"get"));
        waitForLogResponse(future, 1000);
        return logFromResponse;
    }

    public boolean clearForDevice(String receiverId){
        JSONObject jObj = new JSONObject();
        try {
            jObj.put("deviceId", receiverId);
        }catch(Exception e){
            log.warn("JSON Exception: "+e.getLocalizedMessage(), e);
        }
        Future future = executorService.submit(new SendAndReceive(jObj.toString(),"clear"));
        waitForLogResponse(future, 1000);
        return (result!=null) && result.contains("Success");
    }

    public boolean clearDB(){
        JSONObject jObj = new JSONObject();
        try {
            jObj.put("db", "db");
        }catch(Exception e){
            log.warn("JSON Exception: "+e.getLocalizedMessage(), e);
        }
        Future future = executorService.submit(new SendAndReceive(jObj.toString(),"clear"));
        waitForLogResponse(future, 1000);
        return (result!=null) && result.contains("Success");
    }

    private void connectToSocket(String method) {
        AsyncHttpClient c = new AsyncHttpClient();
        try {

            AsyncHttpClient.BoundRequestBuilder builder = c.prepareGet(createUri(method))
                    .addHeader(HttpHeaders.Names.CONNECTION, "Upgrade")
                    .addHeader(HttpHeaders.Names.UPGRADE, "WebSocket")
                    .addHeader(HttpHeaders.Names.HOST, "SocketService");

            websocket = builder.execute(new WebSocketUpgradeHandler.Builder()
                    .addWebSocketListener(this).build()).get();
        } catch (IOException e) {
            log.warn("Input output exception", e);
        } catch (InterruptedException e) {
            log.warn("Interrupted Exception", e);
        } catch (ExecutionException e) {
            log.warn("Concurrent execution exception", e);
        }catch(Exception e){
            log.warn("Exception", e);
        }
    }

    @Override
    public void onOpen(WebSocket webSocket) {
        log.debug("Opening WebSocket");
    }

    @Override
    public void onClose(WebSocket webSocket) {
        log.debug("Closing WebSocket");
    }

    @Override
    public void onError(Throwable throwable) { }

    @Override
    public void onMessage(String message) {
        JSONParser parser = new JSONParser();
        try {
            org.json.simple.JSONObject responseObj = (org.json.simple.JSONObject) parser.parse(message);
            serverResponse.add(responseObj.toString());
            synchronized(waitAndCont) {
                waitAndCont.notify();
            }
        } catch (ParseException e) {
            log.warn("Failed to parse, Parse exception: "+e);
        } catch (ClassCastException e) {
            log.warn("Class cast exception: "+e);
        }catch(Exception e){
            log.warn("Exception: "+e);
        }
    }

    @Override
    public void onFragment(String s, boolean b) {    }

    private String createUri(String method) {
        return "ws://" + loggingServiceHost + "/websocket/" + method;
    }

    private String responseFromServer() {
        String response = "";
        if (!serverResponse.isEmpty()) {
            response = serverResponse.get(serverResponse.size() - 1);
            JSONObject jsonObject;
            try {
                jsonObject = new JSONObject(response);
                if(!jsonObject.isNull("log")){
                    JSONArray arr = jsonObject.getJSONArray("log");
                    if(arr.length()==1){
                        logFromResponse.add("Empty");
                    }else {
                        for (int i = 0; i < arr.length(); i++) {
                            logFromResponse.add(arr.get(i).toString());
                        }
                    }
                }

                if(!jsonObject.isNull("success")){
                    response = jsonObject.get("success").toString();
                }
            }catch (JSONException e) {
                log.warn("Response: "+response + "\nJSON Exception: "+e, e);
            }catch(Exception e){
                log.warn("JSON Exception: "+e.getLocalizedMessage(), e);
            }
        }
        return response;
    }

    //only for junit testing
    public void setValue(String receiverId1, String receiverId2){
        connectToSocket("post");
        org.json.simple.JSONObject jObj = new org.json.simple.JSONObject();
        for(int i =0; i<10; i++) {
            try {
                jObj.put("all", "All");
                jObj.put("deviceId", receiverId1);
                jObj.put("log", System.currentTimeMillis() + i + "--Log test--TestModuleLoggingClient--" + i);
                if(i==4) {
                    jObj.put("log", System.currentTimeMillis() + i + "--Log test Exception--TestModuleLoggingClient--" + i);
                    jObj.put("deviceId", receiverId1);
                }
                if(i==9) {
                    jObj.put("deviceId", receiverId2);
                    jObj.put("log", System.currentTimeMillis() + i + "--Log test NullPointerException--TestModuleLoggingClient--" + i);
                }
            } catch (Exception e) {
                log.warn("Exception: "+e.getLocalizedMessage(), e);
            }
            websocket.sendTextMessage(jObj.toJSONString());
        }

    }

    protected void sleep(long millis) {
        try {
            Thread.sleep(millis);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
