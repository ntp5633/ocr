package com.comcast.xre.testframework.util;

import com.comcast.xre.testframework.services.TestModuleLoggingClient;
import junit.framework.Assert;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * Author: Sreenath
 * Created on: 8/11/2014.
 */

public class TestModuleLoggingClientTest {

    private TestModuleLoggingClient client;
    private String deviceId1 = "4948988528844097456";
    private String deviceId2 = "4948988528844095675";
    private final String serviceIP = "162.150.184.195";
    public static Logger log = LoggerFactory.getLogger(TestModuleLoggingClientTest.class);

    @Before
    public void setUp(){
        client = new TestModuleLoggingClient();
        client.setLoggingServiceHost(serviceIP);
        testDataConnection();
    }

    //save some logs in DB
    public void testDataConnection(){
        client.setValue(deviceId1, deviceId2);
    }

    @Test
    public void testToGetAllLogs(){
        List<String> test = client.getAllLogs(deviceId1);
        Assert.assertFalse(test.isEmpty());
        testToClearDB();
    }

    @Test
    public void testToSeeExceptionLog(){
        List<String> test = client.getExceptionFromLogs(deviceId2);
        Assert.assertTrue(!test.isEmpty());
        testToClearDB();
    }

    @Test
    public void testToGetLogsForPeriod(){
        long end = 1*60*1000; //1mins
        List<String> test = client.getLogsForTimeAndMarker(deviceId1, System.currentTimeMillis() - end, System.currentTimeMillis());
        Assert.assertFalse(test.isEmpty());
        testToClearDB();
    }

    @Test
     public void testToClearLogsForDevice(){
        boolean test = client.clearForDevice(deviceId2);
        Assert.assertTrue(test);
        testToGetAllLogs();//get for device1
    }

    public void testToClearDB(){
        boolean test = client.clearDB();
        Assert.assertTrue(test);
    }
}
