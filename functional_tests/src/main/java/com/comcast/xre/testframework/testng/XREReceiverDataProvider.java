/* ===========================================================================
 * Copyright (c) 2014 Comcast Corp. All rights reserved.
 * ===========================================================================
 *
 * Author: TCarro004
 * Created: 7/9/14  4:55 PM
 */

package com.comcast.xre.testframework.testng;

import com.comcast.xre.testframework.IXREReceiver;
import org.testng.ITestContext;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.testng.annotations.DataProvider;

public class XREReceiverDataProvider {
    private static List<IXREReceiver> receiverList = new ArrayList<IXREReceiver>();

    private XREReceiverDataProvider()
    {

    }

    public static boolean receiverListEmpty() {
        return receiverList.isEmpty();
    }

    public static void clearReceivers() {
        receiverList = new ArrayList<IXREReceiver>();
    }

    public static void addReceiver(IXREReceiver receiver)
    {
        if( receiver == null )
        {
            return;
        }
        receiverList.add(receiver);
    }

    public static void addReceivers(IXREReceiver[] receivers)
    {
        if( receivers == null )
        {
            return;
        }
        for(IXREReceiver receiver : receivers)
        {
            if( !receiverList.contains(receiver) ) {
                addReceiver(receiver);
            }
        }
    }

    public static void addReceivers(List<IXREReceiver> receivers)
    {
        if( receivers == null )
        {
            return;
        }
        receiverList.addAll(receivers);
    }

    @DataProvider(name="receiverDataProvider")
    public static Iterator<Object[]> xreReceiverDataProvider(ITestContext context)
    {
        // Get a list of String file content (line items) from the test file.
        List<IXREReceiver> testData = receiverList;

        // We will be returning an iterator of Object arrays so create that
        // first.
        List<Object[]> dataToBeReturned = new ArrayList<Object[]>();

        // Populate our List of Object arrays with the file content.
        for (IXREReceiver userData : testData)
        {
            dataToBeReturned.add(new Object[] { userData });
        }
        // return the iterator - testng will initialize the test class and calls
        // the
        // test method with each of the content of this iterator.
        return dataToBeReturned.iterator();

    }
}
