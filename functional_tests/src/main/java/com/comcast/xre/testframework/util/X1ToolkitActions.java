/* ===========================================================================
 * Copyright (c) 2014 Comcast Corp. All rights reserved.
 * ===========================================================================
 *
 * Author: TCarro004
 * Created: 9/10/2014  1:41 PM
 */

package com.comcast.xre.testframework.util;

import com.comcast.xre.events.VirtualKey;
import static com.comcast.xre.ToolkitTestPoints.*;
import com.comcast.xre.testframework.IXREReceiver;
import com.comcast.xre.testframework.services.TestModuleDataClient;
import com.comcast.xre.testframework.services.TestModuleLoggingClient;
import com.comcast.xre.toolkit.DeepLinkUrlBuilder;
import com.comcast.xre.toolkit.presentation.model.DataModelEntry;
import com.comcast.xre.toolkit.presentation.model.ModuleModel;
import com.comcast.xre.toolkit.presentation.model.OpenableModuleModel;
import com.comcast.xre.toolkit.presentation.model.PresentationModel;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.lang.StringUtils;
import org.testng.Assert;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Map;

import static org.testng.AssertJUnit.fail;

public class X1ToolkitActions {
    public static final String SUBKEY_TEST_MODE_ENABLED = ":testModeEnabled";
    protected static final String SUBKEY_UPDATELOG = ":updatelog";
    private static final long DEFAULT_POST_KEY_DELAY_MILLIS = 500;
    public enum FailureMode {FAIL_ON_TIMEOUT, DONT_FAIL_ON_TIMEOUT};

    protected IXREReceiver receiver;
    protected TestModuleDataClient dataClient;
    protected TestModuleLoggingClient loggingClient;
    protected long maxRowWaitTime;
    private long postKeyDelayMillis = DEFAULT_POST_KEY_DELAY_MILLIS;

    public X1ToolkitActions(IXREReceiver receiver, TestModuleDataClient dataClient, TestModuleLoggingClient loggingClient) {
        this.receiver = receiver;
        this.dataClient = dataClient;
        this.loggingClient = loggingClient;
    }

    protected String getCurrentRowName() {
        return null;
    }

    public void setPostKeyDelayMillis(long delay) {
        this.postKeyDelayMillis = delay;
    }

    public String buildRequestKey(String deviceId, String appId, String component){
        StringBuilder sb = new StringBuilder();
        sb.append(deviceId);
        sb.append(":");
        sb.append(appId);
        sb.append(":");
        sb.append(component);
        return sb.toString();
    }

    public void setAndWaitForTestModeEnabled(String appId) {
        setAndWaitForTestModeEnabled(receiver.getDeviceId(), receiver, appId);
    }

    protected void setAndWaitForTestModeEnabled(String deviceId, IXREReceiver receiver, String appId) {
        String appIdEnabledKey = ":" + appId + SUBKEY_TEST_MODE_ENABLED;
        DeepLinkUrlBuilder urlBuilder = new DeepLinkUrlBuilder("shelltestmodule:setTestMode");
        urlBuilder.addParam("opCode", "enableTesting");
        urlBuilder.addParam("enabled", "true");
        urlBuilder.addParam("testModeEnableAck", "testModeEnabled");
        urlBuilder.addParam("targetAppId", appId);
        urlBuilder.addParam("testDataServiceHost", dataClient.getTestDataServiceHost());
        urlBuilder.addParam("testLoggingServiceHost", loggingClient.getTestLoggingServiceHost());
        int timeoutCounter = 24;
        do {
            sleep(5000);
            receiver.sendDeeplinkUrl(urlBuilder.toString());
            if( --timeoutCounter == 0 ) {
                fail("timeout waiting for test mode enabled acknowledgement");
            }
        } while(!isTestModeEnabled(deviceId, appIdEnabledKey) );

    }

    public void clearStats(String appId, boolean clearStartupValue) {
        if( !testDataConnection(receiver.getDeviceId()) ) {
            failTest("Cannot reach data service");
        }
        clearStats(receiver.getDeviceId(), appId, clearStartupValue);
    }

    public void clearUpdateLog() {
        clearUpdateLog(receiver.getDeviceId());
    }

    public void clearUpdateLog(String deviceId) {
        dataClient.clearKeys(deviceId+SUBKEY_UPDATELOG, null);
    }

    public boolean testDataConnection(String deviceId) {
        dataClient.setValue(deviceId+":guide:clientTest", "1234");
        sleep(2000);
        String value = dataClient.getStringValue(deviceId+":guide:clientTest");
        return StringUtils.equals("1234", value);
    }

    private boolean isTestModeEnabled(String deviceId, String appIdKey) {
        String startupCompleted = dataClient.getStringValue(deviceId + appIdKey);
        if( startupCompleted!=null && StringUtils.isNotEmpty(startupCompleted)) {
            return StringUtils.equals(startupCompleted,"true");
        }

        return false;
    }

    public void endTestMode(String appId) {
        DeepLinkUrlBuilder urlBuilder = new DeepLinkUrlBuilder("shelltestmodule:setTestMode");
        urlBuilder.addParam("opCode", "enableTesting");
        urlBuilder.addParam("enabled", "false");
        urlBuilder.addParam("testModeEnableAck", "testModeEnabled");
        urlBuilder.addParam("testDataServiceHost", dataClient.getTestDataServiceHost());
        urlBuilder.addParam("testLoggingServiceHost", loggingClient.getTestLoggingServiceHost());
        urlBuilder.addParam("targetAppId", appId);
        receiver.sendDeeplinkUrl(urlBuilder.toString());
    }

    public void clearStats(String deviceId, String appId, boolean clearStartupValue) {
        if( dataClient == null ) {
            return;
        }
        StringBuilder keyHeader = new StringBuilder();
        keyHeader.append(deviceId);
        keyHeader.append(":");
        keyHeader.append(appId);
        dataClient.clearKeys(keyHeader.toString()+SUBKEY_TEST_MODE_ENABLED, null);//takes two first is dbKey and second is hashKey, dbKey cannot be null
        if( clearStartupValue ) {
            dataClient.clearKeys(keyHeader.toString()+":startupStatus", null);
        }
        dataClient.clearKeys(keyHeader.toString() + ":state", null);
        dataClient.clearKeys(deviceId + ":updatelog", null);
    }

    public void sendKey(VirtualKey key) {
        sendKey(key, 0);
    }

    public void sendKeyThenDelay(VirtualKey key) {
        sendKey(key, postKeyDelayMillis);
    }

    public void sendKey(VirtualKey key, long delayMillis) {
        receiver.sendKey(key.toString());
        if( delayMillis > 0 )
            sleep(delayMillis);
    }

    public void sendKeyMultipleTimes(VirtualKey key, int count) {
        for(int k = 0; k < count; ++k) {
            sendKey(key, 0);
        }
    }

    public void pressEnter() {
        sendKey(VirtualKey.ENTER);
    }

    public void pressExit() {
        sendKey(VirtualKey.EXIT);
    }

    public void navigateRight() {
        sendKey(VirtualKey.RIGHT);
    }

    public void navigateLeft() {
        sendKey(VirtualKey.LEFT);
    }

    public void simulateEasNotification(String mode) {
        DeepLinkUrlBuilder urlBuilder = new DeepLinkUrlBuilder("shelltestmodule:easSimulatedNotification");
        urlBuilder.addParam("opCode", "simulateEasNotification");
        urlBuilder.addParam("targetAppId", "guide");
        urlBuilder.addParam("notificationURL", "guideCmd:simulateEasNotification#mode="+mode);
        receiver.sendDeeplinkUrl(urlBuilder.toString());
    }

    public boolean waitForRowChange(long maxWaitMillis, String currentRowName, boolean failOnTimeout) {
        long delayTimeMills = 200;
        long startingCounter;
        long timeoutCounter = maxWaitMillis/delayTimeMills;
        if( timeoutCounter == 0 ) {
            timeoutCounter = 1;
        }
        startingCounter = timeoutCounter;

        do {
            sleep(delayTimeMills);
            String rowName = getCurrentRowName();
            if( !StringUtils.equals(rowName, currentRowName) ) {
                long diffCount = startingCounter - timeoutCounter + 1;
                if( (diffCount*delayTimeMills) > maxRowWaitTime ) {
                    maxRowWaitTime = diffCount*delayTimeMills;
                }
                return true;
            }
        } while( --timeoutCounter != 0 );

        if( failOnTimeout ) {
            fail("timeout waiting row to change");
        }

        return false;
    }

    protected String getCurrentRowNameFromModuleModel(String moduleModelAsJSON) {
        ModuleModel moduleModel = getModuleModelFromJSON(moduleModelAsJSON);
        if( moduleModel == null ) {
            return "";
        }
        return moduleModel.getName();
    }

    protected String getCurrentSelectionListingIDFromModuleModel(String moduleModelAsJSON) {
        ModuleModel moduleModel = getModuleModelFromJSON(moduleModelAsJSON);

        if( moduleModel == null ) {
            return "";
        }
        Object listingId =  moduleModel.getParam("listingId");
        if(listingId == null){
            return "";
        }
        return listingId.toString();
    }

    protected String getCurrentSelectionEntityIDFromModuleModel(String moduleModelAsJSON) {
        ModuleModel moduleModel = getModuleModelFromJSON(moduleModelAsJSON);

        if( moduleModel == null ) {
            return "";
        }
        Object entityId =  moduleModel.getParam("entityId");
        if(entityId == null){
            return "";
        }
        return entityId.toString();
    }

    public String getAppStateValue(String appId, String key) {
        return dataClient.hashGet(receiver.getDeviceId() + ":" + appId + ":state", key);
    }

    public void setAppStateValue(String appId, String key, String value) {
        String requestKey = this.buildRequestKey(receiver.getDeviceId(), appId, "state");
        String groupValue = dataClient.setHashOrSetValue(null, key, value);
        dataClient.setHashValue(requestKey, groupValue);
    }


    public boolean waitForAppStateValue(String appId, String valueSubKey, String expectedValue, long maxWaitMillis, boolean failOnTimeout) {
        long delayInMillis = 500;
        long timeoutCounter = maxWaitMillis/delayInMillis;
        if( timeoutCounter == 0 ) {
            timeoutCounter = 1;
        }

        String currentValue;
        do {
            sleep(delayInMillis);
            currentValue = getAppStateValue(appId, valueSubKey);
            if( StringUtils.equals(expectedValue,currentValue) ) {
                return true;
            }
        } while( --timeoutCounter != 0 );

        if( failOnTimeout ) {
            failTest("timeout waiting for app state value to change to " + expectedValue + ".  Last seen state=" + currentValue);
        }

        return false;
    }

    public boolean waitForAppStateValueChange(String appId, String valueSubKey, String initialValue, long maxWaitMillis, boolean failOnTimeout) {
        long delayInMillis = 500;
        long timeoutCounter = maxWaitMillis/delayInMillis;
        if( timeoutCounter == 0 ) {
            timeoutCounter = 1;
        }

        String currentValue;
        do {
            sleep(delayInMillis);
            currentValue = getAppStateValue(appId, valueSubKey);
            if( currentValue != null && !StringUtils.equals(initialValue,currentValue) ) {
                return true;
            }
        } while( --timeoutCounter != 0 );

        if( failOnTimeout ) {
            failTest("timeout waiting for app state value to change from <" + initialValue + ">.  Last seen value=<" + currentValue + ">");
        }

        return false;
    }

    public boolean sendKeyExpectOverlay(VirtualKey key, String appId, String expectedOverlayName, long maxWaitMillis, boolean failOnTimeout) {
        sendKey(key);
        return waitForAppStateValue(appId, TP_CURRENT_OVERLAY, expectedOverlayName, maxWaitMillis, failOnTimeout);
    }

    public void sendX2Notification(String title, String message) {

    }

    public void sendX2Notification(Map<String,String> params) {
        receiver.sendX2Notification(params);
    }

    protected String derubyize(String s) {
        if( s == null || s.isEmpty() ) {
            return "";
        }

        String cval;
        cval = s.replaceAll("=>nil", ":null");
        cval = cval.replaceAll("=>", ":");
        return cval;
    }

    protected String getCurrentSelectionEntityTypeFromModuleModel(String moduleModelAsJSON) {
        ModuleModel moduleModel = getModuleModelFromJSON(moduleModelAsJSON);

        if( moduleModel == null ) {
            return "";
        }
        Object entityType =  moduleModel.getParam("entityType");
        if(entityType == null){
            return "";
        }
        return entityType.toString();
    }




    protected String encodeString(String value) {
        try {
            value = URLEncoder.encode(value, "UTF-8");
            if( value == null ) {
                value = "";
            }
        } catch (UnsupportedEncodingException e) {
            // ignored;
        }

        return value;
    }

    protected ModuleModel getModuleModelFromJSON(String moduleModelJson) {
        if( moduleModelJson == null || moduleModelJson.isEmpty() || moduleModelJson.equals("null")) {
            return new ModuleModel();
        }
        String[] parts = moduleModelJson.split(",", 2);
        if( parts.length != 2 ) {
            return new ModuleModel();
        }
        String className = parts[0];
        String json = parts[1];
        moduleModelJson = derubyize(json);
        ModuleModel moduleModel = null;
        ObjectMapper mapper = new ObjectMapper();
        try {
            if( className.contains("OpenableModuleModel")) {
                moduleModel = mapper.readValue(moduleModelJson,OpenableModuleModel.class);
            } else {
                moduleModel = mapper.readValue(moduleModelJson,ModuleModel.class);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return moduleModel;
    }

    protected DataModelEntry getDataModelEntryFromJSON(String modelAsJson) {
        if( modelAsJson == null || modelAsJson.isEmpty() || modelAsJson.equals("null") ) {
            return new DataModelEntry();
        }
        String[] parts = modelAsJson.split(",", 2);
        if( parts.length != 2 ) {
            return new DataModelEntry();
        }
        String className = parts[0];
        String json = parts[1];
        modelAsJson = derubyize(json);
        DataModelEntry model = null;
        ObjectMapper mapper = new ObjectMapper();
        try {
                model = mapper.readValue(modelAsJson, DataModelEntry.class);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return model;
    }

    public PresentationModel getPresentationModelEntryFromJSON(String modelAsJson) {
        if( modelAsJson == null || modelAsJson.isEmpty() || modelAsJson.equals("null") ) {
            return new PresentationModel();
        }
        String[] parts = modelAsJson.split(",", 2);
        if( parts.length != 2 ) {
            return new PresentationModel();
        }
        String className = parts[0];
        String json = parts[1];
        modelAsJson = derubyize(json);
        PresentationModel model = null;
        ObjectMapper mapper = new ObjectMapper();
        try {
            model = mapper.readValue(modelAsJson, PresentationModel.class);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return model;
    }

    protected void sleep(long millis) {
        try {
            Thread.sleep(millis);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void failTest(String message)  {
        Assert.fail(message);
    }
}
