/* ===========================================================================
 * Copyright (c) 2014 Comcast Corp. All rights reserved.
 * ===========================================================================
 *
 * Author: TCarro004
 * Created: 7/9/14  4:40 PM
 */

package com.comcast.xre.testframework.receivers;

import com.comcast.xre.testframework.IXREReceiver;
import com.comcast.xre.xapi.IXapiConnector;
import com.comcast.xre.xapi.XapiConnectorImpl;
import com.comcast.xre.xapi.http.HttpClientException;

import java.util.Map;

public class StbXREReceiver extends XREReceiverBase implements IXREReceiver {
    private IXapiConnector connector;

    public StbXREReceiver(String deviceId) {
        super(deviceId);
        connector = new XapiConnectorImpl.Builder().deviceID( deviceId ).build();
    }

    public boolean isHeadless() {
        return false;
    }

    public void setupReceiver() {

    }

    public void sendKey(String key) {
        try {
            connector.sendKeyEvent(IXapiConnector.KEY_TYPE.valueOf(key));
        } catch (HttpClientException e) {
            e.printStackTrace();
        }
    }

    public void sendDeeplinkUrl(String deeplink) {
        try {
            connector.processDeeplink(deeplink);
        } catch (HttpClientException e) {
            e.printStackTrace();
        }
    }

    public void sendMessage(String message) {
        try {
            connector.sendAlert(message);
        } catch (HttpClientException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void sendX2Notification(Map<String,String> params) {
        try {
            connector.sendX2Notification(params);
        } catch (HttpClientException e) {
            e.printStackTrace();
        }
    }

    public void exitTest() {

    }
}
