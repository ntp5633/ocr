/* ===========================================================================
 * Copyright (c) 2014 Comcast Corp. All rights reserved.
 * ===========================================================================
 *
 * Author: TCarro004
 * Created: 7/10/14  4:13 PM
 */

package com.comcast.xre.testframework.util;

import ch.qos.logback.classic.Level;
import com.comcast.xre.testframework.IXREReceiver;
import com.comcast.xre.testframework.receivers.HeadlessXREReceiver;
import com.comcast.xre.testframework.receivers.StbXREReceiver;
import com.comcast.xre.testframework.services.TestModuleDataClient;
import com.comcast.xre.testframework.services.TestModuleLoggingClient;
import com.comcast.xre.testframework.testng.XREReceiverDataProvider;
import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.*;

import static org.testng.AssertJUnit.fail;

public class TestBase {
    public static Logger log = LoggerFactory.getLogger(TestBase.class);

    private String testDataServiceHost = "localHost";
    private String testLoggingServiceHost = "localHost";
    private static IXREReceiver[] receiverList;
    private static TestModuleDataClient dataClient;
    private static TestModuleLoggingClient loggingClient;
    private static boolean connectionFailure;

    private IXREReceiver receiver;

    @BeforeSuite(alwaysRun = true)
    public void setup() {
        ch.qos.logback.classic.Logger logger = (ch.qos.logback.classic.Logger) LoggerFactory.getLogger("ROOT");
        logger.setLevel(Level.ERROR);

        setupDataAndLoggerClients();

        String xreServerUrl = System.getProperty("xreServer");
        String deviceId = System.getProperty("deviceId");
        boolean isHeadless = BooleanUtils.toBoolean(System.getProperty("isHeadless"));

        if( XREReceiverDataProvider.receiverListEmpty() ) {
            receiverList = new IXREReceiver[1];
            if (isHeadless) {
                receiverList[0] = new HeadlessXREReceiver(deviceId, xreServerUrl);
            } else {
                receiverList[0] = new StbXREReceiver(deviceId);
            }
            receiverList[0].setupReceiver();
            prepareReceiver(receiverList[0]);
            XREReceiverDataProvider.addReceivers(receiverList);
        }
    }

    protected void prepareReceiver(IXREReceiver receiver) {
        // overridden by implementations
    }

    public static void setConnectionFailure(boolean failed) {
        connectionFailure = failed;
    }

    public static boolean connectionFailed() {
        return connectionFailure;
    }

    @AfterSuite(alwaysRun = true)
    public void suiteShutdown() {
        int k = 0;
        for(IXREReceiver receiver : receiverList) {
            shuttingDownAllTests(receiverList[k++]);
        }

    }
    protected void shuttingDownAllTests(IXREReceiver receiver) {
        // overridden by implementations
    }

    @BeforeClass(alwaysRun = true)
    public void init() {
    }

    @BeforeTest(alwaysRun = true)
    public void beforeTest() {
        if( connectionFailed() ) {
            Assert.fail("Data connection failure");
        }
    }

    @AfterTest(alwaysRun = true)
    public void shutdown()
    {
    }

    public TestBase() {
    }

    public void setupDataAndLoggerClients() {
        String serviceUrl = System.getProperty("testDataServiceHost");
        if( StringUtils.isNotEmpty(serviceUrl)) {
            this.testDataServiceHost = serviceUrl;
        }
        log.info("testDataServiceHost=" + testDataServiceHost);

        serviceUrl = System.getProperty("testLoggingServiceHost");
        if( StringUtils.isNotEmpty(serviceUrl)) {
            this.testLoggingServiceHost = serviceUrl;
        }
        log.info("testLoggingServiceHost=" + testLoggingServiceHost);

        this.dataClient = new TestModuleDataClient();
        dataClient.setTestDataHost(testDataServiceHost);

        this.loggingClient = new TestModuleLoggingClient();
        loggingClient.setLoggingServiceHost(testLoggingServiceHost);
    }

    public TestModuleDataClient getTestModuleDataClient() {
        return dataClient;
    }

    public TestModuleLoggingClient getTestModuleLoggingClient() {
        return loggingClient;
    }

    public void setReceiver(IXREReceiver receiver) {
        this.receiver = receiver;
    }

    protected void sleep(long millis) {
        try {
            Thread.sleep(millis);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}
