/* ===========================================================================
 * Copyright (c) 2014 Comcast Corp. All rights reserved.
 * ===========================================================================
 *
 * Author: Comcast
 * Created: 7/9/14  4:40 PM
 */

package com.comcast.xre.testframework.receivers;

import com.comcast.xre.command.XRECommand;
import com.comcast.xre.test.receiver.XREClientConnection;
import com.comcast.xre.test.receiver.XREReceiver;
import com.comcast.xre.test.receiver.configuration.Configurations;
import com.comcast.xre.test.receiver.event.RPCCallEvent;
import com.comcast.xre.test.receiver.interfaces.ICommandListener;
import com.comcast.xre.test.receiver.runtime.ReceiverGroup;
import com.comcast.xre.test.receiver.scenario.items.KeyPressScenarioItem;
import com.comcast.xre.test.receiver.scenario.model.KeyPress;
import com.comcast.xre.testframework.IXREReceiver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class HeadlessXREReceiver extends XREReceiverBase implements ICommandListener, IXREReceiver {
    public static Logger log = LoggerFactory.getLogger(HeadlessXREReceiver.class);

    private XREReceiver receiver;
    private String xreServerUrl;

    public HeadlessXREReceiver(String deviceId, String serverUrl) {
        super(deviceId);
        this.xreServerUrl = serverUrl;
    }

    public boolean isHeadless() {
        return true;
    }

    public void setupReceiver() {
        ReceiverGroup receiverGroup;
        receiverGroup = Configurations.XG1.get().getReceiverGroup().get(0);
         URI uri;
        try{
            uri = new URI(xreServerUrl);
        }
        catch(URISyntaxException e){
            return;
        }

        receiver = new XREReceiver(uri, null,receiverGroup);
        receiver.addCommandListner(this);
        receiver.connect(uri.getPath().substring(1), null, null);
        run();
    }

    public void sendKey(String key) {
        KeyPress keyPress = new KeyPress();
        keyPress.setVirtualKey(key);
        new KeyPressScenarioItem().execute(keyPress, receiver);
    }

    public void sendDeeplinkUrl(String deeplink) {
        List paramList = new ArrayList<Object>();
        paramList.add(deeplink);
        sendRPCCall("deepLinkUrl", paramList);
    }

    @Override
    public void sendMessage(String message) {

    }

    @Override
    public void sendX2Notification(Map<String,String> params) {

    }

    //To be broken out - right now it's quick and dirty for prototype
    private void sendRPCCall(String method, List paramList) {
        String destinationGuid = receiver.getSessionGUID();
        String sourceGuid = receiver.getSessionGUID();
        RPCCallEvent e = new RPCCallEvent();
        e.setSource(XREClientConnection.XRE_ID_ROOT_APP);
        e.getParams().setMethod(method);
        e.getParams().setCallParams(paramList);
        e.getParams().setDestinationSessionGUID(destinationGuid);
        e.getParams().setSourceSessionGUID(sourceGuid);
        e.getParams().setCallGUID(sourceGuid);

        String destGUID = e.getParams().getDestinationSessionGUID();

        //Hack - Just use the receiver as the client connection for now
        XREClientConnection destApp = receiver;

        if(destApp != null) {
            log.trace("Sending RPC method {} to {}", e.getParams().getMethod(), destGUID);
            destApp.getEventDispatcher().dispatchEvent(e);
        }
        else {
            log.debug("Destination app not found {}", destGUID );
        }
    }

    public void exitTest() {
        receiver.disconnect();
        receiver.terminate();
    }

    public void onCommand(XRECommand command) {
        if(command.getCommand().equals(XRECommand.Name.CONNECT)) {
        }

    }

    public void run() {
        // wait for connect
        while( !receiver.isConnected() ) {
            sleep(1000);
        }

        log.info("TC: Headless receiver is connected");
    }

    private void sleep(long millis) {
        try {
            Thread.sleep(millis);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
