/* ===========================================================================
 * Copyright (c) 2014 Comcast Corp. All rights reserved.
 * ===========================================================================
 *
 * Author: skode001c
 * Created: 8/4/2014  4:40 PM
 */
package com.comcast.xre.testframework.services;

import org.apache.commons.lang.StringUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.codehaus.jettison.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;

public class TestModuleDataClient {
    private String dataHost;
    private String testDataServiceHost;

    public TestModuleDataClient(){

    }

    public void setTestDataHost(String testDataHost) {
        this.testDataServiceHost = testDataHost;
        this.dataHost = "http://" + testDataHost + "/";
    }

    public String getTestDataServiceHost() {
        return testDataServiceHost;
    }

    public String getStringValue(String key) {
        String result = null;
        HttpResponse response = dataServiceGet(buildParams(key, null));
        int responseStatusCode = getResponseCode(response);
        if (responseStatusCode == 200) {
            result = getResultFromResponse(response);
            result = extractFromResult(result, key);
        } else if (responseStatusCode == 500) {
            return "error";
        }
        return result;
    }

    private String decodeString(String value) {
        try {
            value = URLDecoder.decode(value, "UTF-8");
        } catch(UnsupportedEncodingException use) {
            System.err.println("invalid UTF-8 encoding in string=" + value);
        } catch(IllegalArgumentException iae) {
            System.err.println("unable to decode string=" + value);
        }

        return value;
    }

    public String hashGet(String dbKey, String hashKey){
        String result = null;
        HttpResponse response = dataServiceGet(buildParams(dbKey, hashKey));
        int responseStatusCode = getResponseCode(response);
        if (responseStatusCode == 200) {
            result = getResultFromResponse(response);
            result = convertJSON2String(result, dbKey, hashKey);
        } else if (responseStatusCode == 500) {
            return "error";
        }
        String rtnString = decodeString(result);
        if( rtnString != null && rtnString == "null" ) {
            return null;
        }

        return rtnString;
    }

    private String extractFromResult(String result, String key){
        if( result!=null && StringUtils.isNotEmpty(result)) {
            try{
                JSONObject jObj = new JSONObject(result);
                result = jObj.get(key).toString();
                result = result.substring(0,result.indexOf(':'));
                return result;
            }catch(Exception e){
                return null;
            }
        }
        return result;
    }

    private int getResponseCode(HttpResponse response){
        return response.getStatusLine().getStatusCode();
    }

    private String getResultFromResponse(HttpResponse response){
        String startupCompleted = null;
        try{
            BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
            String line;
            while ((line = rd.readLine()) != null) {
                startupCompleted = line;
                JSONObject jObj = new JSONObject(startupCompleted);
                if(jObj.get("key_value")!=null)
                    startupCompleted = jObj.get("key_value").toString();
            }
        }catch(Exception e){
            return null;
        }
        return startupCompleted;
    }


    private HttpResponse dataServiceGet(String parameters) {
        HttpResponse response;
        HttpClient client = HttpClientBuilder.create().build();
        HttpGet get = new HttpGet(createUri("redis_get_value")+"?"+parameters);

        get.setHeader("Content-Type", "application/json");
        get.setHeader("Accept", "application/json");
        try{
            response = client.execute(get);
        }catch(Exception e){
            response = null;
        }
        return response;
    }

    private String buildParams(String dbKey, String hashKey){

        StringBuilder sb = new StringBuilder();
        if(dbKey!=null) {
            sb.append("db_key=");
            sb.append(dbKey);
        }
        if(hashKey!=null) {
            sb.append("&type=hash&hash_key=");
            sb.append(hashKey);
        }

        return sb.toString();
    }

    private String convertJSON2String(String appState, String key, String hashKey){

        try {
            JSONObject jObj = new JSONObject(appState);
            JSONObject jObj1 = new JSONObject(jObj.get(key).toString());
            appState = jObj1.get(decodeString(hashKey)).toString();
        }catch(Exception e){
            e.printStackTrace();
        }
        return appState;
    }


    private String createUri(String cmd) {
        return dataHost + cmd;
    }

    private String dataServiceClear(String dbKey, String hashKey) {
        HttpClient client = HttpClientBuilder.create().build();
        HttpPost post = new HttpPost(createUri("delete_by_key"));
        StringBuilder rtnString = new StringBuilder();
        String line;
        try {

            JSONObject jObj = new JSONObject();
            jObj.put("db_key", dbKey);
            if(hashKey!=null)
                jObj.put("hash_keys", hashKey);

            StringEntity input = new StringEntity(jObj.toString());

            input.setContentType("application/json");
            post.setHeader("Content-Type", "application/json");
            post.setHeader("Accept", "application/json");
            post.setEntity(input);
            HttpResponse response = client.execute(post);
            BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

            while ((line = rd.readLine()) != null) {
                rtnString.append(line);
            }
        }catch(Exception e){
            return "";
        }
        return rtnString.toString();
    }

    public String clearKeys(String dbKey, String hashKey){
        return dataServiceClear(dbKey, hashKey);
    }

    public void setValue(String rdKey, String value){
        String postValue = setHashOrSetValue(value, null, null);
        dataServicePost(rdKey, postValue);
    }
    public void setHashValue(String rdKey, String value){
        dataServicePost(rdKey, value);
    }

    private void dataServicePost(String rdKey, String value) {
        HttpClient client = HttpClientBuilder.create().build();
        HttpPost post = new HttpPost(createUri("redis_save"));
        try {
            StringEntity input = new StringEntity("{\"redis\":{\"" + rdKey +"\":"+value+"}}");
            input.setContentType("application/json");
            post.setHeader("Content-Type", "application/json");
            post.setHeader("Accept", "application/json");
            post.setEntity(input);
            HttpResponse response = client.execute(post);
            BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
            StringBuilder sb = new StringBuilder();
            String line;
            while ((line = rd.readLine()) != null) {
                sb.append(line);
            }
            System.out.println("dataServicePost(" + rdKey + ", " + value + "): Response=" + sb.toString());
        }catch(Exception e){
            System.err.println("dataServicePost(" + rdKey + ", " + value + "): Response=" + "Exception: " + e.toString());
        }
    }

    public String setHashOrSetValue(String status, String key, String value){
        if(status!=null) {
            String valueSet = status + ":" + System.currentTimeMillis();
            valueSet = "\""+valueSet+"\"";
            return valueSet;
        }
        else
            return "{\""+key+"\":\""+value+"\"}";

    }
}
