/* ===========================================================================
 * Copyright (c) 2014 Comcast Corp. All rights reserved.
 * ===========================================================================
 *
 * Author: TCarro004
 * Created: 7/10/14  4:27 PM
 */

package com.comcast.xre.testframework.receivers;

public abstract class XREReceiverBase {
    private String deviceId;

    public XREReceiverBase(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getDeviceId() {
        return deviceId;
    }
}
