/**
 * Interface to the XAPI public apis
 *
 *
 * @author MBarbo000
 *
 */
package com.comcast.xre.xapi;

import com.comcast.xre.xapi.http.HttpClientException;
import com.comcast.xre.xapi.http.ResponseObject;

import java.util.Map;

public interface IXapiConnector {

	/**
	 * Available types of content which can be tuned to via XAPI
	 */
	static enum SOURCE_TYPE {
		CHANNEL, VOD, DVR, WEB,
	}
	/**
	 * Available Keys which can be sent to the settop box via XAPI
	 *
	 */
	static enum KEY_TYPE {
		MENU, GUIDE, INFO, EXIT, PREV,
        PLAY, STOP, PAUSE, RECORD, FAST_FORWARD, REWIND, REPLAY,
        MUTE,
        COLOR_KEY_0, COLOR_KEY_1, COLOR_KEY_2, COLOR_KEY_3,
        CHANNEL_UP, CHANNEL_DOWN,
		PAGE_UP, PAGE_DOWN,
        LEFT, RIGHT, UP, DOWN,
        ESCAPE, BACKSPACE, SPACE, ENTER,
        NUMBER_0, NUMBER_1, NUMBER_2, NUMBER_3, NUMBER_4, NUMBER_5, NUMBER_6, NUMBER_7, NUMBER_8, NUMBER_9,
        NUMPAD_ADD, NUMPAD_SUBTRACT, NUMPAD_MULTIPLY, NUMPAD_DIVIDE, NUMPAD_DECIMAL,
        A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z,
        BACKQUOTE, MINUS, EQUAL, LEFTBRACKET, RIGHTBRACKET, BACKSLASH, SEMICOLON, QUOTE,
        COMMA, PERIOD, SLASH,
        DELETE
	}
	
	/**
     * Sends a deeplink URL to the XAPI server.  Supported urls are available
     * in the XAPI documentation
     * 
     * <br><br><strong>NOTE</strong>: deviceID must be obtained prior to running this method.  The
     * deviceID is automatically obtained in the retrieveDeviceID() 
     * method.
     * 
     * @param url String representation of the XCal application deeplink URL to send to the XAPI server
     */
	public ResponseObject processDeeplink(String url)
        throws HttpClientException;

	/**
	 * Sends a key event to the XAPI server.  Supported keys are all keys available
	 * in the XAPI documentation
	 * 
	 * <br><br><strong>NOTE</strong>: deviceID must be obtained prior to running this method.  The
	 * deviceID is automatically obtained in the retrieveDeviceID() 
	 * method.
	 * 
	 * @param k Value from KEY_TYPE enum which specifies which remote key to send
	 */
	public ResponseObject sendKeyEvent(KEY_TYPE k)
		throws HttpClientException;
	
	/**
	 * Tunes to the specified URI.  See XAPI documentation for details on what 
	 * can be tuned to
	 * 
	 * <br><br><strong>NOTE</strong>: deviceID must be obtained prior to running this method.  The
	 * deviceID is automatically obtained in the retrieveDeviceID() 
	 * method
	 * 
	 * @see SOURCE_TYPE
	 * 
	 * @param s SOURCE_TYPE The type of service to attempt to turn to
	 * @param uri The URI of the service to tune to (see XAPI docs for details)
	 * @throws HttpClientException
	 */
	public ResponseObject tuneToChannel(SOURCE_TYPE s, String uri)
			throws HttpClientException;

	/**
	 * Returns the deviceID as a string which can be used in all XAPI requests.
	 * It will first telnet into the box,using the given IP address, and
	 * retrieve the receiverID from /opt/www/whitebox/wbdevice.dat. The DeviceID
	 * is obtained by looking for the receiverID in the json response from the
	 * XAPI server to getdevices.js.
	 * 
	 * @param stbIP
	 * @return String - deviceID : if the telnet was unsuccessfull it will
	 *         return null;
	 * @throws HttpClientException
	 */
	//public String retrieveDeviceID(String ip) throws HttpClientException;

	/**
	 * Send a command to shutdown an XRE application if it's up - the app name
	 * would be the one that used in the uri eg http://{server}:{port}/shell or
	 * http://{server}:{port}/xtream
	 * 
	 * @param appName
	 * @throws HttpClientException
	 */
	public ResponseObject shutdownApp(String appName) throws HttpClientException;
	
	/**
	 * Setter for deviceid
	 * @param deviceId
	 */
	public void setDeviceID(String deviceId);

	/**
	 * Return the device id set for the xapi connector
	 * 
	 * @return
	 */
	public String getDeviceID();

	ResponseObject sendAlert(String message) throws HttpClientException;
    ResponseObject sendX2Notification(Map<String,String> params) throws HttpClientException;
}
