package com.comcast.xre.xapi.http;

/**
 * A class that encompasses the http response
 * @author panand200
 *
 */
public class ResponseObject {
	
	 private String requestParams;
	 private String responseBody;
	 private int responseCode;
	
	public ResponseObject(String params, String body, int code ){
		this.requestParams = params;
		this.responseBody = body;
		this.responseCode = code;
	}
	
	public String getRequestParams() {
		return requestParams;
	}
	
	public String getResponseBody() {
		return responseBody;
	}
	
	public int getResponseCode() {
		return responseCode;
	}
	
	/**
	 * Xapi response is returned in a json format with {code:0,
	 * message:successfull}
	 * 
	 * @return
	 */
	public int getXapiResponseCode(){
		int code = 0;
			String[] s = responseBody.split(",");
			for(int i =0; i<s.length;i++){
				if(s[i].contains("code")){
					int split= s[i].indexOf(":");
					String c = s[i].substring(split+1, s[i].length());
					code = Integer.parseInt(c);
					break;
				}
			}
		
		return code;
	}

	public String getXapiResponseMessage(){
		String res = null;
		String[] s = responseBody.split(",");
		for(int i =0; i<s.length;i++){
			if(s[i].contains("message")){
				int split= s[i].indexOf(":");
				res= s[i].substring(split+1, s[i].length());
				break;
			}
		}
		return res;
		
	}
	@Override
	public String toString() {
		return "ResponseObject [REQUEST_PARAMS=" + requestParams
				+ ", RESPONSE_BODY=" + responseBody + ", RESPONSE_CODE="
				+ responseCode + "]";
	}

	
	
	
	

}