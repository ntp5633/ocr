package com.comcast.xre.xapi.http;

import org.apache.commons.httpclient.methods.PostMethod;

public class PostRequestBuilder extends RequestBuilder<PostMethod> {

	public PostRequestBuilder(String baseURL) {
		super(new PostMethod(baseURL));
	}

	public PostRequestBuilder withParameter(String name, String value) {
		method.addParameter(name, value);
		return this;
	}
}
