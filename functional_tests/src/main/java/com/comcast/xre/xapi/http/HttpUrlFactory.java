package com.comcast.xre.xapi.http;

public class HttpUrlFactory {

	//private String deviceId;
	private static String basePath = "/xapi";

	public enum OutputFormat {
		JS(".js"), XML(".xml");
		private String op;

		private OutputFormat(String opF) {
			this.op = opF;
		}

		public String getValue() {
			return this.op;
		}
	}

	public enum XAPI {
		tune, partnerlogin, userlogin, getdevices, identifydevice, channel, processkey, processdeeplink, shutdownApp, sendAlert, mediaComment;

	};

	public HttpUrlFactory() {
	}

	//public HttpUrlFactory(String devId) {
	//	this.deviceId = devId;
	//}

	//public void setDeviceId(String deviceId) {
	//	HttpUrlFactory.deviceId = deviceId;
	//}

	public String getPath(XAPI api, String deviceId) {
		String path = null;
		switch (api) {
		case tune:
			path = basePath
					+ new RequestApi("/tune", deviceId,
							OutputFormat.JS.getValue());
			break;
		case partnerlogin:
			path = basePath
					+ new RequestApi("/partnerlogin", null,
							OutputFormat.JS.getValue());
			break;
		case userlogin:
			path = basePath
					+ new RequestApi("/userlogin", null,
							OutputFormat.JS.getValue());
			break;
		case getdevices:
			path = basePath
					+ new RequestApi("/getdevices", null,
							OutputFormat.JS.getValue());
			break;
		case processkey:
			path = basePath
					+ new RequestApi("/processkey", deviceId,
							OutputFormat.JS.getValue());
			break;
		case identifydevice:
			path = basePath
					+ new RequestApi("/identifydevice", null,
							OutputFormat.JS.getValue());
			break;
		case processdeeplink:
            path = basePath
                    + new RequestApi("/processdeeplink", deviceId,
                            OutputFormat.JS.getValue());
            break;
		case mediaComment:
            path = basePath
                    + new RequestApi("/mediacomment", deviceId,
                            OutputFormat.JS.getValue());
            break;
		case sendAlert:
            path = basePath
                    + new RequestApi("/sendalert", deviceId,
                            OutputFormat.JS.getValue());
            break;
		case shutdownApp:
			path = basePath 
					+ new RequestApi("/shutdownApp",deviceId, OutputFormat.JS.getValue());
            break;
		}

		return path;
	}
}
