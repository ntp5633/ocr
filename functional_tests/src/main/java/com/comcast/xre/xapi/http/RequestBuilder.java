package com.comcast.xre.xapi.http;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpMethodBase;
import org.apache.commons.httpclient.NameValuePair;

public abstract class RequestBuilder<T extends HttpMethodBase> {
	
	protected T method;
	private List<NameValuePair> queryValues = new ArrayList<NameValuePair>();

	public RequestBuilder(T method) {
		this.method = method;
	}

	public static GetRequestBuilder getRequest(String baseUrl) {		
		return new GetRequestBuilder(baseUrl);
	}
	
	public static GetRequestBuilder getRequest(String baseUrl, String keycode, String psid) {
		return new GetRequestBuilder(baseUrl).withParameter("vcode", keycode).withParameter("psid", psid);
	}

	public static PostRequestBuilder postRequest(String baseUrl) {
		return new PostRequestBuilder(baseUrl);
	}

	public RequestBuilder<T> withPath(String path) {
		method.setPath(path);
		return this;
	}

	
	public void addQueryParam(String key, String value) {
		queryValues.add(new NameValuePair(key, value));
	}

	public T getMethod() {
		method.setQueryString(queryValues.toArray(new NameValuePair[queryValues.size()]));
		// TODO remove this once we have debugged everything
        /*
		try {
			System.out.println(method.getURI());
		} catch (URIException e) { 
			e.printStackTrace();
		}
        */
		return method;
	}
	
	public ResponseObject executeMethod() throws IOException {
		//System.out.println("Request URI: " + method.getURI());
		//System.out.println("Request Method: " + method.getClass().getSimpleName());
		
		HttpClient client = new HttpClient();
		int status = client.executeMethod(getMethod());
		if (status != 200) {
			throw new IOException("Epic Failz from Servar[code="+method.getStatusCode()+"]: " + method.getResponseBodyAsString());
		}
		return new ResponseObject(method.getParams().toString(), method.getResponseBodyAsString(), method.getStatusCode());
	}
}
