package com.comcast.xre.xapi.http;

public class HttpClientException extends Exception {

	private static final long serialVersionUID = 1L;

	public HttpClientException(String message) {
		super(message);
	}
	
	public HttpClientException(String message, Throwable cause) {
		super(message, cause);
	}

}
