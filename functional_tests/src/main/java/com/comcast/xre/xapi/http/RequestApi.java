package com.comcast.xre.xapi.http;

public class RequestApi {

	String apiName;
	String deviceId;
	String opFormat;

	public RequestApi(String api, String device, String js) {
		this.apiName = api;
		this.deviceId = device;
		this.opFormat = js;
	}

	public String getApiName() {
		return apiName;
	}

	public void setApiName(String apiName) {
		this.apiName = apiName;
	}

	public String getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}

	public String getOutputFormat() {
		return opFormat;
	}

	public void setOutputFormat(String outputFormat) {
		this.opFormat = outputFormat;
	}

	@Override
	public String toString() {
		if (this.deviceId != null)
			return this.apiName + "/" + this.deviceId + this.opFormat;
		else
			return this.apiName + this.opFormat;
	}

}
