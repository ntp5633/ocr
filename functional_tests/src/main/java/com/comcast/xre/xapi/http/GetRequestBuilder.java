package com.comcast.xre.xapi.http;

import org.apache.commons.httpclient.methods.GetMethod;


public class GetRequestBuilder extends RequestBuilder<GetMethod> {

	public GetRequestBuilder(String baseUrl) {
		super(new GetMethod(baseUrl));
	}

	public GetRequestBuilder withParameter(String name, String value) {		
		this.addQueryParam(name, value);				
		return this;
	}

}
