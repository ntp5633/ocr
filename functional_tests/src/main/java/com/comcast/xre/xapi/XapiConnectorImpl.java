package com.comcast.xre.xapi;

import java.io.IOException;
import java.util.Map;

import com.comcast.xre.xapi.IXapiConnector;
import com.comcast.xre.xapi.http.*;
import com.comcast.xre.xapi.http.HttpUrlFactory.XAPI;
import org.apache.commons.lang.StringUtils;

/**
 * @author TEL35
 *
 */
public class XapiConnectorImpl implements IXapiConnector {
	private String receiverID;
	private String catCmd = "cat ";
	private static String file = "/opt/www/whitebox/wbdevice.dat";
	protected String user_name ;
	protected String token;
	protected String psid;
	private String deviceID;
	private String serverAddress;
	private HttpUrlFactory urlFactory;

	/**
	 * Builder for XapiConnector which allows the username, token,
	 * serverAddress, deviceID to be specified To create a XapiConnector follow
	 * this pattern <code>IXapiConnector connector = 
	 * 							new XapiConnectorImpl.Builder().
	 * 							deviceID("1949681").
	 * 							serverAddress(serverAddress).build();</code>
	 * 
	 * The other values that have not been specified will have default values.
	 * Note: for deviceID, there isn't a default value that works, so incase the
	 * deviceID is not known use
	 * {link XapiConnectorImpl#initiateConnection(String)} and
	 * {link XapiConnectorImpl#retrieveDeviceID(String ip)}
	 *
	 * param user_name
	 *            the comcast.net email address which is used to authenticate
	 *            the device. Needs to take the form of xxxxx@comcast.net
	 * 
	 */

	
	public static class Builder{
		// Optional parameters - initialized to default values
		protected String user_name = "igorshapiro@comcast.net";
		protected String token = "token";
		protected String psid = "6by6lzvW0aqX6MB2E3pmMA==";
		private String deviceID;
		private String serverAddress = "http://ci.xapi.ccp.xcal.tv:10046";
		
		public Builder userName(String uName){
			this.user_name = uName;
			return this;
		}
		
		public Builder deviceID(String id){
			this.deviceID = id;
			return this;
		}
		
		public Builder serverAddress(String address){
			this.serverAddress = address;
			return this;
		}
		
		public Builder psid(String psid){
			this.psid = psid;
			return this;
		}
		
		public Builder token(String token){
			this.token = token;
			return this;
		}
		
		public XapiConnectorImpl build(){
			return new XapiConnectorImpl(this);
		}
	}
	
	private XapiConnectorImpl(Builder b){
		this.deviceID = b.deviceID;
		this.user_name = b.user_name;
		this.serverAddress = b.serverAddress;
		this.psid = b.psid;
		this.token = b.token;
		urlFactory = new HttpUrlFactory();
	}
	/**
	 * Getter for the current deviceID
	 * 
	 * @return String current deviceID 
	 */
	@Override
	public String getDeviceID() {
		return deviceID;
	}

	@Override
	public void setDeviceID(String deviceId){
		this.deviceID = deviceId;
	}

	@Override
	public ResponseObject tuneToChannel(SOURCE_TYPE s, String uri)
			throws HttpClientException {
		try {
			ResponseObject response = 
					RequestBuilder.getRequest(serverAddress)
							.withParameter("sourceType", s.toString())
							.withParameter("uri", uri)
							.withParameter("psid", this.psid)
							.withPath(urlFactory.getPath(XAPI.tune, deviceID)).executeMethod();

			
			//System.out.println("Response:\n" + response.toString() + "\n---END---");
			return response;
		} catch (IOException e) {
			throw new HttpClientException(
					"Client was unable to send an HTTP request: {IOException} ");
		}
		
		
	}
	
	/*
	 * (non-Javadoc)
	 * @see com.comcast.video.xapi.IXapiConnector#sendKeyEvent(com.comcast.video.xapi.IXapiConnector.KEY_TYPE)
	 */
	@Override
	public ResponseObject sendKeyEvent(KEY_TYPE k)
		throws HttpClientException {
		try {
			ResponseObject response = RequestBuilder.getRequest(serverAddress)
										.withParameter("vcode", k.toString())
										.withParameter("psid", this.psid)
										.withPath(urlFactory.getPath(XAPI.processkey, deviceID))
										.executeMethod();
			
			//System.out.println("Response:\n" + response.toString() + "\n---END---");
			return response;
		} catch (IOException e) {
			throw new HttpClientException(
					"Client was unable to send an HTTP request: {IOException} ", e);
		}
	}

    @Override
    public ResponseObject sendX2Notification(Map<String,String> params)throws HttpClientException {
        try {
            GetRequestBuilder builder = RequestBuilder.getRequest(serverAddress);
            builder.withParameter("version", "2.0");
            builder.withParameter("messageType", params.get("messageType"));
            builder.withParameter("messageTitle",params.get("messageTitle")); //"MediaComment")
            builder.withParameter("messageBody",params.get("messageBody"));
            String btnTexts = params.get("btnTexts");
            if(!StringUtils.isEmpty(btnTexts)) {
                builder.withParameter("btnTexts", btnTexts);
            }
            String btnActions = params.get("btnActions");
            if(!StringUtils.isEmpty(btnActions)) {
                builder.withParameter("btnActions", btnActions);
            }
            builder.withParameter("psid", this.psid);
            builder.withPath(urlFactory.getPath(XAPI.sendAlert, deviceID));
            ResponseObject response = builder.executeMethod();

            //System.out.println("Response:\n" + response.toString() + "\n---END---");
            return response;
        } catch (IOException e) {
            throw new HttpClientException(
                    "Client was unable to send an HTTP request: {IOException} ", e);
        }
    }

    /*
     * (non-Javadoc)
     * @see com.comcast.video.xapi.IXapiConnector#sendKeyEvent(com.comcast.video.xapi.IXapiConnector.KEY_TYPE)
     */
    @Override
    public ResponseObject sendAlert(String message) throws HttpClientException {
        return sendAlertInternal(message, "1.0");
    }

	private ResponseObject sendAlertInternal(String message, String version)
		throws HttpClientException {
		try {
			ResponseObject response = RequestBuilder.getRequest(serverAddress)
                                        .withParameter("version", version)
										.withParameter("messageType", "TVMessage")//"MediaComment")
										.withParameter("messageTitle","MediaComment") //"MediaComment")
										.withParameter("messageBody","Test")
										.withParameter("psid", this.psid)
										.withPath(urlFactory.getPath(XAPI.sendAlert, deviceID))
										.executeMethod();
			
			//System.out.println("Response:\n" + response.toString() + "\n---END---");
			return response;
		} catch (IOException e) {
			throw new HttpClientException(
					"Client was unable to send an HTTP request: {IOException} ", e);
		}
	}
	
	
	/* (non-Javadoc)
	 * @see com.comcast.video.xapi.IXapiConnector#processDeeplink(java.lang.String)
	 */
	@Override
	public ResponseObject processDeeplink(String url)
			throws HttpClientException {
	    try {
	    	ResponseObject response = RequestBuilder.getRequest(serverAddress)
                                        .withParameter("deeplink", url)
                                        .withParameter("psid", this.psid)
                                        .withParameter("forceChangeState", "true")
                                        .withPath(urlFactory.getPath(XAPI.processdeeplink, deviceID))
                                        .executeMethod();
            
            //System.out.println("Response:\n" + response.toString() + "\n---END---");
			return response;
        } catch (IOException e) {
            throw new HttpClientException(
                    "Client was unable to send an HTTP request: {IOException} ", e);
        }
	}
	

	@Override
	public ResponseObject shutdownApp(String appName) throws HttpClientException {
		 try {
			 ResponseObject response = RequestBuilder.getRequest(serverAddress)
	                                        .withParameter("psid", this.psid)
	                                        .withParameter("appname", appName)
	                                        .withPath(urlFactory.getPath(XAPI.shutdownApp, deviceID))
	                                        .executeMethod();
	            
	            //System.out.println("Response:\n" + response.toString() + "\n---END---");
	            return response;
	        } catch (IOException e) {
	            throw new HttpClientException(
	                    "Client was unable to send an HTTP request: {IOException} ", e);
	        }
	
	}
	@Override
	public String toString() {
		return "XapiConnector [receiverID=" + receiverID + ", user_name="
				+ user_name + ", token=" + token + ", psid=" + psid
				+ ", deviceID=" + deviceID + ", serverAddress=" + serverAddress
				+ "]";
	}

}
