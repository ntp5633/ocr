/* ===========================================================================
 * Copyright (c) 2014 Comcast Corp. All rights reserved.
 * ===========================================================================
 *
 * Author: Comcast
 * Created: 10/15/2014  12:47 PM
 */

package com.comcast.xre;

public interface ToolkitTestPoints {
    //Set whenever an actionbar selection changes
    String TP_ACTIONBAR_CHOICE = "actionBarChoice";

    //Set whenever a RowControl selection changes
    String TP_ROWCONTROL_CHOICE = "rowControlChoice";

    // Written whenever a new selection is made in a gallery row
    String TP_TILECONTAINER_CHOICE = "tileContainerChoice";

    // Written whenever PREV key is passed to see recently watched programs
    String TP_LASTNINE_TILECONTAINER_CHOICE = "LastNine.tileContainerChoice";

    //Written whenver an option selector choice is changed
    String TP_OPTIONSELECTOR_CHOICE = "optionSelectorChoice";

    // Written when input module text, such as in mini search, is changed
    String TP_INPUT_MODULE_TEXT = "inputModuleText";

    // Written when x2 style notification is shown (upper right hand corner of screen)
    String TP_X2_NOTIFICATION_SHOWN = "x2NotificationShown";

    //Set whenever an empty state 'shingle' presentation is shown, such as when there are no favorites
    String TP_SHINGLE_SHOWN = "shingleShown";

    //Written whenever a placeholder presentation change takes place and the init method is completed
    String TP_PLACEHOLDER_INITCOMPLETE = "placeholder.placeHolderPresentation.initComplete";

    // Written whenever the placeholder container is focus - when selection moves from actionbar to container
    String TP_CONTAINER_PLACEHOLDER_FOCUSED = "container.placeholderFocused";

    // Written whenever the ViewControlPresentation is focused
    String TP_CONTAINER_VCP_FOCUSED = "container.vcpFocused";

    String TP_CURRENT_OVERLAY = "currentOverlay";
}
